(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/moment', 'utils/js/logUtil', 'utils/js/roUtil'], function($, moment, LogUtil, RoUtil) {
    var _currentlyAssignedAccount = null;
    var _caseSO = new SObjectModel.Case();

    //initializeBoard();
    LogUtil.info("User ID: {0}, case ID: {1}", ctx.ForceUI.UserId, ctx.ForceUI.CurrentRecordId);

    function initializeComponent(){
      $Lightning.use("c:ASP_RecentlyOpenedCasesApp", function() {
        $Lightning.createComponent("c:ASP_RecentlyOpenedCasesForAccontLtng",
          {
            'SObjectType': "Case",
            'recordId': ctx.ForceUI.CurrentRecordId
          },
          "caseListDetails",
          function(cmp) {
            $A.eventService.addHandler({
              event: "c:ASP_RecentlyOpenedCasesSelectRecordEvt",
              handler: function(event) {
                LogUtil.debug("Received record selected event: {0}", JSON.stringify(event));
                var recordId = event.getParam("recordId");
                var recordName = event.getParam("recordName");
                if (!recordId) {
                  LogUtil.error("Record ID missing");
                  return;
                }
                sforce.console.openPrimaryTab(null, '/' + recordId+'?isdtp=vw', true, recordName, function (openPrimaryResult) {
                  LogUtil.debug("OpenPrimaryTab result: {0}", openPrimaryResult.success);
                  if (!openPrimaryResult.success) {
                    LogUtil.debug("OpenPrimaryTab failed, trying to find existing tab");
                    sforce.console.focusPrimaryTabByName(recordName, function (focusPrimaryResult) {
                      LogUtil.debug("FocusPrimaryTabByName result: {0}", focusPrimaryResult.success);
                    });
                  }
                }, recordName);

                sforce.console.setCustomConsoleComponentVisible(false);
              }
            });
          }
        );
      });
    }

    initializeComponent();
  });
})(this);