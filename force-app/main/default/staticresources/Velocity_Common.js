function cleanPhoneNumber(destination) {
  destination = "" + destination;
  if(!destination) {
    return "";
  }
  
  var prefix = "";
  if(destination.charAt(0) === "+") {
    prefix = "+";
  }

  destination = destination.replace(/(\[.*\])?[^\d\*#]?/g, "");
  
  return destination.length > 0 ? prefix + destination : "";
}

phoneReady.then(function(p) {
  var phone = p.phone;
  var ui = p.ui;
    
    /*
  phone.loggedIn.subscribe(function() {
    var reasonsRetriever = ciscoReasonCodes(p.ccSettings["/reqGeneralInfo/reqCtiEndpoint"], ui.phone.login.fields.userName.value, ui.phone.login.fields.password.value);
    reasonsRetriever.getReasons().then(function(reasons) {
      var states = [];

      var ready = { label: "Ready", data: { state: "READY" }, image: "https://na1.salesforce.com/img/support/liveagent/onlineDot.png" };
      states.push(ready);
      for(var k in reasons) {
        var r = reasons[k];
        states.push({ label: k, data: r, image: "https://na1.salesforce.com/img/support/liveagent/awayDot.png" });
      }

      function getUiState(state) {
        if(state.code == -1)
          return { label: "Not Ready", image: "https://na1.salesforce.com/img/support/liveagent/awayDot.png", data: state };
        else if(state.state === "READY")
          return ready;
        else
          return ui.phoneBar.readyStates.get(state);
      }

      ui.phoneBar.readyStates = states;
      ui.phoneBar.selectedState = getUiState(phone.state);

      phone.stateChanged.subscribe(function() {
        ui.phoneBar.selectedState = getUiState(phone.state);
      });
    });
  });
    */

  phone.calls.added.subscribe(function(call) {
    call.sfCallType = call.direction == softphone.cti.callDirection.incoming ? "inbound" : "outbound";
    call.startTime = Date.now();

    var mainScreen = ui.calls.get(call).main;
  });
});

