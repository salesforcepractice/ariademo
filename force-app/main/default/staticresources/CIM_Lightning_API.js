/*!
 * Copyright (C) 2013, Aria Solutions Inc.
 * All rights reserved.
*/

/* Promise Polyfill https://github.com/taylorhakes/promise-polyfill */
!function (e, n) { "object" == typeof exports && "undefined" != typeof module ? n() : "function" == typeof define && define.amd ? define(n) : n() }(0, function () { "use strict"; function e() { } function n(e) { if (!(this instanceof n)) throw new TypeError("Promises must be constructed via new"); if ("function" != typeof e) throw new TypeError("not a function"); this._state = 0, this._handled = !1, this._value = undefined, this._deferreds = [], f(e, this) } function t(e, t) { for (; 3 === e._state;)e = e._value; 0 !== e._state ? (e._handled = !0, n._immediateFn(function () { var n = 1 === e._state ? t.onFulfilled : t.onRejected; if (null !== n) { var i; try { i = n(e._value) } catch (f) { return void r(t.promise, f) } o(t.promise, i) } else (1 === e._state ? o : r)(t.promise, e._value) })) : e._deferreds.push(t) } function o(e, t) { try { if (t === e) throw new TypeError("A promise cannot be resolved with itself."); if (t && ("object" == typeof t || "function" == typeof t)) { var o = t.then; if (t instanceof n) return e._state = 3, e._value = t, void i(e); if ("function" == typeof o) return void f(function (e, n) { return function () { e.apply(n, arguments) } }(o, t), e) } e._state = 1, e._value = t, i(e) } catch (u) { r(e, u) } } function r(e, n) { e._state = 2, e._value = n, i(e) } function i(e) { 2 === e._state && 0 === e._deferreds.length && n._immediateFn(function () { e._handled || n._unhandledRejectionFn(e._value) }); for (var o = 0, r = e._deferreds.length; r > o; o++)t(e, e._deferreds[o]); e._deferreds = null } function f(e, n) { var t = !1; try { e(function (e) { t || (t = !0, o(n, e)) }, function (e) { t || (t = !0, r(n, e)) }) } catch (i) { if (t) return; t = !0, r(n, i) } } var u = setTimeout; n.prototype["catch"] = function (e) { return this.then(null, e) }, n.prototype.then = function (n, o) { var r = new this.constructor(e); return t(this, new function (e, n, t) { this.onFulfilled = "function" == typeof e ? e : null, this.onRejected = "function" == typeof n ? n : null, this.promise = t }(n, o, r)), r }, n.prototype["finally"] = function (e) { var n = this.constructor; return this.then(function (t) { return n.resolve(e()).then(function () { return t }) }, function (t) { return n.resolve(e()).then(function () { return n.reject(t) }) }) }, n.all = function (e) { return new n(function (n, t) { function o(e, f) { try { if (f && ("object" == typeof f || "function" == typeof f)) { var u = f.then; if ("function" == typeof u) return void u.call(f, function (n) { o(e, n) }, t) } r[e] = f, 0 == --i && n(r) } catch (c) { t(c) } } if (!e || "undefined" == typeof e.length) throw new TypeError("Promise.all accepts an array"); var r = Array.prototype.slice.call(e); if (0 === r.length) return n([]); for (var i = r.length, f = 0; r.length > f; f++)o(f, r[f]) }) }, n.resolve = function (e) { return e && "object" == typeof e && e.constructor === n ? e : new n(function (n) { n(e) }) }, n.reject = function (e) { return new n(function (n, t) { t(e) }) }, n.race = function (e) { return new n(function (n, t) { for (var o = 0, r = e.length; r > o; o++)e[o].then(n, t) }) }, n._immediateFn = "function" == typeof setImmediate && function (e) { setImmediate(e) } || function (e) { u(e, 0) }, n._unhandledRejectionFn = function (e) { void 0 !== console && console && console.warn("Possible Unhandled Promise Rejection:", e) }; var c = function () { if ("undefined" != typeof self) return self; if ("undefined" != typeof window) return window; if ("undefined" != typeof global) return global; throw Error("unable to locate global object") }(); c.Promise || (c.Promise = n) });

var console = console || { log: function () { } };
var aria = window.aria;
var isInLightning = window.location.href.indexOf("lightning.force.com") !== -1;

if (typeof (aria) === "undefined") {
  aria = {

    globToRegex: function (glob) {
      return new RegExp("^" + aria.escapeRegex(glob).replace(/\\\*/g, ".*") + "$");
    },
    escapeRegex: function (str) {
      return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string  
    },
    log: {
      error: function (message) {
        console.log("ERROR: " + message);
      },
      warn: function (message) {
        console.log("WARN: " + message);
      },
      info: function (message) {
        console.log("INFO: " + message);
      },
      debug: function (message) {
        console.log("DEBUG: " + message);
      }
    }
  };
}

aria.browserCheck = function () {
  var uaMatch = function (ua) {
    ua = ua.toLowerCase();

    var match =
      /(trident)\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(ua) ||
      /(chrome)[ \/]([\w.]+)/.exec(ua) ||
      /(webkit)[ \/]([\w.]+)/.exec(ua) ||
      /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
      /(msie) ([\w.]+)/.exec(ua) ||
      ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
      [];

    var detectedBrowser = {
      browser: match[1] || "",
      version: match[2] || "0"
    };

    // IE 11 user agent detection fix, see http://stackoverflow.com/questions/17907445/how-to-detect-ie11
    if (detectedBrowser.browser === "trident") {
      detectedBrowser.browser = "msie";
    }

    return detectedBrowser;
  };

  var matched = uaMatch(navigator.userAgent);
  var browser = {};

  if (matched.browser) {
    browser[matched.browser] = true;
    browser.version = document.documentMode || matched.version;
  }

  // Chrome is Webkit, but Webkit is also Safari.
  if (browser.chrome) {
    browser.webkit = true;
  } else if (browser.webkit) {
    browser.safari = true;
  }

  return {
    isPostMessageLimited: function () {
      return browser.msie;
    },
    isIE8: function () {
      return browser.msie && document.all && !document.addEventListener;
    },
    getBrowser: function () {
      return browser;
    }
  };
}();

aria.addWindowEventListener = function (eventName, handler) {
  if (window.addEventListener)
    window.addEventListener(eventName, handler);
  else
    window.attachEvent("on" + eventName, handler);
};

aria.crossWindowProtocol = function (messageProtocol) {
  // checkNotNull and checkType were copied here to allow this file to work without dependencies, in case this is deployed outside of CIMplicity
  function checkNotNull(value, argName) {
    if (value == null) {
      throw "Required argument '" + argName + "' not found";
    }
    return value;
  };

  function checkType(value, type, argName) {
    if (typeof value != type) {
      throw "Required argument '" + argName + "' is not of type '" + type + "', it is of type'" + typeof value + "'";
    }
    return value;
  };


  var _messageProtocol = checkNotNull(messageProtocol, "messageProtocol");
  var _messageId = 0;
  var _registeredCallbacks = {};
  var _registeredHandlers = {};

  var _RETURNCOMMAND = "_return";

  function getDefaultOrigin() {
    return window.location.origin || (window.location.protocol + "//" + window.location.host);
  };

  function init() {
    _messageProtocol.addEventListener(function (event) {
      var message = null;
      try {
        var message = _messageProtocol.parseEvent(event);
        if (!message.command) {
          throw "X-Window-Message was not sent by aria.crossWindowProtocol";
        }
      } catch (ex) {
        return;
      }

      aria.log.debug("Received message '" + message.command + "' with id '" + message.id + "' from origin '" + event.origin + "'");

      if (_registeredHandlers[message.command]) {
        var source = event.source;
        var origin = event.origin && event.origin.toLowerCase();

        var postbackResult = function (result) {
          var resultMessage = { id: message.id, command: _RETURNCOMMAND };

          if (result && result.exception) {
            resultMessage.exception = result.exception;
          } else {
            resultMessage.result = result;
          }
          _messageProtocol.postMessage(source, resultMessage, origin);
        };

        try {
          var handlers = _registeredHandlers[message.command];

          for (var handlerIndex = 0, totalHandlers = handlers.length; handlerIndex < totalHandlers; handlerIndex++) {
            var handlerData = handlers[handlerIndex];
            var allowedDomains = handlerData.allowedDomains;
            var domainMatched = false;

            for (var domainIndex = 0, totalDomains = allowedDomains.length; domainIndex < totalDomains; domainIndex++) {
              var theDomain = allowedDomains[domainIndex].toLowerCase().replace(/\/$/, "");

              // 'source' and 'origin' properties are not set when running inside the Salesforce Console. The security is handled through the domain
              // whitelist of the Console Application and the Salesforce APIs. As a result, if source and origin are undefined, continue to invoke the handlers.
              if (source === undefined && origin === undefined || aria.globToRegex(theDomain).test(origin)) {
                domainMatched = true;
                handlerData.handler(message.parameters, postbackResult, source);
                break;
              }
            }
          }
          if (!domainMatched)
            aria.log.warn("Received cross window message from unrecognized origin: " + origin);
        }
        catch (ex) {
          postbackResult({ exception: ex });
        }
      }
      else if (message.command === _RETURNCOMMAND) {
        if (_registeredCallbacks[message.id]) {
          var callbackFunctions = _registeredCallbacks[message.id];
          delete _registeredCallbacks[message.id];

          if (message.exception && !!callbackFunctions.error) {
            callbackFunctions.error(message.exception);
          }
          else if (!!callbackFunctions.success) {
            callbackFunctions.success(message.result);
          }
        }
      }
    }, false);
  }

  init();

  return {
    post: function (args) {
      var command = checkNotNull(args.command, "command");
      var targetWindow = args.targetWindow;
      var parameters = args.parameters;
      var onSuccess = args.successCallback;
      var onError = args.errorCallback;
      var onTimeout = args.timeoutCallback;
      var timeout = args.timeout ? args.timeout : 30000;
      var targetDomain = args.targetDomain || getDefaultOrigin();

      var id = _messageId++;

      var callbacks = {};
      if (typeof onSuccess === "function") {
        callbacks.success = onSuccess;
      }
      if (typeof onError === "function") {
        callbacks.error = onError;
      }
      if (typeof onTimeout === "function") {
        callbacks.timeout = onTimeout;
      }

      if (callbacks.success || callbacks.error) {
        _registeredCallbacks[id] = callbacks;

        setTimeout(function () {
          if (_registeredCallbacks[id]) {
            if (_registeredCallbacks[id].timeout) {
              _registeredCallbacks[id].timeout("A timeout occurred when posting the command " + command + " with parameters " + JSON.stringify(parameters));
            } else if (_registeredCallbacks[id].error) {
              _registeredCallbacks[id].error("A timeout occurred when posting the command " + command + " with parameters " + JSON.stringify(parameters));
            }
          }
          delete _registeredCallbacks[id];
        }, timeout);
      }

      // This setTimeout call is necessary for IE 8/9 to address the following scenario
      // Window A has a frame containing window B.
      // Window A pops window C, which is in the same domain as A.
      // If C calls a function in A that posts to window B, then the message event.source will reference window C as opposed to A
      // In Firefox and Chrome, event.source will actually be window A
      // The setTimeout ensures that the postMessage is made from window A, resulting in the correct source in IE
      var message = { id: id, command: command, parameters: parameters };
      setTimeout(function () { _messageProtocol.postMessage(targetWindow, message, targetDomain, onError); }, 0);
    },
    registerHandler: function (command, handler, allowedDomains) {
      if (command === _RETURNCOMMAND) {
        throw _RETURNCOMMAND + " is a reserved command";
      }

      if (!allowedDomains) {
        allowedDomains = [getDefaultOrigin()];
      }
      if (typeof allowedDomains === "string") {
        allowedDomains = [allowedDomains];
      }

      var handlerData = { handler: checkType(handler, "function", "handler"), allowedDomains: allowedDomains };
      if (_registeredHandlers[command]) {
        _registeredHandlers[command].push(handlerData);
      }
      else {
        _registeredHandlers[command] = [handlerData];
      }
    },
    removeHandler: function (command, toRemove) {
      var handlers = _registeredHandlers[command];
      if (!handlers) {
        return;
      }

      var newHandlers = [];
      for (var i = 0, total = handlers.length; i < total; i++) {
        var aHandler = handlers[i];
        if (aHandler.handler !== toRemove) {
          newHandlers.push(aHandler);
        }
      }

      _registeredHandlers[command] = newHandlers;
    }
  };
};

/* Internet Explorer, unlike the other browsers, does not support sending postMessages between browser windows, only between iFrames.
 * As a result, communicating with a popup window (window.open()) is not supported, even in IE11. In order to overcome this limitation,
 * the following proxy method was created to support cross window communication between browser windows of the same domain. 
 * On top of that, IE8 does not support the dispatch of the 'message' event like IE9 or higher, so a workaround had to be introduced, which
 * fakes a property change on the documentElement. Handler are configured accordingly.
 */
var __ariaPostMessageProxy = function (source, msg) {
  var event = document.createEvent ? document.createEvent("HTMLEvents") : document.createEventObject();

  event.origin = window.location.protocol + "//" + window.location.host;
  event.eventName = "message";
  event.data = msg;

  setTimeout(function () {
    if (document.createEvent) { // more advanced browsers
      event.source = source;
      event.initEvent("message", true, true);
      window.dispatchEvent(event);
    } else if (window.fireEvent) { // more advanced browsers
      event.source = source;
      event.eventType = "message";
      window.fireEvent("on" + event.eventType, event);
    } else { // IE8
      event.src = source;
      event.eventType = "propertychange";
      event.propertyName = "ariaPostMessage";
      document.documentElement.fireEvent("on" + event.eventType, event);
    }
  }, 0);
};


aria.api = new function () {
  var _onLoadDeferreds = [];
  var _errorDeferreds = [];
  var _originWhitelist = (window.CIM_ORIGIN_WHITELIST ? window.CIM_ORIGIN_WHITELIST : (isInLightning ? ["*"] : [])).concat([window.location.origin || (window.location.protocol + "//" + window.location.host)]);
  var _cimplicityWindow = null;
  var lightningCimplicityWindow = null;

  var _apiReady = function () {
    return !!aria.crossWindowMessage;
  };

  var _executeAllDeferreds = function (wasSuccessful) {
    var deferreds = wasSuccessful ? _onLoadDeferreds : _errorDeferreds;
    for (var i = 0, length = deferreds.length; i < length; i++)
      deferreds[i]();

    _onLoadDeferreds = [];
    _errorDeferreds = [];
  };

  var registerWindow = function () {
    return new Promise(function (resolve, reject) {
      if (typeof sforce !== "object") {
        aria.crossWindowMessage.post({
          targetWindow: lightningCimplicityWindow || window.parent,
          command: "Aria.Api.InitializeApi",
          successCallback: function (data) {
            if (data) {
              _originWhitelist = _originWhitelist.concat(data.originWhitelist);
            }

            _cimplicityWindow = lightningCimplicityWindow || window.parent;
            _executeAllDeferreds(true);
            resolve();
          },
          errorCallback: function (message) {
            console.log("Error registering window with CIMplicity.");
            _executeAllDeferreds(false);
            reject();
          },
          timeoutCallback: function () {
            console.log("Timeout registering window with CIMplicity");
            _executeAllDeferreds(false);
            reject();
          },
          targetDomain: "*"
        });
      } else {
        _executeAllDeferreds(true);
        resolve();
      }
    });
  };

  var registerLightningComponent = function (cimplicityWindow) {
    lightningCimplicityWindow = cimplicityWindow;
    return registerWindow();
  }

  var sendApiRequest = function (command, parameters, successCallback, errorCallback, timeoutCallback) {
    var request = function () {
      aria.crossWindowMessage.post({
        targetWindow: _cimplicityWindow,
        command: command,
        parameters: parameters,
        successCallback: successCallback,
        errorCallback: errorCallback,
        timeoutCallback: timeoutCallback,
        targetDomain: "*"
      });
    }

    if (_apiReady())
      request();
    else {
      _onLoadDeferreds.push(request);
      if (typeof errorCallback === "function")
        _errorDeferreds.push(function () { errorCallback("Failed to register window with CIMplicity, API methods unavailable."); });
    }
  };

  var Event = function (command) {
    var _handlers = [];

    var fire = function (data) {
      for (var i = 0, length = _handlers.length; i < length; i++)
        _handlers[i](data);
    }

    _onLoadDeferreds.push(function () { aria.crossWindowMessage.registerHandler(command, fire, _originWhitelist); });

    return {
      bind: function (handler) {
        _handlers.push(handler);
      },
      unbind: function (handler) {
        for (var i = 0, length = _handlers.length; i < length; i++) {
          if (_handler[i] == handler) {
            delete _handlers[i];
            return;
          }
        }
      }
    };
  }

  var CustomEvent = function (incomingCommand, outgoingCommand) {

    var _channelEventHandlers = {}; // channel:string => { eventType:string => handlers:list}

    function handleIncomingEvent(data) {
      var channel = data.channel;
      var eventType = data.eventType;
      var message = data.message;

      function fireEventHandlers(handlers) {
        var i, len = handlers.length;
        for (i = 0; i < len; i++) {
          try {
            handlers[i](message, eventType);
          } catch (ex) {
            aria.log.error("Error thrown by custom event handler: " + ex.message);
          }
        }
      }

      if (_channelEventHandlers[channel]) {
        var channelHandlers = _channelEventHandlers[channel];

        if (channelHandlers[eventType]) {
          fireEventHandlers(channelHandlers[eventType]);
        }

        if (channelHandlers["*"]) {
          fireEventHandlers(channelHandlers["*"]);
        }
      }
    }

    function fire(args, successCallback, errorCallback, timeoutCallback) {
      if (typeof (args.channel) != "string" || args.channel.length == 0) {
        throw "Invalid 'channel' provided. 'channel' must be a non-empty string.";
      }

      if (typeof (args.eventType) != "string" || args.channel.length == 0 || args.channel == "*") {
        throw "Invalid 'eventType' provided. 'eventType' must be a non-empty string and the value cannot be a single asterisks.";
      }

      sendApiRequest(outgoingCommand, args, successCallback, errorCallback, timeoutCallback);
    }

    // Array.indexOf is not supported in IE8. This implementation is derived from 
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf 
    // as recommended by the MDN.
    function indexOf(arr, searchElement, fromIndex) {
      var length = arr.length >>> 0; // Hack to convert object.length to a UInt32

      fromIndex = +fromIndex || 0;

      if (Math.abs(fromIndex) === Infinity) {
        fromIndex = 0;
      }

      if (fromIndex < 0) {
        fromIndex += length;
        if (fromIndex < 0) {
          fromIndex = 0;
        }
      }

      for (; fromIndex < length; fromIndex++) {
        if (arr[fromIndex] === searchElement) {
          return fromIndex;
        }
      }

      return -1;
    }

    function subscribe(channel, eventType, handler) {
      if (!channel) {
        throw "Subscription failed: No 'channel' provided";
      }

      if (!eventType) {
        throw "Subscription failed: No 'eventType' provided";
      }

      if (typeof (handler) !== "function") {
        throw "Subscription failed: 'handler' is not a function";
      }

      if (!_channelEventHandlers[channel]) {
        _channelEventHandlers[channel] = {};
      }

      if (!_channelEventHandlers[channel][eventType]) {
        _channelEventHandlers[channel][eventType] = [];
      }

      var index = indexOf(_channelEventHandlers[channel][eventType], handler);
      if (index > -1) {
        throw "The specified handler does already exist for the event '" + eventType + "' in channel '" + channel + "'";
      }

      _channelEventHandlers[channel][eventType].push(handler);
    }

    function unsubscribe(channel, eventType, handler) {
      if (!channel) {
        throw "Unsubscribe failed: No 'channel' provided";
      }

      if (!eventType) {
        throw "Unsubscribe failed: No 'eventType' provided";
      }

      if (typeof (handler) !== "function") {
        throw "Unsubscribe failed: 'handler' is not a function";
      }

      if (!_channelEventHandlers[channel] || !_channelEventHandlers[channel][eventType]) {
        throw "The specified handler does not exist for the event '" + eventType + "' in channel '" + channel + "'";
      }

      var handlers = _channelEventHandlers[channel][eventType];
      var index = indexOf(handlers, handler);
      if (index == -1) {
        throw "The specified handler does not exist for the event '" + eventType + "' in channel '" + channel + "'";
      }

      handlers.splice(index, 1);
    }

    _onLoadDeferreds.push(function () { aria.crossWindowMessage.registerHandler(incomingCommand, handleIncomingEvent, _originWhitelist); });

    return {
      fire: fire,
      subscribe: subscribe,
      unsubscribe: unsubscribe
    };
  };

  function loadCrossWindowMessaging() {
    aria.crossWindowMessage = (function () {
      var _messageProtocol;

      if (typeof sforce === "object" && sforce.console) {
        _messageProtocol = {
          addEventListener: function (handler) {
            sforce.console.addEventListener("Aria.Api.Message", handler);
          },
          postMessage: function (targetWindow, message, targetDomain, errorCallback) {
            try {
              aria.log.debug("Posting command '" + message.command + "' with id '" + message.id + "' from '" + window.location.href + "' to salesforce'");
              sforce.console.fireEvent("Aria.Api.Message", JSON.stringify(message));
            }
            catch (ex) {
              aria.log.error("Error posting command " + message.command + " with message id: " + message.id + ": " + ex);
              if (typeof errorCallback === "function") {
                errorCallback(ex);
              }
            }
          },
          parseEvent: function (event) {
            return JSON.parse(event.message);
          }
        };
      } else {
        _messageProtocol = {
          addEventListener: function (handler) {
            window.addEventListener("message", handler);
          },
          postMessage: function (targetWindow, message, targetDomain, errorCallback) {
            try {
              aria.log.debug("Posting command '" + message.command + "' with id '" + message.id + "' from '" + window.location.href + "' to target domain '" + targetDomain + "'");
              if (aria.browserCheck.isPostMessageLimited() && targetDomain === (window.location.protocol + "//" + window.location.host)) {
                try {
                  targetWindow.__ariaPostMessageProxy(window, JSON.stringify(message));
                } catch (ex) {
                  aria.log.warn("Error invoking postMessage proxy: " + ex.message);
                }
              } else {
                targetWindow.postMessage(JSON.stringify(message), targetDomain);
              }
            } catch (ex) {
              aria.log.error("Error posting command " + message.command + " with message id: " + message.id + ": " + ex);
              if (typeof errorCallback === "function") {
                errorCallback(ex);
              }
            }
          },
          parseEvent: function (event) {
            return JSON.parse(event.data);
          }
        };
      }

      return new aria.crossWindowProtocol(_messageProtocol);
    })();

    if (!isInLightning)
      registerWindow();
  };

  if (isInLightning) {
    loadCrossWindowMessaging();
    aria.apiReady = new Promise(function (resolve, reject) {
      function registerWindow(args, postbackFunction, senderWindow) {
        postbackFunction();
        registerLightningComponent(senderWindow).then(resolve, reject);
      }

      aria.crossWindowMessage.registerHandler("Aria.LightningIntegration.InitializeApi", registerWindow, _originWhitelist);
    });
  } else {
    aria.addWindowEventListener("load", loadCrossWindowMessaging);
  }

  return {
    events: {
      signedIn: new Event("Aria.Api.Event.SignedIn"),
      signedOut: new Event("Aria.Api.Event.SignedOut"),
      interactionStateChanged: new Event("Aria.Api.Event.InteractionStateChanged"),
      placeStateChanged: new Event("Aria.Api.Event.PlaceStateChanged"),
      tabPageChanged: new Event("Aria.Api.Event.TabPageChanged")
    },
    place: {
      getUserInfo: function (successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.GetUserInfo", {}, successCallback, errorCallback, timeoutCallback);
      },
      goReady: function (successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.GoReady", {}, successCallback, errorCallback, timeoutCallback);
      },
      goNotReady: function (args, successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.GoNotReady", args.reasonCode, successCallback, errorCallback, timeoutCallback);
      }
    },
    interaction: {
      setAttachedData: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId: , data:}
        sendApiRequest("Aria.Api.SetAttachedData", args, successCallback, errorCallback, timeoutCallback);
      },
      getAllAttachedData: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId:}
        sendApiRequest("Aria.Api.GetAllAttachedData", args, successCallback, errorCallback, timeoutCallback);
      },
      getAttachedDataValue: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId: , key:}
        sendApiRequest("Aria.Api.GetAttachedDataValue", args, successCallback, errorCallback, timeoutCallback);
      },
      answer: function (args, successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.Answer", args, successCallback, errorCallback, timeoutCallback);
      },
      release: function (args, successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.Release", args, successCallback, errorCallback, timeoutCallback);
      },
      markDone: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId:}
        sendApiRequest("Aria.Api.MarkDone", args, successCallback, errorCallback, timeoutCallback);
      },
      sendCurrentCtiState: function (args, successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.SendCurrentCtiState", args, successCallback, errorCallback, timeoutCallback);
      }
    },
    voice: {
      dial: function (args, successCallback, errorCallback, timeoutCallback) { // args: {destination: , attachedData: }
        sendApiRequest("Aria.Api.Dial", args, successCallback, errorCallback, timeoutCallback);
      },
      hold: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId: }
        sendApiRequest("Aria.Api.Hold", args, successCallback, errorCallback, timeoutCallback);
      },
      retrieve: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId: }
        sendApiRequest("Aria.Api.Retrieve", args, successCallback, errorCallback, timeoutCallback);
      },
      sendDtmf: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId: , dtmfKeys: }
        sendApiRequest("Aria.Api.SendDtmf", args, successCallback, errorCallback, timeoutCallback);
      },
      oneStepTransfer: function (args, successCallback, errorCallback, timeoutCallback) { // args: {integrationId: , destination:, attachedData: }
        sendApiRequest("Aria.Api.OneStepTransfer", args, successCallback, errorCallback, timeoutCallback);
      },
      twoStepTransfer: function (args, successCallback, errorCallback, timeoutCallback) { // args: {integrationId: , destination:, attachedData: }
        sendApiRequest("Aria.Api.TwoStepTransfer", args, successCallback, errorCallback, timeoutCallback);
      },
      oneStepConference: function (args, successCallback, errorCallback, timeoutCallback) { // args: {integrationId: , destination:, attachedData: }
        sendApiRequest("Aria.Api.OneStepConference", args, successCallback, errorCallback, timeoutCallback);
      },
      twoStepConference: function (args, successCallback, errorCallback, timeoutCallback) { // args: {integrationId: , destination:, attachedData: }
        sendApiRequest("Aria.Api.TwoStepConference", args, successCallback, errorCallback, timeoutCallback);
      },
      consult: function (args, successCallback, errorCallback, timeoutCallback) { // args: {integrationId: , destination:, attachedData: }
        sendApiRequest("Aria.Api.Consult", args, successCallback, errorCallback, timeoutCallback);
      },
      completeComplexCall: function (args, successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.CompleteComplexCall", args, successCallback, errorCallback, timeoutCallback); // args: {integrationId: }
      },
      cancelComplexCall: function (args, successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.CancelComplexCall", args, successCallback, errorCallback, timeoutCallback); // args: {integrationId: }
      }
    },
    outbound: {
      setOutboundData: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId: , data:}
        sendApiRequest("Aria.Api.SetOutboundData", args, successCallback, errorCallback, timeoutCallback);
      },
      getAllOutboundData: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId:}
        sendApiRequest("Aria.Api.GetAllOutboundData", args, successCallback, errorCallback, timeoutCallback);
      },
      getOutboundDataValue: function (args, successCallback, errorCallback, timeoutCallback) { // args: {interactionId: , key:}
        sendApiRequest("Aria.Api.GetOutboundDataValue", args, successCallback, errorCallback, timeoutCallback);
      },
      nextPreviewRecord: function (args, successCallback, errorCallback, timeoutCallback) { // args: {}
        sendApiRequest("Aria.Api.NextPreviewRecord", args, successCallback, errorCallback, timeoutCallback);
      },
      cancelRecord: function (args, successCallback, errorCallback, timeoutCallback) { // args: {phoneNumber: , allRecords: }
        sendApiRequest("Aria.Api.CancelRecord", args, successCallback, errorCallback, timeoutCallback);
      },
      doNotCallRecord: function (args, successCallback, errorCallback, timeoutCallback) { // args: {phoneNumber: , allRecords: }
        sendApiRequest("Aria.Api.DoNotCallRecord", args, successCallback, errorCallback, timeoutCallback);
      }
    },
    scripting: {
      runEosScript: function (args, successCallback, errorCallback, timeoutCallback) { // args: { interactionId, script }
        sendApiRequest("Aria.Api.RunEosScript", args, successCallback, errorCallback, timeoutCallback);
      }
    },
    log: {
      debug: function (args, successCallback, errorCallback, timeoutCallback) { // args: {message }
        sendApiRequest("Aria.Api.LogDebug", args, successCallback, errorCallback, timeoutCallback);
      },
      info: function (args, successCallback, errorCallback, timeoutCallback) { // args: {message }
        sendApiRequest("Aria.Api.LogInfo", args, successCallback, errorCallback, timeoutCallback);
      },
      warn: function (args, successCallback, errorCallback, timeoutCallback) { // args: {message }
        sendApiRequest("Aria.Api.LogWarn", args, successCallback, errorCallback, timeoutCallback);
      },
      error: function (args, successCallback, errorCallback, timeoutCallback) { // args: {message }
        sendApiRequest("Aria.Api.LogError", args, successCallback, errorCallback, timeoutCallback);
      }
    },
    customEvent: new CustomEvent("Aria.Api.Event.CustomEvent", "Aria.Api.FireCustomEvent"), // args: {channel, eventType, message }
    notification: {
      showToaster: function (args, successCallback, errorCallback, timeoutCallback) { // args: {title, message, image, timeout }
        sendApiRequest("Aria.Api.ShowToaster", args, successCallback, errorCallback, timeoutCallback);
      },
      showMessageBox: function (args, successCallback, errorCallback, timeoutCallback) { // args: {title, message, image, richContent }
        sendApiRequest("Aria.Api.ShowMessageBox", args, successCallback, errorCallback, timeoutCallback);
      },
      notify: function (args, successCallback, errorCallback, timeoutCallback) {
        sendApiRequest("Aria.Api.Notify", args, successCallback, errorCallback, timeoutCallback);
      }
    }
  };
}();

if (isInLightning)
  window.aria = aria;