/**
 * caseInfo -- handler for case creation when a call is released
 */
function caseInfo(currentPage, call) {
  var currentObject = currentPage.object;
  var caseInfo = {};
  if(currentObject === "Account")
    caseInfo.AccountId = currentPage.objectId;
  else if(currentObject === "Contact")
    caseInfo.ContactId = currentPage.objectId;

  return caseInfo;
}

/**
 * taskInfo -- handler for task creation when a call is released
 */
function taskInfo(currentPage, call) {
  var currentObject = currentPage.object;
  var taskInfo = {
    Status: "Completed"
  };
  if(currentObject === "Contact" || currentObject === "Lead")
    taskInfo.WhoId = currentPage.objectId;
  else
    taskInfo.WhatId = currentPage.objectId;

  taskInfo.TaskSubtype = "Call";

  taskInfo.CallDurationInSeconds = Math.ceil((Date.now() - call.startTime) / 1000);
  taskInfo.Subject = "Call";

  return taskInfo;
}

function handleNewCall(call) {
  sforce.interaction.searchAndScreenPop(call.otherParties[0].number, "", "inbound");
};

phoneReady.then(function(p) {
  var phone = p.phone;
  var ui = p.ui;
  phone.calls.ringing.subscribe(handleNewCall);
  phone.calls.dialing.subscribe(handleNewCall);

  phone.calls.added.subscribe(setContactCard);
});
