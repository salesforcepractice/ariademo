/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  var _SHOW_ALERT = "SHOW_ALERT";
  var _NO_ALERT = "NO_ALERT";

  ctx.Rjs.define(function () {

    function displayNotification(args) {
      var notification = new Notification(args.title, args)
      if (args.timeout) {
        setTimeout(function() { notification.close() }, args.timeout);
      }
    }

    function createHtmlNotification(args, notificationFailureBehavior) {
      if (!("Notification" in window)) {
        if (notificationFailureBehavior === _SHOW_ALERT) {
          alert(args.body);
        }
      } else if (Notification.permission === "granted") {
        displayNotification(args);
      } else if (Notification.permission !== "denied") {
        Notification.requestPermission(function (permission) {
          if (permission === "granted") {
            displayNotification(args);
          } else {
            if (notificationFailureBehavior === _SHOW_ALERT) {
              alert(args.title + "\n" + args.body);
            }
          }
        });
      } else {
        if (notificationFailureBehavior === _SHOW_ALERT) {
          alert(args.body);
        }
      }
    }

    function notifyOnly(args) {
      createHtmlNotification(args, _NO_ALERT);
    }

    function notifyOrAlert(args) {
      createHtmlNotification(args, _SHOW_ALERT);
    }

    return {
      /*
       * args: {
           title: <string>
           body: <string>
           timeout: <number>
         }
       */
      notifyOnly: notifyOnly,
      notifyOrAlert: notifyOrAlert
    }
  });
})(this);