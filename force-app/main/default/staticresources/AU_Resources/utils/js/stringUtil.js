/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(function () {
    function format() {
      var result = arguments[0];
      var msgParams = Array.prototype.slice.call(arguments, 1);

      var i, len = msgParams.length;
      if (msgParams) {
        for (i = 0; i < len; i++) {
          result = result.replace("{" + i + "}", msgParams[i]);
        }
      }
      return result;
    }

    function capitalizeFirstLetter(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    return {
      format: format,
      capitalizeFirstLetter: capitalizeFirstLetter
    }
  });
})(this);