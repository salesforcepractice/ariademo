/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {

  /*! Taken from https://github.com/eriwen/javascript-stacktrace (January 4th, 2012)
   * Domain Public by Eric Wendelin http://eriwen.com/ (2008)
   *                  Luke Smith http://lucassmith.name/ (2008)
   *                  Loic Dachary <loic@dachary.org> (2008)
   *                  Johan Euphrosine <proppy@aminche.com> (2008)
   *                  Oyvind Sean Kinsey http://kinsey.no/blog (2010)
   *                  Victor Homyakov <victor-homyakov@users.sourceforge.net> (2010)
   */

  /**
  * Main function giving a function stack trace with a forced or passed in Error
  *
  * @cfg {Error} e The error to create a stacktrace from (optional)
  * @cfg {Boolean} guess If we should try to resolve the names of anonymous functions
  * @return {Array} of Strings with functions, lines, files, and arguments where possible
  */
  function printStackTrace(options) {
    options = options || { guess: true };
    var ex = options.e || null, guess = !!options.guess;
    var p = new printStackTrace.implementation(), result = p.run(ex);
    return (guess) ? p.guessAnonymousFunctions(result) : result;
  }

  printStackTrace.implementation = function () {
  };

  printStackTrace.implementation.prototype = {
    /**
    * @param {Error} ex The error to create a stacktrace from (optional)
    * @param {String} mode Forced mode (optional, mostly for unit tests)
    */
    run: function (ex, mode) {
      ex = ex || this.createException();
      // examine exception properties w/o debugger
      //for (var prop in ex) {alert("Ex['" + prop + "']=" + ex[prop]);}
      mode = mode || this.mode(ex);
      if (mode === 'other') {
        return this.other(arguments.callee);
      } else {
        return this[mode](ex);
      }
    },

    createException: function () {
      try {
        this.undef();
      } catch (e) {
        return e;
      }
    },

    /**
    * Mode could differ for different exception, e.g.
    * exceptions in Chrome may or may not have arguments or stack.
    *
    * @return {String} mode of operation for the exception
    */
    mode: function (e) {
      if (e['arguments'] && e.stack) {
        return 'chrome';
      } else if (typeof e.message === 'string' && typeof window !== 'undefined' && window.opera) {
        // e.message.indexOf("Backtrace:") > -1 -> opera
        // !e.stacktrace -> opera
        if (!e.stacktrace) {
          return 'opera9'; // use e.message
        }
        // 'opera#sourceloc' in e -> opera9, opera10a
        if (e.message.indexOf('\n') > -1 && e.message.split('\n').length > e.stacktrace.split('\n').length) {
          return 'opera9'; // use e.message
        }
        // e.stacktrace && !e.stack -> opera10a
        if (!e.stack) {
          return 'opera10a'; // use e.stacktrace
        }
        // e.stacktrace && e.stack -> opera10b
        if (e.stacktrace.indexOf("called from line") < 0) {
          return 'opera10b'; // use e.stacktrace, format differs from 'opera10a'
        }
        // e.stacktrace && e.stack -> opera11
        return 'opera11'; // use e.stacktrace, format differs from 'opera10a', 'opera10b'
      } else if (e.stack) {
        return 'firefox';
      }
      return 'other';
    },

    /**
    * Given a context, function name, and callback function, overwrite it so that it calls
    * printStackTrace() first with a callback and then runs the rest of the body.
    *
    * @param {Object} context of execution (e.g. window)
    * @param {String} functionName to instrument
    * @param {Function} function to call with a stack trace on invocation
    */
    instrumentFunction: function (context, functionName, callback) {
      context = context || window;
      var original = context[functionName];
      context[functionName] = function instrumented() {
        callback.call(this, printStackTrace().slice(4));
        return context[functionName]._instrumented.apply(this, arguments);
      };
      context[functionName]._instrumented = original;
    },

    /**
    * Given a context and function name of a function that has been
    * instrumented, revert the function to it's original (non-instrumented)
    * state.
    *
    * @param {Object} context of execution (e.g. window)
    * @param {String} functionName to de-instrument
    */
    deinstrumentFunction: function (context, functionName) {
      if (context[functionName].constructor === Function &&
                context[functionName]._instrumented &&
                context[functionName]._instrumented.constructor === Function) {
        context[functionName] = context[functionName]._instrumented;
      }
    },

    /**
    * Given an Error object, return a formatted Array based on Chrome's stack string.
    *
    * @param e - Error object to inspect
    * @return Array<String> of function calls, files and line numbers
    */
    chrome: function (e) {
      var stack = (e.stack + '\n').replace(/^\S[^\(]+?[\n$]/gm, '').
          replace(/^\s+at\s+/gm, '').
          replace(/^([^\(]+?)([\n$])/gm, '{anonymous}()@$1$2').
          replace(/^Object.<anonymous>\s*\(([^\)]+)\)/gm, '{anonymous}()@$1').split('\n');
      stack.pop();
      return stack;
    },

    /**
    * Given an Error object, return a formatted Array based on Firefox's stack string.
    *
    * @param e - Error object to inspect
    * @return Array<String> of function calls, files and line numbers
    */
    firefox: function (e) {
      return e.stack.replace(/(?:\n@:0)?\s+$/m, '').replace(/^\(/gm, '{anonymous}(').split('\n');
    },

    opera11: function (e) {
      // "Error thrown at line 42, column 12 in <anonymous function>() in file://localhost/G:/js/stacktrace.js:\n"
      // "Error thrown at line 42, column 12 in <anonymous function: createException>() in file://localhost/G:/js/stacktrace.js:\n"
      // "called from line 7, column 4 in bar(n) in file://localhost/G:/js/test/functional/testcase1.html:\n"
      // "called from line 15, column 3 in file://localhost/G:/js/test/functional/testcase1.html:\n"
      var ANON = '{anonymous}', lineRE = /^.*line (\d+), column (\d+)(?: in (.+))? in (\S+):$/;
      var lines = e.stacktrace.split('\n'), result = [];

      for (var i = 0, len = lines.length; i < len; i += 2) {
        var match = lineRE.exec(lines[i]);
        if (match) {
          var location = match[4] + ':' + match[1] + ':' + match[2];
          var fnName = match[3] || "global code";
          fnName = fnName.replace(/<anonymous function: (\S+)>/, "$1").replace(/<anonymous function>/, ANON);
          result.push(fnName + '@' + location + ' -- ' + lines[i + 1].replace(/^\s+/, ''));
        }
      }

      return result;
    },

    opera10b: function (e) {
      // "<anonymous function: run>([arguments not available])@file://localhost/G:/js/stacktrace.js:27\n" +
      // "printStackTrace([arguments not available])@file://localhost/G:/js/stacktrace.js:18\n" +
      // "@file://localhost/G:/js/test/functional/testcase1.html:15"
      var ANON = '{anonymous}', lineRE = /^(.*)@(.+):(\d+)$/;
      var lines = e.stacktrace.split('\n'), result = [];

      for (var i = 0, len = lines.length; i < len; i++) {
        var match = lineRE.exec(lines[i]);
        if (match) {
          var fnName = match[1] ? (match[1] + '()') : "global code";
          result.push(fnName + '@' + match[2] + ':' + match[3]);
        }
      }

      return result;
    },

    /**
    * Given an Error object, return a formatted Array based on Opera 10's stacktrace string.
    *
    * @param e - Error object to inspect
    * @return Array<String> of function calls, files and line numbers
    */
    opera10a: function (e) {
      // "  Line 27 of linked script file://localhost/G:/js/stacktrace.js\n"
      // "  Line 11 of inline#1 script in file://localhost/G:/js/test/functional/testcase1.html: In function foo\n"
      var ANON = '{anonymous}', lineRE = /Line (\d+).*script (?:in )?(\S+)(?:: In function (\S+))?$/i;
      var lines = e.stacktrace.split('\n'), result = [];

      for (var i = 0, len = lines.length; i < len; i += 2) {
        var match = lineRE.exec(lines[i]);
        if (match) {
          var fnName = match[3] || ANON;
          result.push(fnName + '()@' + match[2] + ':' + match[1] + ' -- ' + lines[i + 1].replace(/^\s+/, ''));
        }
      }

      return result;
    },

    // Opera 7.x-9.2x only!
    opera9: function (e) {
      // "  Line 43 of linked script file://localhost/G:/js/stacktrace.js\n"
      // "  Line 7 of inline#1 script in file://localhost/G:/js/test/functional/testcase1.html\n"
      var ANON = '{anonymous}', lineRE = /Line (\d+).*script (?:in )?(\S+)/i;
      var lines = e.message.split('\n'), result = [];

      for (var i = 2, len = lines.length; i < len; i += 2) {
        var match = lineRE.exec(lines[i]);
        if (match) {
          result.push(ANON + '()@' + match[2] + ':' + match[1] + ' -- ' + lines[i + 1].replace(/^\s+/, ''));
        }
      }

      return result;
    },

    // Safari, IE, and others
    other: function (curr) {
      var ANON = '{anonymous}', fnRE = /function\s*([\w\-$]+)?\s*\(/i, stack = [], fn, args, maxStackSize = 10;
      while (curr && stack.length < maxStackSize) {
        fn = fnRE.test(curr.toString()) ? RegExp.$1 || ANON : ANON;
        args = Array.prototype.slice.call(curr['arguments'] || []);
        stack[stack.length] = fn + '(' + this.stringifyArguments(args) + ')';
        curr = curr.caller;
      }
      return stack;
    },

    /**
    * Given arguments array as a String, subsituting type names for non-string types.
    *
    * @param {Arguments} object
    * @return {Array} of Strings with stringified arguments
    */
    stringifyArguments: function (args) {
      var result = [];
      var slice = Array.prototype.slice;
      for (var i = 0; i < args.length; ++i) {
        var arg = args[i];
        if (arg === undefined) {
          result[i] = 'undefined';
        } else if (arg === null) {
          result[i] = 'null';
        } else if (arg.constructor) {
          if (arg.constructor === Array) {
            if (arg.length < 3) {
              result[i] = '[' + this.stringifyArguments(arg) + ']';
            } else {
              result[i] = '[' + this.stringifyArguments(slice.call(arg, 0, 1)) + '...' + this.stringifyArguments(slice.call(arg, -1)) + ']';
            }
          } else if (arg.constructor === Object) {
            result[i] = '#object';
          } else if (arg.constructor === Function) {
            result[i] = '#function';
          } else if (arg.constructor === String) {
            result[i] = '"' + arg + '"';
          } else if (arg.constructor === Number) {
            result[i] = arg;
          }
        }
      }
      return result.join(',');
    },

    sourceCache: {},

    /**
    * @return the text from a given URL
    */
    ajax: function (url) {
      var req = this.createXMLHTTPObject();
      if (req) {
        try {
          req.open('GET', url, false);
          req.send(null);
          return req.responseText;
        } catch (e) {
        }
      }
      return '';
    },

    /**
    * Try XHR methods in order and store XHR factory.
    *
    * @return <Function> XHR function or equivalent
    */
    createXMLHTTPObject: function () {
      var xmlhttp, XMLHttpFactories = [
            function () {
              return new XMLHttpRequest();
            }, function () {
              return new ActiveXObject('Msxml2.XMLHTTP');
            }, function () {
              return new ActiveXObject('Msxml3.XMLHTTP');
            }, function () {
              return new ActiveXObject('Microsoft.XMLHTTP');
            }
        ];
      for (var i = 0; i < XMLHttpFactories.length; i++) {
        try {
          xmlhttp = XMLHttpFactories[i]();
          // Use memoization to cache the factory
          this.createXMLHTTPObject = XMLHttpFactories[i];
          return xmlhttp;
        } catch (e) {
        }
      }
    },

    /**
    * Given a URL, check if it is in the same domain (so we can get the source
    * via Ajax).
    *
    * @param url <String> source url
    * @return False if we need a cross-domain request
    */
    isSameDomain: function (url) {
      return url.indexOf(location.hostname) !== -1;
    },

    /**
    * Get source code from given URL if in the same domain.
    *
    * @param url <String> JS source URL
    * @return <Array> Array of source code lines
    */
    getSource: function (url) {
      // NOTE reuse source from script tags?
      if (!(url in this.sourceCache)) {
        this.sourceCache[url] = this.ajax(url).split('\n');
      }
      return this.sourceCache[url];
    },

    guessAnonymousFunctions: function (stack) {
      for (var i = 0; i < stack.length; ++i) {
        var reStack = /\{anonymous\}\(.*\)@(.*)/,
                reRef = /^(.*?)(?::(\d+))(?::(\d+))?(?: -- .+)?$/,
                frame = stack[i], ref = reStack.exec(frame);

        if (ref) {
          var m = reRef.exec(ref[1]), file = m[1],
                    lineno = m[2], charno = m[3] || 0;
          if (file && this.isSameDomain(file) && lineno) {
            var functionName = this.guessAnonymousFunction(file, lineno, charno);
            stack[i] = frame.replace('{anonymous}', functionName);
          }
        }
      }
      return stack;
    },

    guessAnonymousFunction: function (url, lineNo, charNo) {
      var ret;
      try {
        ret = this.findFunctionName(this.getSource(url), lineNo);
      } catch (e) {
        ret = 'getSource failed with url: ' + url + ', exception: ' + e.toString();
      }
      return ret;
    },

    findFunctionName: function (source, lineNo) {
      // NOTE findFunctionName fails for compressed source
      // (more than one function on the same line)
      // NOTE use captured args
      // function {name}({args}) m[1]=name m[2]=args
      var reFunctionDeclaration = /function\s+([^(]*?)\s*\(([^)]*)\)/;
      // {name} = function ({args}) NOTE args capture
      // /['"]?([0-9A-Za-z_]+)['"]?\s*[:=]\s*function(?:[^(]*)/
      var reFunctionExpression = /['"]?([0-9A-Za-z_]+)['"]?\s*[:=]\s*function\b/;
      // {name} = eval()
      var reFunctionEvaluation = /['"]?([0-9A-Za-z_]+)['"]?\s*[:=]\s*(?:eval|new Function)\b/;
      // Walk backwards in the source lines until we find
      // the line which matches one of the patterns above
      var code = "", line, maxLines = Math.min(lineNo, 20), m, commentPos;
      for (var i = 0; i < maxLines; ++i) {
        // lineNo is 1-based, source[] is 0-based
        line = source[lineNo - i - 1];
        commentPos = line.indexOf('//');
        if (commentPos >= 0) {
          line = line.substr(0, commentPos);
        }
        // NOTE check other types of comments? Commented code may lead to false positive
        if (line) {
          code = line + code;
          m = reFunctionExpression.exec(code);
          if (m && m[1]) {
            return m[1];
          }
          m = reFunctionDeclaration.exec(code);
          if (m && m[1]) {
            //return m[1] + "(" + (m[2] || "") + ")";
            return m[1];
          }
          m = reFunctionEvaluation.exec(code);
          if (m && m[1]) {
            return m[1];
          }
        }
      }
      return '(?)';
    }
  };

  ctx.Rjs.define(['lib/moment', 'utils/js/stringUtil'], function (moment, StringUtil){
    var _loggerMap = new Map();
    var _globalLogger = createLogger('ARIA');

    // default writer for use with VF pages implementing remote objects
    _globalLogger.setDebugInfoWriter(function(debugInfoDetails) {
      var debugInfoSO = new SObjectModel.DebugInfo();

      debugInfoSO.create(debugInfoDetails, function (err) {
        if (err) {
          var fatalErrorMsg = StringUtil.format("FATAL: Failed to report error! Message: {0}, ErrorMessage: {1}", debugInfoDetails.Data, err.message);
          if (window.console) {
            window.console[level](fatalErrorMsg);
          } else {
            alert(fatalErrorMsg);
          }
        }
      });
    });

    function createLogger(pageId) {
      var _pageId = pageId;
      var _logStackSize = 50;
      var _logStack = [];
      var _proxy = window.console;
      var _debugInfoWriter = null;

      function log(level, message) {
        let msgToLog = message;

        if (_proxy) {
          // There is an issue with IE 11 where the log level based functions are not available. Therefore reverting to the standard log() function.
          if (!_proxy[level]) {
            level = "log";
          }

          try {
            _proxy[level] && _proxy[level](_pageId + " | " + msgToLog);
          } catch (ex) {
            if (window.console) {
              window.console['error']('Failed to log statement: ' + msgToLog);
            }
          }
        }

        if (_logStack.length === _logStackSize) {
          _logStack.shift();
        }

        _logStack.push(moment().format('YYYY-MM-DD HH:mm:ss.SSSZZ') + ' - ' + message);

        if (level === "error" && _debugInfoWriter != null) {
          var debugInfo = "Browser: " + navigator.userAgent
            + "\nStacktrace: " + printStackTrace() + "\n"
            + "\nLogs:";

          while (_logStack.length > 0) {
            debugInfo = debugInfo + _logStack.pop() + "\n";
          }

          var debugInfoDetails = {
            Component: _pageId,
            Data: msgToLog,
            ExtraInfo: debugInfo,
            Level: StringUtil.capitalizeFirstLetter(level)
          };

          try {
            _debugInfoWriter(debugInfoDetails);
          } catch (ex) {
            var fatalErrorMsg = StringUtil.format("FATAL: Failed to report error! Message: {0}, Exception: {1}", msgToLog, JSON.stringify(ex));
            if (window.console) {
              window.console[level](fatalErrorMsg);
            } else {
              alert(fatalErrorMsg);
            }
          }
        }
      }

      return {
        setPageId: function (pageId) {
          _pageId = pageId;
        },

        getPageId: function() {
          return _pageId;
        },

        setDebugInfoWriter: function(writer) {
          _debugInfoWriter = writer;
        },

        setLogStackSize: function(newSize) {
          _logStackSize = newSize;
        },

        forwardLogsTo: function (proxy) {
          if (proxy) {
            _proxy = proxy;
          }
        },

        debug: function () {
          log("debug", StringUtil.format.apply(null, arguments));
        },

        info: function () {
          log("info", StringUtil.format.apply(null, arguments));
        },

        warn: function () {
          log("warn", StringUtil.format.apply(null, arguments));
        },

        error: function () {
          log("error", StringUtil.format.apply(null, arguments));
        }
      };
    }

    function getLogger(loggerId) {
      var logger = _loggerMap.get(loggerId);

      if (logger === undefined) {
        logger = createLogger(loggerId);
        _loggerMap.set(loggerId, logger);
      }

      return logger;
    }

    return {
      getLogger: getLogger,

      setPageId: function (pageId) {
        _globalLogger.setPageId(pageId);
      },

      getPageId: function() {
        return _globalLogger.getPageId();
      },

      setLogStackSize: function(newSize) {
        _globalLogger.setLogStackSize(newSize);
      },

      forwardLogsTo: function(proxy) {
        _globalLogger.forwardLogsTo(proxy);
      },

      debug: function () {
        _globalLogger.debug.apply(null, arguments);
      },

      info: function () {
        _globalLogger.info.apply(null, arguments);
      },

      warn: function () {
        _globalLogger.warn.apply(null, arguments);
      },

      error: function () {
        _globalLogger.error.apply(null, arguments);
      }
    };
  });
})(this);