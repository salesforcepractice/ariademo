sforce.console.setCustomConsoleComponentPopoutable(true);
sforce.interaction.setVisible(true);

phoneReady.then(function(p) {
  var phone = p.phone;
  var ui = p.ui;

  function setConsoleText() {
    var text;
    if(phone.calls.length === 0)
      text = "No Call";
    else if(ui.screen.contactCard)
      text = ui.screen.contactCard.header;
    else if(ui.screen.callId)
      text = phone.calls.get(ui.screen.callId).otherParties[0].number;
    
    sforce.console.setCustomConsoleComponentButtonText(text);
  }

  sforce.interaction.cti.setSoftphoneHeight(p.ccSettings["/reqGeneralInfo/reqSoftphoneHeight"]);
  sforce.interaction.cti.setSoftphoneWidth(p.ccSettings["/reqGeneralInfo/reqSoftphoneWidth"]);
  
  ui.screenChanged.subscribe(function() {
    setConsoleText();
  });
  setConsoleText();

  function setUiLabels(call) {
    setContactCard(call).then(function() {
      setConsoleText();
    });
  }
  phone.calls.added.subscribe(setUiLabels);
});
