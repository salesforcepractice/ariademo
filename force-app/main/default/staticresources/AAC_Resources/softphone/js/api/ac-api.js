/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */
if (typeof (window.aria) === "undefined") {
  window.aria = {
    log: {
      error: function (message) {
        console.log("AcApi ERROR > " + message);
      },
      info: function (message) {
        console.log("AcApi INFO > " + message);
      },
      debug: function (message) {
        console.log("AcApi DEBUG > " + message);
      }
    }
  };
}

window.aria.ac = function () {
  let __softphoneFrame = null;
  let __registrationPromise = new Promise(function (resolve, reject) {

    /************************************************************
     *********************** REGISTRATION ***********************
     ************************************************************/
    var registerWindow = function (args, postbackFunction, senderWindow) {
      aria.log.info("AcAPI:registerWindow invoked");
      __softphoneFrame = senderWindow;
      postbackFunction();
      resolve();
      aria.log.info("AcAPI:registerWindow done");
    };

    aria.crossWindowMessage.registerHandler("Ac.Api.Event.InitializeApi", registerWindow, "*");
  });

  function sendCommand(name, args) {
    aria.log.info("AcAPI:sendCommand invoked");
    function doSendCommand() {
      return new Promise(function (resolve, reject) {
        aria.crossWindowMessage.post({
          targetWindow: __softphoneFrame,
          command: name,
          parameters: args,
          targetDomain: "*",
          timeout: 3000,
          successCallback: function(result){
            resolve(result);
          },
          errorCallback: function (ex) {
            reject(ex);
          }
        });
      });
    }

    if (!__softphoneFrame) {
      return __registrationPromise.then(doSendCommand)
    } else {
      return doSendCommand();
    }
  }

  return {
    isSoftphoneRegistered: function () {
      return !!__softphoneFrame;
    },

    getAttributes: function () {
      aria.log.info('AcAPI:getAttributes');
      return sendCommand('Ac.Api.Request.GetAttributes', {});
    },

    makeCall: function (number) {
      aria.log.info('AcAPI:makeCall');
      return sendCommand('Ac.Api.Request.MakeCall', {
        number: number
      });
    },
  }
}();
