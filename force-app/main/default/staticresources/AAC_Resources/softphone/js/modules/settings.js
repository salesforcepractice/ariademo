/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'utils/js/logUtil', 'console', 'connect'], function ($, LogUtil, sfConsole, connect) {
    LogUtil.info("Settings initializing");

    if (sfConsole && !sfConsole.isInConsole()) {
      LogUtil.info("MuteButton:onAgentHandler not in console");
      return;
    }

    $("#downloadLogsBtn").click(function () {
      connect.getLog().download();
    });
  });
})(this);