/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'lib/libphonenumber', 'utils/js/logUtil', 'utils/js/notificationUtil', 'UTILS/softphone-settings', 'connect', 'cti-wrapper'], function ($, Promise, PhoneLib, LogUtil, NotificationUtil, Settings, connect, cti) {
    LogUtil.info("Click-to-dial initializing");

    LogUtil.debug("Disabling click-to-dial");
    cti.disableClickToDial();

    Settings.getStringValues().then(function (ccSettings) {
      LogUtil.debug("Call Center Settings successfully retrieved");
      var connectPhoneFormat = JSON.parse(ccSettings["/reqConnectSFCCPOptions/reqConnectPhoneFormat"]);

      connect.agent(function (agent) {
        LogUtil.debug("Enabling click-to-dial");
        cti.enableClickToDial();

        cti.onClickToDial(function (data) {
            LogUtil.debug("Click-to-dial event received: {0}", JSON.stringify(data));

            var phoneParsed = PhoneLib.parse(data.number, { country: { default: connectPhoneFormat.Country }});
            LogUtil.info("Parsed Phonenumber: " + phoneParsed.country + "|" + phoneParsed.phone);
            var e164PhoneNumber = PhoneLib.format(phoneParsed.phone, phoneParsed.country, connectPhoneFormat.NF);
            LogUtil.info("h164Number: " + e164PhoneNumber);

            agent.connect(connect.Address.byPhoneNumber(e164PhoneNumber), {
                failure: function (e) {
                    LogUtil.error('Failed to initiate call: {0}', e);
                    if (e) {
                        var message = JSON.parse(e).message || e;
                        NotificationUtil.notifyOrAlert({
                            title: 'Outbound call failed!',
                            body: message,
                            timeout: 5000
                        });
                    }
                }
            });
        });
      });
    });
  });
})(this);