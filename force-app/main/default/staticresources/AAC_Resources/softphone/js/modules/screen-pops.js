/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  let _SCREEN_POP_RECORD_ATTRIBUTE_KEY = 'actoolkit-pop-record';
  let _SCREEN_POP_SEARCH_ATTRIBUTE_KEY = 'actoolkit-pop-search';
  let _SCREEN_POP_URL_ATTRIBUTE_KEY = 'actoolkit-pop-url';

  ctx.Rjs.define(['lib/promise', 'utils/js/logUtil', 'lib/libphonenumber', 'UTILS/softphone-settings', 'connect', 'console', 'cti-wrapper', 'MODULES/call-information'], function (Promise, LogUtil, PhoneLib, Settings, connect, sfConsole, cti, CallInformation) {
    LogUtil.info("ScreenPopManager:initializing");
    function setSoftphoneVisible() {
      cti.showSoftphone();
    }

    function ctiSearchAndPop(searchParams, queryParams, callType) {
      cti.searchAndScreenPop(searchParams, queryParams, callType);
    }

    function popCtiUrl(url) {
      cti.screenPopUrl(url);
    }

    function searchAndPopAni(contact, connectPhoneFormat ) {
      LogUtil.info("ScreenPopManager:searchAndPopAni:InboundPhone:Invoked");
      var phoneNumber = contact.getInitialConnection().getAddress().phoneNumber;
      LogUtil.info("ScreenPopManager:searchAndPopAni:InboundPhone:PN:" + phoneNumber);
      var phoneParsed = PhoneLib.parse(phoneNumber, { country: { default: connectPhoneFormat.Country } });
      LogUtil.info("ScreenPopManager:searchAndPopAni:InboundPhone:Parsed:"+phoneParsed.country + "|" + phoneParsed.phone);

      LogUtil.info("ScreenPopManager:searchAndPopAni:ScreenPopSrch");
      ctiSearchAndPop(phoneParsed.phone, '', 'inbound');
    }

    function popRecordOrSearch(contact, connectPhoneFormat ) {
      var attributes = contact.getAttributes();
      LogUtil.info("ScreenPopManager:popRecordOrSearch:attributes:" + JSON.stringify(attributes));

      var screenPopUrlAttribute = attributes[_SCREEN_POP_URL_ATTRIBUTE_KEY];
      if (screenPopUrlAttribute && screenPopUrlAttribute.value) {
        LogUtil.info("ScreenPopManager:popRecordOrSearch:popUrlFound:" + screenPopUrlAttribute.value);
        popCtiUrl(screenPopUrlAttribute.value);
        return;
      }

      var screenPopRecordAttribute = attributes[_SCREEN_POP_RECORD_ATTRIBUTE_KEY];
      if (screenPopRecordAttribute && screenPopRecordAttribute.value) {
        LogUtil.info("ScreenPopManager:popRecordOrSearch:popRecordFound:" + screenPopRecordAttribute.value);
        popCtiUrl("/" + screenPopRecordAttribute.value);
        return;
      }

      var screenPopSearchAttribute = attributes[_SCREEN_POP_SEARCH_ATTRIBUTE_KEY];
      if (screenPopSearchAttribute && screenPopSearchAttribute.value) {
        LogUtil.info("ScreenPopManager:popRecordOrSearch:popSearchFound:" + screenPopSearchAttribute.value);
        ctiSearchAndPop(screenPopSearchAttribute.value, '', 'inbound');
        return;
      }

      searchAndPopAni(contact, connectPhoneFormat);
    }

    connect.contact(function (con){
      con.onConnecting(function(contact) {
        LogUtil.info("ScreenPopManager:onConnecting");
        setSoftphoneVisible();

        if (sfConsole && !sfConsole.isInConsole()) {
          LogUtil.warn("ScreenPopManager:onConnecting Screen pops not supported outside of sfConsole");
          return;
        }

        if (!contact.isInbound() && contact.getType() !== 'queue_callback') {
          LogUtil.warn("ScreenPopManager:onConnecting Outbound call detected, no screen pop will be performed");
          return;
        }

        Settings.getStringValues().then(function (ccSettings) {
          let connectPhoneFormat = JSON.parse(ccSettings["/reqConnectSFCCPOptions/reqConnectPhoneFormat"]);

          CallInformation.getWorkspaceUrl(
            contact,
            function (result) {
              try {
                LogUtil.info("ScreenPopManager:getWorkspaceUrl:onSuccess");
                if(result.url !== undefined) {
                  !ctx.ForceUI.IsLightning && sfConsole.openConsoleUrl(null, result.url, true,  undefined, [contact.getContactId()], function (openConsoleUrlResult) {
                    LogUtil.info("ScreenPopManager:opensfConsoleUrlResult success=" + openConsoleUrlResult.success);
                    if (!openConsoleUrlResult.success) {
                      searchAndPopAni(contact, connectPhoneFormat);
                    }
                  });

                  cti.screenPopUrl(result.url);
                } else {
                  popRecordOrSearch(contact, connectPhoneFormat)
                }
              } catch(e){
                LogUtil.info("ScreenPopManager:getWorkspaceUrl:onSuccess:Error " + e);
              }
            },
            function (errorMsg) {
              LogUtil.info("ScreenPopManager:getWorkspaceUrl:onError " + errorMsg);
              popRecordOrSearch(contact, connectPhoneFormat);
            }
          );
        });
      });
    });

    return {
      getScreenPopAttributeKey: function () {
        return _SCREEN_POP_RECORD_ATTRIBUTE_KEY;
      }
    }
  });
})(this);