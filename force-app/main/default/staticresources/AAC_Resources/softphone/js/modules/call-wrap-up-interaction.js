/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'utils/js/logUtil', 'lib/libphonenumber', 'connect', 'console', 'cti-wrapper', 'MODULES/screen-pops', 'UTILS/lightning-omni-api'], function ($, Promise, LogUtil, PhoneLib, connect, sfConsole, cti, ScreenPopController, ltngApi) {
    LogUtil.info("PostCallWrapUpInteraction:initializing");

    if (sfConsole && !sfConsole.isInConsole()) {
      LogUtil.info("PostCallWrapUpInteraction:onAgentHandler not in console");
      return;
    }

    var _namespacePrefix = ctx.ForceUI.NamespacePrefix;
    sfConsole.addEventListener(sfConsole.ConsoleEvent.OPEN_TAB, getTabObjectId);

    function startActiveCall() {
      LogUtil.info("PostCallWrapUpInteraction:onAgentHandler:startActiveCall invoked");

      setCallContextProperty("callActive", true);

      var callStartdate = new Date();

      setCallContextProperty("callStartTime", new Date().getTime());
      setCallContextProperty("callStartdate", callStartdate.toISOString());
      setCallContextProperty("callStartDateTime", callStartdate.toISOString().substr(0, 19).replace("T", " "));
    }

    function createTask(callContact, callContext) {
      LogUtil.info("PostCallWrapUpInteraction:createSegment invoked");
      if(callContact.isInbound()){
        var attributes = callContact.getAttributes();
        LogUtil.info("PostCallWrapUpInteraction:attributes: "+JSON.stringify(attributes));
        var attContactId;
        if(attributes.hasOwnProperty("interaction_id")){
          LogUtil.info("PostCallWrapUpInteraction: Entered if statement");
          attContactId = attributes["interaction_id"].value;
        }
        LogUtil.info("PostCallWrapUpInteraction:interaction_id "+ attContactId);
      }
      var properties = JSON.stringify({contactId: callContact.contactId, originalContactId: callContact.getOriginalContactId(), attId: attContactId});
      LogUtil.info("PostCallWrapUpInteraction:properties "+properties);

      Visualforce.remoting.Manager.invokeAction(
        'AAC_PostCallSegmentCreationController.createInteractionSegment',
        properties,
        function(result, event) {

          LogUtil.info("PostCallWrapUpInteraction:RemoteAction: createSegment saved. Id=" + result.Id);
          var recordIds = sessionStorage.getItem("CCP-tabObjectIds")
          var taskURL = "/apex/" + _namespacePrefix + "ASP_PostWorkUpdateInteraction?interaction_segment_Id=" + result.Interaction_Segment_Id__c + "&objectIds=" + recordIds;
          LogUtil.info("PostCallWrapUpInteraction:RemoteAction URL "+ taskURL);
          sfConsole.getFocusedPrimaryTabId(function(result){
            var primaryTabId = result.id;
            if (primaryTabId && primaryTabId !== "null"){
              sfConsole.openSubtab(primaryTabId , taskURL, true, "Work Wrap Up", null, openWorkingTab);
            } else {
              sfConsole.openPrimaryTab(null, taskURL, true, "Work Wrap Up", openWorkingTab);
            }
          });
        }
      );
    }

    function openWorkingTab(result) {
      LogUtil.info("PostCallWrapUpInteraction:openWorkingTab invoked");
      if (result.success) {
        sfConsole.ConsoleEvent.CLOSE_TAB,
        onTabClose,
        { tabId : result.id }
        sfConsole.disableTabClose(true, result.id);
      }
      else {
        LogUtil.error("PostCallWrapUpInteraction:openWorkingTab unable to open tab");
      }
    }

    function onTabClose(result) {
      connect.agent(function(agent) {
        LogUtil.info("PostCallWrapUpInteraction:onTabClose invoked");
        var availableState = agent.getAgentStates().filter(function(state) {
          return state.name === "Available";
        })[0];
        agent.setState(availableState, {
          success : function() {
            LogUtil.info("PostCallWrapUpInteraction:onTabClose agent state set to Available");
          },
          failure : function() {
            LogUtil.error("PostCallWrapUpInteraction:onTabClose unable to set agent state to Available");
          }
        });
      });

      sfConsole && sfConsole.removeEventListener(
        sfConsole.ConsoleEvent.CLOSE_TAB,
        onTabClose,
        { tabId : result.id }
      );
    }

    function setCallContextProperty(name, value) {
      LogUtil.info(
        "PostCallWrapUpInteraction:setCallContextProperty setting call context property " +
        name + " to " + value
      );
      sessionStorage.setItem("CCP-" + name, value);
    }

    function clearCallContext() {
      LogUtil.info("PostCallWrapUpInteraction:clearCallContext clearing all call context");
      sessionStorage.removeItem("CCP-callActive");
      sessionStorage.removeItem("CCP-callQueue");
      sessionStorage.removeItem("CCP-callType");
      sessionStorage.removeItem("CCP-whoId");
      sessionStorage.removeItem("CCP-callStartTime");
      sessionStorage.removeItem("CCP-callEndTime");
      sessionStorage.removeItem("CCP-callStartdate");
      sessionStorage.removeItem("CCP-callStartDateTime");
      sessionStorage.removeItem("CCP-callPhoneNumber");
      sessionStorage.removeItem("CCP-TabObjectIds")
    }

    function getCurrentCallContext() {
      var result = {
        callActive: sessionStorage.getItem("CCP-callActive"),
        callQueue: sessionStorage.getItem("CCP-callQueue"),
        callType: sessionStorage.getItem("CCP-callType"),
        whoId: sessionStorage.getItem("CCP-whoId"),
        callStartTime: sessionStorage.getItem("CCP-callStartTime"),
        callEndTime: sessionStorage.getItem("CCP-callEndTime"),
        callStartdate: sessionStorage.getItem("CCP-callStartdate"),
        callStartDateTime: sessionStorage.getItem("CCP-callStartDateTime"),
        callPhoneNumber: sessionStorage.getItem("CCP-callPhoneNumber")
      };
      LogUtil.info("PostCallWrapUpInteraction:getCurrentCallContext Current call context: {0}", JSON.stringify(result));
      return result;
    }

    function getTabObjectId(result){

      LogUtil.info("PostCallWrapUpInteraction:tabOpened with object Id "+ result.objectId);

      if(!sessionStorage.getItem("CCP-callType")){
        LogUtil.info("PostCallWrapUpInteraction:No active call not logging at this time");

        //extra clean up in the rare case it isn't removed at the end of a call
        if(sessionStorage.getItem("CCP-tabObjectIds")){
          sessionStorage.removeItem("CCP-tabObjectIds");
        }
        return;
      }

      LogUtil.info("PostCallWrapUpInteraction:Active Call. Logging tab ids");

      var objectIds = "";
      if(sessionStorage.getItem("CCP-tabObjectIds"))
      {
        objectIds += sessionStorage.getItem("CCP-tabObjectIds");
        if(!objectIds.includes(result.objectId)){
          objectIds += ","+result.objectId;
        }
      }
      else{
        objectIds += result.objectId;
      }

      sessionStorage.setItem("CCP-tabObjectIds", objectIds);

    }

    connect.contact(function(contact) {
      var conns = contact.getConnections();
      var custConn = conns.find(
        c => c.getType() === connect.ConnectionType.INBOUND ||
        c.getType() === connect.ConnectionType.OUTBOUND
        );
      if (!custConn)
        return;

      setCallContextProperty('callType', contact.isInbound() ? 'Inbound' : 'Outbound');

      var phoneNumber = custConn.getEndpoint().phoneNumber;
      var containsAtSymbol = phoneNumber.indexOf('@') > -1;
      setCallContextProperty('callPhoneNumber', phoneNumber.substring(0, containsAtSymbol ? phoneNumber.indexOf('@') : phoneNumber.length).replace('sip:', ''));

      contact.onAccepted(function(contactOnAccepted) {
        LogUtil.info("PostCallWrapUpInteraction:onAgentHandler:ContactOnAcceptedHandler invoked");
        startActiveCall();
      });

      contact.onConnected(function(contactOnConnected) {
        LogUtil.info("PostCallWrapUpInteraction:onAgentHandler:ContactOnConnectedHandler invoked");

        /*
         * Due to how callback works need to add a check so that it triggers
         * the startActiveCall() method. Callbacks are always inbound but hit
         * here when connecting to the customer so need to add a check to see
         */
        if (!contactOnConnected.isInbound() || contactOnConnected.getType() === 'queue_callback') {
          startActiveCall();
        }
      });

      contact.onEnded(function(contactOnEnded) {
        LogUtil.info("PostCallWrapUpInteraction:onAgentHandler:ContactOnEndedHandler invoked");
        setCallContextProperty("callEndTime", new Date().getTime());
        setCallContextProperty("callQueue", contact.getQueue().name);

        var callContext = getCurrentCallContext();

        clearCallContext();
        connect.agent(function(agent){
          var state = agent.getState().name;
          LogUtil.info("PostCallWrapUpInteraction:AgentStates "+state);
          /* Callbacks in AC are represented in two different calls. An inbound call that establishes the
           * connection, which is then transitioned to an outbound call dialing the customer. Since the
           * inbound call will end, we don't want to have a task recorded at that stage, but at the end
           * of the outbound call. The ACW state indicates to us that the main call is completed.
           */
          if (callContext.callActive && state === 'AfterCallWork') {
            createTask(contactOnEnded, callContext);
          }
        });

      });
    });
  });
})(this);