/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/jquery', 'lib/promise', 'utils/js/logUtil', 'lib/libphonenumber', 'UTILS/softphone-settings', 'connect', 'cti-wrapper', 'UTILS/ac-lightning-api'], function ($, Promise, LogUtil, PhoneLib, Settings, connect, cti, ltngApi) {
    LogUtil.info("acApiHandler initializing");

    var _activeContact = null;

    connect.contact(function(contact) {
      contact.onConnected(function () {
        _activeContact = contact;
      });

      contact.onEnded(function () {
        _activeContact = null;
      });
    });

    ltngApi.requests && ltngApi.requests.getAttributes.bind(function (args, postbackFunction) {
      LogUtil.info("acApiHandler:Request:getAttributes");
      _activeContact === null ?
        postbackFunction({exception: "No active contact available"}) :
        postbackFunction({attributes: _activeContact.getAttributes() });
    });

    ltngApi.requests && ltngApi.requests.makeCall.bind(function (args, postbackFunction) {
      var phoneNumber = args.number;
      LogUtil.info("acApiHandler:Request:makeCall:" + phoneNumber);

      connect.agent(function(agent){
        Settings.getStringValues().then(function (ccSettings) {
          var connectPhoneFormat = JSON.parse(ccSettings["/reqConnectSFCCPOptions/reqConnectPhoneFormat"]);
          var phoneParsed = PhoneLib.parse(phoneNumber, { country: { default: connectPhoneFormat.Country }});

          LogUtil.info("CallCampaign:DialAgent:Parsed:"+phoneParsed.country+"|"+phoneParsed.phone);
          var e164PhoneNumber = PhoneLib.format(phoneParsed.phone, phoneParsed.country, connectPhoneFormat.NF);

          LogUtil.info("CallCampaign:DialAgent:h164Number:"+ e164PhoneNumber);
          agent.connect(connect.Address.byPhoneNumber(e164PhoneNumber),{});

          cti.showSoftphone();

          postbackFunction();
        });
      });
    });
  });
})(this);