/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/promise', 'utils/js/logUtil', 'x-window'], function (Promise, LogUtil, XWindow) {
    LogUtil.info("AcLightningApi initializing");

    if (!ctx.ForceUI.IsLightning) {
      LogUtil.info("AcLightningApi not in lightning - disabled");
      return {};
    }

    var Event = function (eventName) {
      var _handlers = [];

      var fire = function (data, postbackFunction) {
        for (var i = 0, length = _handlers.length; i < length; i++)
          _handlers[i](data, postbackFunction);
      }

      XWindow.registerHandler(eventName, fire, "*");

      return {
        bind: function (handler) {
          _handlers.push(handler);
        },
        unbind: function (handler) {
          for (var i = 0, length = _handlers.length; i < length; i++) {
            if (_handler[i] == handler) {
              delete _handlers[i];
              return;
            }
          }
        }
      };
    };

    var intervalCount = 0;
    var initIntervalId = setInterval(function (){
      XWindow.post({
        targetWindow: window.parent,
        command: "Ac.Api.Event.InitializeApi",
        targetDomain: "*",
        successCallback: function () {
          LogUtil.info("AcLightningAPI: Registering window with AcAPI");
          clearInterval(initIntervalId);
        },
        errorCallback: function (message) {
          //LogUtil.info("AcLightningAPI: Error registering window with AcAPI");
          intervalCount++;
          if (intervalCount => 30) {
            clearInterval(initIntervalId);
          }
        }
      });
    }, 1000);

    return {
      requests: {
        getAttributes: new Event('Ac.Api.Request.GetAttributes'),
        makeCall: new Event('Ac.Api.Request.MakeCall'),
      }
    };
  });
})(this);