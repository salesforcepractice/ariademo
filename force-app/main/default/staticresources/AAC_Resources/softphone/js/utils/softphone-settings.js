/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belowing to
 * the customer require a written permission from Aria Solutions.
 */

(function (ctx) {
  ctx.Rjs.define(['lib/promise', 'utils/js/logUtil', 'cti-wrapper'], function (Promise, LogUtil, cti) {
    LogUtil.info("SoftphoneSettings:initializing");

    let _settingsPromise = cti.getCallCenterSettings().catch(function (ex) {
      LogUtil.error("Failed to return settings: " + ex.message, ex);
    });

    return {
      getStringValues: function () {
        return _settingsPromise;
      }
    };
  });
})(this);