var phoneReady = new Promise(function(resolve, reject) {
  sforce.interaction.cti.getCallCenterSettings(function(result) {
    if(!result.result) {
      return reject("Salesforce did not return call center settings -- verify call center definition");
    }
    var ccSettings = JSON.parse(result.result);

    var vendor = ccSettings["/reqGeneralInfo/reqCtiVendor"];

    var phone;
    if(vendor.match(/CiscoFinesse/i)) {
      var endpoint = ccSettings["/reqGeneralInfo/reqCtiEndpoint"];
      var eventTunnel = ccSettings["/reqGeneralInfo/reqCtiEventTunnel"];

      phone = softphone.phone.CiscoFinesse(endpoint, eventTunnel);
    } else if(vendor.match(/test/i)) {
      phone = softphone.phone.Test(1);
    } else {
      return reject("Invalid vendor cannot create phone");
    }
      
      phone = softphone.phone.Demo("ws://localhost:10000");

    var ui = softphone.Ui(phone, document.getElementById("phone")).quickRestore();

    resolve({ phone: phone, ui: ui, ccSettings: ccSettings });
  });
});

phoneReady.then(function(p) {
  var phone = p.phone;
  var ui = p.ui;

  /**
   * Salesforce CTI integration setup for click to dial
   */
  sforce.interaction.cti.onClickToDial(function (r) {
    var data = JSON.parse(r.result);
    phone.dial(cleanPhoneNumber(data.number));
  });
  
  phone.loggedIn.subscribe(function() {
    sforce.interaction.cti.enableClickToDial();
  });
  phone.loggedOut.subscribe(function() {
    sforce.interaction.cti.disableClickToDial();
  });

  window.setContactCard = function(call) {
    return new Promise(function(resolve) {
      var attachedId = call.data.get("objectId");
      var objectName = call.data.get("objectName");

      sforce.interaction.searchAndGetScreenPopUrl(call.otherParties[0].number + (objectName ? " " + objectName : ""), "", call.sfCallType, function(r) {

        if(r.result) {
          var result = JSON.parse(r.result);
          var mainScreen = ui.calls.get(call).main;

          var item;
          if(result[attachedId])
            item = result[attachedId];
          else {
            for(var i in result) {
              item = result[i];
              if(item.object === "Account" || item.object === "Contact" || item.object === "User")   
                break;
            }
          }

          if(item.object === "Account" || item.object === "Contact" || item.object === "User") {
            mainScreen.contactCard.header = item.Name;
            mainScreen.contactCard.image = softphone.images.person;
          }
          if(item.object === "Contact" || item.object === "User")
            mainScreen.contactCard.field1 = item.Title || "";

          resolve(call);
        }
      }); 
    });
  };
});

/**
 * createObject -- utility method for utilizing Salesforce's open cti API saveLog function
 */
function createObject(objName, params) {
  return new Promise(function (resolve, reject) {
    var sfparams = "";
    for (var k in params) {
      sfparams += (k + "=" + params[k] + "&");
    }
    sforce.interaction.saveLog(objName, sfparams.slice(0, -1), function (result) {
      if (result.result)
        resolve(result.result);
      else
        reject(result.error);
    });
  });
};
