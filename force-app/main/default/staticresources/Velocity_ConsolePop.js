phoneReady.then(function(p) {
  var phone = p.phone;
  var ui = p.ui;

  function handlePushNotification(notification) {
    if(notification.entityType === "Case" && notification.IsClosed) {
      if(phone.calls.length === 0)
        phone.setState("Ready");
    }
  };
  sforce.console.addPushNotificationListener(["Case"], handlePushNotification);

  /**
   * handleNewCall -- Callback for calls when a new call is added
   *    Handles Salesforce integration for new calls including: setting attached data, search and 
   *    get screen pop, popping screen pops, and attempting to create a case off attached data. 
   */
  function handleNewCall(call) {
    sforce.interaction.setVisible(true);

    call.sfContext = {};

    sforce.console.cti.fireOnCallBegin(call.id, call.sfCallType, call.otherParties[0].number, function () {
      sforce.console.cti.setCallAttachedData(call.id, JSON.stringify(call.data), call.sfCallType);
    });

    sforce.interaction.searchAndGetScreenPopUrl(call.otherParties[0].number, "", call.sfCallType, function (r) {
      if (r.result) {
        var result = JSON.parse(r.result);

        call.sfContext.possibleRecords = result;
        call.sfContext.pendingRecord = true;
        sforce.interaction.screenPop(result.screenPopUrl, true);
      }
    });
  }

  function handleAnswered(call) {
    if(!call.sfContext || !call.sfContext.pendingRecord)
      return;

    var context = call.sfContext;
    sforce.console.getFocusedPrimaryTabId(function(result) {
      var primaryTabId = result.id;
      sforce.console.getSubtabIds(primaryTabId, function (result) {
        if(!result.ids)
          return;
        
        var ids = result.ids;

        var idPromises = result.ids.reduce(function(acc, id) {
          acc.push(new Promise(function(resolve, reject) {
            sforce.console.getPageInfo(id, function(result) {
              var pageInfo = JSON.parse(result.pageInfo);
              var objId = pageInfo.objectId;

              resolve(objId);
            });
          }));
          return acc;
        }, []);
        Promise.all(idPromises).then(function(ids) {
          var caseInfo = {};
          ids.forEach(function(id) {
            var record = context.possibleRecords[id];
            if(!record)
              return;
            
            if(record.object === "Contact")
              caseInfo.ContactId = id;
            else if(record.object === "Account")
              caseInfo.AccountId = id;
          });

          context.casePromise = createObject("Case", caseInfo).then(function (caseId) {
            sforce.console.openSubtab(primaryTabId, caseId, true, "New Case", null);
            return {
              id: caseId,
              info: caseInfo
            };
          });
        });
      });
    });
  }

  function handleCallReleased(call) {
    var callDuration = Math.ceil((Date.now() - call.startTime) / 1000);
    sforce.console.cti.fireOnCallEnd(call.id, callDuration, "Disposition");
    
    var context = call.sfContext;
    if(context.casePromise) {
      context.casePromise.then(function(c) {
        var taskInfo = {
          CallDurationInSeconds: callDuration,
          CallObject: call.id,
          CallType: call.sfCallType,
          IsClosed: true,
          Status: "Completed",
          Subject: "Call " + new Date(call.startTime).toLocaleString(),
          TaskSubtype: "Task",
          WhatId: c.id
        };

        if(c.info.AccountId)
          taskInfo.AccountId = c.info.AccountId;
        if(c.info.ContactId)
          taskInfo.WhoId = c.info.ContactId;

        createObject("Task", taskInfo);
      });
    }
  }

  
  phone.calls.added.subscribe(function(call) {
    sforce.console.cti.fireOnCallBegin(call.id, call.sfCallType, call.otherParties[0].number + (new Date()).toLocaleString(), function() {
      var data = {};
      call.data.entries().forEach(function(kvp) { data[kvp[0]] = kvp[1]; });
      sforce.console.cti.setCallAttachedData(call.id, JSON.stringify(data), call.sfCallType);
    });
  });
  phone.calls.added.subscribe(handleNewCall);
  phone.calls.released.subscribe(handleCallReleased);
  phone.calls.answered.subscribe(handleAnswered);
});
