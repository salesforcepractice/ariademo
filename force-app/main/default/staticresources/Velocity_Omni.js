phoneReady.then(function(p) {
  var phone = p.phone;
  var ui = p.ui;
  
  /**
   * Omni-Channel configuration variables
   * All of these id's can be found from workbench.developerforce.com for your org, under standard object ServicePresenceStatus
   * 
   * ready - A mapping of the phone ready state to an Omni Channel ready state
   * busy - Mappings of phone not-ready states to Omni Channel busy states
   * onCallId           When the phone receives a call omni-channel will default to this service presence status
   * defaults - Mappings for default ready states
   * defaults.ready - A mapping for the default ready state
   * defaults.busy - A mapping for the default busy state
   * defaults.onCall - A mapping for the default state to use when on a call
   */
  var ctiToOmni = {
    ready: {
      Ready: "0N515000000TOCi",
      undef1: "0N515000000Cbp4",
      undef2: "0N515000000Cbp9"
    },
    busy: {
      Lunch: "0N515000000TOCn",
      "Not Ready": "0N515000000TOCi"
    },
    defaults: {
      ready: { Ready: "0N515000000TOCi" },
      busy: { "Not Ready": "0N515000000TOCs" },
      onCall: { "Not Ready": "0N515000000TOCi" }
    }
  };

  var omniToCti = (function() {
    function invert(map) {
      var result = {};
      for(var i in map) {
        result[map[i]] = i;
      }
      return result;
    }
    return {
      ready: invert(ctiToOmni.ready),
      busy: invert(ctiToOmni.busy),
      defaults: {
        ready: invert(ctiToOmni.defaults.ready),
        busy: invert(ctiToOmni.defaults.busy),
        onCall: invert(ctiToOmni.defaults.onCall)
      }
    };
  })();

  /**
   * Omni-channel object values -- these are set and used dynamically and do not need to be set manually
   */
  var omni = {
    workCount: 0,
    workLimit: 1,
    lastReadyPresenceStatusId: ctiToOmni.ready.Ready
  };

  function ctiStateName(state) {
    var found = ui.phoneBar.readyStates.get(state);
    return found && found.label || "Unknown";
  }

  function getPhoneState(name) {
    for(var i in ui.phoneBar.readyStates) {
      var state = ui.phoneBar.readyStates[i];
      if(state.label === name)
        return state.data;
    }
  }

  /**
   * setAgentWorkload -- callback function for sforce.console.presence.getAgentWorkload
   *  Fills variables with omni-channel values for workCount and workLimit which are utilized in the omniEventHandler 
   *  to sync states between omni and softphone 
   */
  function setAgentWorkload(result) {
    omni.workCount = result.currentWorkload || omni.workCount;
    omni.workLimit = result.configuredCapacity || omni.workLimit;
  }

  /**
   * omniEventHandler -- callback function for all omni-channel console events
   *    Handles omni service presence status changes
   *    Records the latest ready service presence status id
   *    Dictates softphone state depending when work is accepted/closed and if work count exceeds work limit based on capacity
   *    Updates work limit and work count whenever service presence status is changed
   * e -- Event received from Salesforce
   */
  function omniEventHandler(e) {
    switch (e.message) {
      case "SFORCE_PRESENCE:STATUS_CHANGED":
        sforce.console.presence.getAgentWorkload(setAgentWorkload);

        var channels = JSON.parse(e.channels);
        if (omniToCti.ready[e.statusId]) {
          omni.lastReadyPresenceStatusId = e.statusId;
        }

        var newState = omniToCti.ready[e.statusId] || omniToCti.busy[e.statusId];
        if(newState)
          phone.setState(getPhoneState(newState));

        break;
      case "SFORCE_PRESENCE:WORK_ACCEPTED":
        omni.workCount++;
        if (omni.workCount >= omni.workLimit && ctiToOmni.ready[ctiStateName(phone.state)]) {
          phone.setState(getPhoneState(Object.keys(ctiToOmni.defaults.busy)[0]));
        }
        break;
      case "SFORCE_PRESENCE:WORK_CLOSED":
        omni.workCount--;
        if (omni.workCount < omni.workLimit && ctiToOmni.busy[ctiStateName(phone.state)]) {
          phone.setState(getPhoneState(Object.keys(ctiToOmni.defaults.ready)[0]));
        }
        break;
      default:
        break;
    }
  };

  /**
   * Salesforce console api calls -- setup and attaching events handlers for calls and omni
   */
  sforce.console.presence.getAgentWorkload(setAgentWorkload);
  sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.STATUS_CHANGED, omniEventHandler);
  sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.WORK_ASSIGNED, omniEventHandler);
  sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.WORK_ACCEPTED, omniEventHandler);
  sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.WORK_DECLINED, omniEventHandler);
  sforce.console.addEventListener(sforce.console.ConsoleEvent.PRESENCE.WORK_CLOSED, omniEventHandler);

  /**
   * Softphone handler for omni integration
   *    Listens to changes in phone state, makes an api call to verify salesforce service presence status.
   *    If omni is 'unavailable' and phone is switching to ready omni will reflect that by switching to the configured default service presence status
   *    If omni is 'available' and phone goes not ready omni will switch to 'unavailable'
   *    If omni is already 'available' and phone goes ready nothing happens and vice versa for not ready/'unavailable'
   */
  phone.stateChanged.subscribe(function() {
    var stateName = ctiStateName(phone.state);
    var omniState = ctiToOmni.ready[stateName] || ctiToOmni.busy[stateName];
    if(omniState)
      sforce.console.presence.setServicePresenceStatus(omniState);
  });

  /**
   * Softphone handler for omni integration
   *    When the number of calls increases beyond 0 changes omni's service presence status to the default configured on call service presence status
   *    When softphone is no longer on any calls switches back to the last selected ready service presence status
   */
  function callAddedRemoved() {
    var numCalls = phone.calls.length;

    sforce.console.presence.getServicePresenceStatusId(function(result) {
      if(result.success) {
        if(omniToCti.ready[result.statusId] && numCalls > 0)
          sforce.console.presence.setServicePresenceStatus(Object.keys(omniToCti.defaults.onCall)[0]);
        else if(omniToCti.busy[result.statusId] && numCalls === 0)
          sforce.console.presence.setServicePresenceStatus(omni.lastReadyPresenceStatusId);
      }
    });
  }
  phone.calls.added.subscribe(callAddedRemoved);
  phone.calls.removed.subscribe(callAddedRemoved);
});
