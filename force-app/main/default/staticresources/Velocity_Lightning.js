var phoneReady = new Promise(function(resolve, reject) {
  sforce.opencti.getCallCenterSettings({
    callback: function(result) {
      if(!result.success) {
        return reject("Salesforce did not return call center settings -- verify call center definition");
      }
      var ccSettings = result.returnValue;

      var vendor = ccSettings["/reqGeneralInfo/reqCtiVendor"];

      var phone;
      if(vendor.match(/CiscoFinesse/i)) {
        var endpoint = ccSettings["/reqGeneralInfo/reqCtiEndpoint"];
        var eventTunnel = ccSettings["/reqGeneralInfo/reqCtiEventTunnel"];

        phone = softphone.phone.CiscoFinesse(endpoint, eventTunnel);
      } else if(vendor.match(/test/i)) {
        phone = softphone.phone.Test(1);
      } else {
        return reject("Invalid vendor cannot create phone");
      }
      
      var ui = softphone.Ui(phone, document.getElementById("phone")).quickRestore();

      resolve({ phone: phone, ui: ui, ccSettings: ccSettings });
    }
  });
});

phoneReady.then(function(p) {
  var phone = p.phone;
  var ui = p.ui;

  sforce.opencti.setSoftphonePanelHeight({ heightPX: p.ccSettings["/reqGeneralInfo/reqSoftphoneHeight"] });
  sforce.opencti.setSoftphonePanelWidth({ widthPX: p.ccSettings["/reqGeneralInfo/reqSoftphoneWidth"] });
  sforce.opencti.disableClickToDial();

  sforce.opencti.onClickToDial({ listener: function(data) {
    phone.dial(cleanPhoneNumber(data.number));
  }});
  phone.loggedIn.subscribe(function() {
    sforce.opencti.enableClickToDial();
    setPhoneLabel("No Call");
  });
  phone.loggedOut.subscribe(function() {
    sforce.opencti.disableClickToDial();
    setPhoneLabel("Logged Out");
  });

  phone.calls.added.subscribe(setContactCard);

  function handleRingingDialing(call) {
    sforce.opencti.searchAndScreenPop({
      searchParams: call.otherParties[0].number,
      callType: sforce.opencti.CALL_TYPE.INBOUND
    });
  }

  phone.calls.ringing.subscribe(handleRingingDialing);
  phone.calls.dialing.subscribe(handleRingingDialing);
  phone.calls.released.subscribe(function handleCallReleased(call) {
    sforce.opencti.getAppViewInfo({
      callback: function(result) {
        if(!result.success || !result.returnValue.recordId)
          return;
        
        var pageInfo = result.returnValue;

        createObject("Task", taskInfo(pageInfo, call));
        createObject("Case", caseInfo(pageInfo, call));
      }
    });
  });

  function setPhoneLabel(text) {
    sforce.opencti.setSoftphonePanelLabel({ label: text });
    sforce.opencti.setSoftphoneItemLabel({ label: text });
  }

  function resolvePhoneLabel() {
    if(phone.calls.length === 0)
      return "No Call";

    if(ui.screen.callId) {
      var call = phone.calls.get(ui.screen.callId);
      var mainScreen = ui.calls.get(call).main;
      if(mainScreen.contactCard && mainScreen.contactCard.header.length > 0)
        return mainScreen.contactCard.header;
      else
        return call.otherParties[0].number;
    }
  }

  ui.screenChanged.subscribe(function() {
    setPhoneLabel(resolvePhoneLabel());
  });
  setPhoneLabel(resolvePhoneLabel());
  phone.calls.added.subscribe(function(call) {
    setContactCard(call).then(function() {
      setPhoneLabel(resolvePhoneLabel());
    });
  });

  /**
   * caseInfo -- handler for case creation when a call is released
   */
  function caseInfo(currentPage, call) {
    var currentObject = currentPage.objectType;
    var caseInfo = {};
    if(currentObject === "Account")
      caseInfo.AccountId = currentPage.recordId;
    else if(currentObject === "Contact")
      caseInfo.ContactId = currentPage.recordId;

    return caseInfo;
  }

  /**
   * taskInfo -- handler for task creation when a call is released
   */
  function taskInfo(currentPage, call) {
    var currentObject = currentPage.objectType;
    var taskInfo = {
      Status: "Completed"
    };
    if(currentObject === "Contact" || currentObject === "Lead")
      taskInfo.WhoId = currentPage.recordId;
    else
      taskInfo.WhatId = currentPage.recordId;

    taskInfo.TaskSubtype = "Call";

    taskInfo.CallDurationInSeconds = Math.ceil((Date.now() - call.startTime) / 1000);
    taskInfo.Subject = "Call";

    return taskInfo;
  }

  function setContactCard(call) {
    return new Promise(function(resolve) {
      var attachedId = call.data.get("objectId");
      var objectName = call.data.get("objectName");

      sforce.opencti.searchAndScreenPop({
        searchParams: call.otherParties[0].number,
        callType: sforce.opencti.CALL_TYPE.INBOUND,
        deferred: true,
        callback: function(result) {
          if(!result.success)
            return;

          var mainScreen = ui.calls.get(call).main;

          for(var i in result.returnValue) {
            var item = result.returnValue[i];
            if(!item || !item.RecordType)
              continue;
            
            if(item.RecordType === "Account" || item.RecordType === "Contact") {
              mainScreen.contactCard.header = item.Name;
              mainScreen.contactCard.image = softphone.images.person;
            }
            if(item.RecordType === "Contact")
              mainScreen.contactCard.field1 = item.Title || "";
          }

          resolve(call);
        }
      });
    });
  }
});

function createObject(objName, params) {
  return new Promise(function (resolve, reject) {
    delete params.Id;
    params.entityApiName = objName;
    
    sforce.opencti.saveLog({
      value: params,
      callback: function (result) {
        if (result.success)
          resolve(result.returnValue.recordId);
        else
          reject();
      }
    });
  });
};
