/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 10/26/2018.
 */

@isTest
private class ASP_CaseLatestQueueHandlerTest {
  @testSetup
  private static void setup() {
    Group g = new Group(Name = 'ASP_CaseLatestQueueHandlerQueue');
    insert g;
  }

  @isTest
  private static void testInsert() {
    Group g = [SELECT Id FROM Group WHERE Name = 'ASP_CaseLatestQueueHandlerQueue'];
    List<Case> cases = new List<Case> { 
      ATU_Case.build('Test Case 1').withOwnerId(g.Id).create(),
      ATU_Case.build('Test Case 2').withOwnerId(UserInfo.getUserId()).create() 
    };

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Case.SObjectType,
      TriggerOperation.BEFORE_INSERT,
      true,
      cases,
      null,
      null,
      null
    );
    ASP_CaseLatestQueueHandler handler = new ASP_CaseLatestQueueHandler();
    handler.invokeMain(args, 1);

    System.assertEquals('ASP_CaseLatestQueueHandlerQueue', cases[0].Latest_Queue__c);
    System.assertNotEquals(null, cases[0].Latest_Queued_Date__c);
    System.assertEquals(null, cases[1].Latest_Queue__c);
    System.assertEquals(null, cases[1].Latest_Queued_Date__c);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void testUpdate() {
    Group g = [SELECT Id FROM Group WHERE Name = 'ASP_CaseLatestQueueHandlerQueue'];
    Map<Id, Case> oldCasesMap = new Map<Id, Case> {
      '500000000000001' => ATU_Case.build('Test Case 1').withOwnerId(UserInfo.getUserId()).create(),
      '500000000000002' => ATU_Case.build('Test Case 2').withOwnerId(g.Id).create()
    };
    List<Case> newCases = new List<Case> { 
      ATU_Case.build('Test Case 1').withOwnerId(g.Id).withField('Id', '500000000000001').create(),
      ATU_Case.build('Test Case 2').withOwnerId(UserInfo.getUserId()).withField('Id', '500000000000002').create()
    };

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Case.SObjectType,
      TriggerOperation.BEFORE_UPDATE,
      true,
      newCases,
      null,
      null,
      oldCasesMap
    );
    ASP_CaseLatestQueueHandler handler = new ASP_CaseLatestQueueHandler();
    handler.invokeMain(args, 1);

    System.assertEquals('ASP_CaseLatestQueueHandlerQueue', newCases[0].Latest_Queue__c);
    System.assertNotEquals(null, newCases[0].Latest_Queued_Date__c);
    System.assertEquals(null, newCases[1].Latest_Queue__c);
    System.assertEquals(null, newCases[1].Latest_Queued_Date__c);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void testUnsupportedEvents() {
    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Case.SObjectType,
      TriggerOperation.BEFORE_DELETE,
      true,
      null,
      null,
      null,
      null
    );
    ASP_CaseLatestQueueHandler handler = new ASP_CaseLatestQueueHandler();
    handler.invokeMain(args, 1);
    handler.invokeWhileInProgress(args, 1);
    handler.invokeCompleted(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
  }
}