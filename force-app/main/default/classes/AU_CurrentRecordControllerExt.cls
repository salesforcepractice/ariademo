/**
 * Created by jfischer on 11/18/2016.
 */

public with sharing class AU_CurrentRecordControllerExt {
  public String currentRecordId {get; private set;}

  public AU_CurrentRecordControllerExt(ApexPages.StandardController controller) {
    currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
  }
}