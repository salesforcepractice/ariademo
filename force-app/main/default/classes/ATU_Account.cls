/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@IsTest
public class ATU_Account {

  // Deprectated, please use .build().create()

  public static Account createAccount (String name) {
    return build(name).create();
  }

  public static ATU_Account.Builder build (String name) {
    return new ATU_Account.Builder (name);
  }

  public class Builder {

    private final Account theRecord;

    private Builder (String name) {
      theRecord = new Account (
        Name = name
      );
    }

    public ATU_Account.Builder withId (String id) {
      theRecord.Id = id;
      return this;
    }

    public ATU_Account.Builder withRecordType (String recordType) {
      // The RecordTypeId field will not exist until a record type is defined. 
      theRecord.put('RecordTypeId', AU_RecordTypes.getId('Account', recordType));
      return this;
    }

    public ATU_Account.Builder withField(String fieldName, Object value) {
      theRecord.put(fieldName, value);
      return this;
    }

    public Account create () {
      return theRecord;
    }

    public Account persist () {
      insert theRecord;
      return theRecord;
    }

    public Account registerNew (AU_UnitOfWork uow) {
      uow.registerNew(theRecord);

      return theRecord;
    }

    public Account monitorNew (AU_UnitOfWork uow) {
      uow.monitorNew(theRecord);

      return theRecord;
    }
  }
}