/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 * Created on 04-May-18.
 *
 */

/**
* The utility sends email notification about running Apex jobs
* */

public class AU_EmailNotificationUtil {

  private static final String CLASS_NAME = 'AU_EmailNotificationUtil';

  public static void sendBatchJobDoneConfirmation(CronJobDetail apexJob, String emailRecipients, String apexJobMessage)  {

    AU_Debugger.enterFunction(CLASS_NAME + '.execute');

    try {
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      mail.setSenderDisplayName('Apex Job Report');
      mail.setSubject('Apex job "' + apexJob.Name + '" has been run successfully');
      mail.setToAddresses(emailRecipients.split(';'));
      mail.setPlainTextBody(String.format('The Apex job Id: {0}, Name: {1} has been run successfully at {2} .\n\n Org Name: {3}\n\n Message: {4}',  new String[] {
          String.valueOf(apexJob.Id),
          apexJob.Name,
          String.valueOf(System.now().format() + ' ' + UserInfo.getTimeZone().getDisplayName()),
          String.valueOf(UserInfo.getOrganizationName() + ' (' + UserInfo.getOrganizationId() + ')'),
          System.URL.getSalesforceBaseUrl().toExternalForm(),
          apexJobMessage
      }));

      List<Messaging.SendEmailResult> emailResults = new List<Messaging.SendEmailResult>();
      emailResults.addAll(Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ mail }, false));

      for (Messaging.SendEmailResult ser : emailResults) {
        if (!ser.isSuccess()) {
          List<Messaging.SendEmailError> errors = ser.getErrors();
          AU_Debugger.debug('Error: ' + errors[0].getMessage());
        }
      }

    } catch (Exception ex)  {
        AU_Debugger.reportException(CLASS_NAME, ex);
    } finally {
      AU_Debugger.leaveFunction();
    }
  }
}