/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 04-Nov-18. 
 */

@isTest
private class ASP_LiveAgentInteractionHandlerTest {

  @isTest
  static void createInteractionAndSegmentTest() {

    List<LiveChatTranscript> transcripts = createLiveChatTranscripts();
    TriggerOperation operation = TriggerOperation.AFTER_UPDATE;
    List<AgentWork> agentWorks = new List<AgentWork>{createAgentWork(transcripts)};
    AU_TriggerEntry.Args args = createArgs(agentWorks, operation);

    Test.startTest();
    ASP_LiveAgentInteractionHandler handler = new ASP_LiveAgentInteractionHandler();
    handler.invokeMain(args, 1);
    List<ASP_Interaction_Segment__c> segments = [
        SELECT  ASP_Interaction__r.Interaction_Id__c, Agent_Identifier_Name__c, Interaction_Segment_Id__c
        FROM    ASP_Interaction_Segment__c
    ];
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(1, segments.size());
    System.assertEquals(transcripts[0].Id, segments[0].ASP_Interaction__r.Interaction_Id__c);
    System.assertEquals(transcripts[0].Id + '' + transcripts[0].OwnerId, segments[0].Interaction_Segment_Id__c);
  }

  @isTest
  static void updateSegmentTest() {

    List<LiveChatTranscript> transcripts = createLiveChatTranscripts();
    List<AgentWork> agentWorks = new List<AgentWork>{createAgentWork(transcripts)};

    Set<Id> agentWorkIds = new Set<Id>();
    for(AgentWork aw : agentWorks)  {
      agentWorkIds.add(aw.Id);
    }
    Id interactionId = createInteraction(transcripts[0].Id);
    List<ASP_Interaction_Segment__c> segments = createSegments(interactionId, new List<AgentWork>{createAgentWork(transcripts)});

    Test.startTest();
    ASP_LiveAgentInteractionHandler.updateSegments(agentWorkIds);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void invokeMainTest() {
    

    List<LiveChatTranscript> transcripts = createLiveChatTranscripts();
    TriggerOperation operation = TriggerOperation.AFTER_UPDATE;
    AU_TriggerEntry.Args args = createArgs(new List<AgentWork>{createAgentWork(transcripts)}, operation);

    Test.startTest();
    ASP_LiveAgentInteractionHandler handler = new ASP_LiveAgentInteractionHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void notSupportedTriggers() {
    

    List<LiveChatTranscript> transcripts = createLiveChatTranscripts();
    TriggerOperation operation = TriggerOperation.BEFORE_INSERT;
    AU_TriggerEntry.Args args = createArgs(new List<AgentWork>{createAgentWork(transcripts)}, operation);

    Test.startTest();
    ASP_LiveAgentInteractionHandler handler = new ASP_LiveAgentInteractionHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void invokeWhileInProgressAndCompleted() {
    AU_TriggerEntry.Args args;
    ASP_LiveAgentInteractionHandler handler = new ASP_LiveAgentInteractionHandler();

    handler.invokeWhileInProgress(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
    handler.invokeCompleted(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  // Helper methods

  static AgentWork createAgentWork(List<LiveChatTranscript> transcripts)  {
    return new AgentWork(
        Id = '0Bz000000000001001',
        WorkItemId = transcripts[0].Id,
        UserId = UserInfo.getUserId()
    );
  }

  static AU_TriggerEntry.Args createArgs(List<AgentWork> items, TriggerOperation operation) {
    return new AU_TriggerEntry.Args(
        AgentWork.SObjectType,
        operation,
        true,
        items,
        null,
        new Map<Id, SObject>(items),
        null
    );
  }

  static List<LiveChatTranscript> createLiveChatTranscripts() {
    LiveChatVisitor visitor = new LiveChatVisitor();
    insert visitor;
    List<LiveChatTranscript> transcripts = new List<LiveChatTranscript>();
    transcripts.add(new LiveChatTranscript(
        RequestTime = DateTime.newInstance(2018, 11, 18, 3, 3, 3),
        StartTime = DateTime.newInstance(2018, 11, 18, 3, 4, 3),
        EndTime = DateTime.newInstance(2018, 11, 18, 3, 5, 3),
        OwnerId = UserInfo.getUserId(),
        LiveChatVisitorId = visitor.Id)
    );
    insert transcripts;
    return transcripts;
  }

  static Id createInteraction(Id workItemId) {
    ASP_Interaction__c interaction = new ASP_Interaction__c(Interaction_Id__c = workItemId);
    insert interaction;
    return interaction.Id;
  }

  static List<ASP_Interaction_Segment__c> createSegments(Id interactionId, List<AgentWork> agentWorks) {
    List<ASP_Interaction_Segment__c> newSegments = new List<ASP_Interaction_Segment__c>();
    for (AgentWork aw : agentWorks) {
      newSegments.add(new ASP_Interaction_Segment__c(
          Interaction_Segment_Id__c = aw.WorkItemId + '' + aw.UserId,
          RecordTypeId = AU_RecordTypes.getId('ASP_Interaction_Segment__c', 'Chat'),
          ASP_Interaction__c = interactionId));
    }
    insert newSegments;
    return newSegments;
  }
}