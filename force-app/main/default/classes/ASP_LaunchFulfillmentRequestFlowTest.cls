/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 09-Jan-19. 
 */

@isTest
private class ASP_LaunchFulfillmentRequestFlowTest {

  private static Account testAccount { get; set; }
  private static Contact testContact { get; set; }

  @isTest
  static void launchFlowsTest() {
    createAccountAndContact();
    ASP_Fulfillment_Request__e request = createFulfilmentRequest();
    System.debug('request: ' + request);

    Test.startTest();
    EventBus.publish(request);
    ASP_LaunchFulfillmentRequestFlowAction.launchFlows(new List<ASP_Fulfillment_Request__e> {request});
    Test.stopTest();

    //ATU_DebugInfoUtil.assertNoErrors();
  }

  // helper methods

  private static ASP_Fulfillment_Request__e createFulfilmentRequest()  {
    return new ASP_Fulfillment_Request__e(
        Intent__c = 'SchedulePickup',
        Origin__c = 'ACP',
        ContactId__c = testContact.Id,
        Payload__c = '{"accountName": "testAccount"}'
    );
  }

  private static void createAccountAndContact() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();
  }

}