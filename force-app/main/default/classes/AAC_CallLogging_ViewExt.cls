/** Copyright 2017 Aria Solutions Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

public with sharing class AAC_CallLogging_ViewExt {

	private FINAL String acctId;

	public AAC_CallLogging_ViewExt(ApexPages.StandardController ctrl){
		acctId = ctrl.getId();
	}

	public List<TaskwithPhone> getPhoneTasks(){
		List<TaskwithPhone> phoneTasks = new List<TaskwithPhone>();
		Map<Id, String> contactNum = new Map<Id, String>();
		List<Contact> contacts = [SELECT Id, Phone FROM Contact where AccountId =: acctId];
		For(Contact contact : contacts)
		{
			contactNum.put(contact.Id, contact.Phone);
		}
		List<Task> tasks = [SELECT Id, CallDurationInSeconds, WhoId, CallObject, CreatedDate, CallType
									FROM Task
									WHERE WhatId =: acctId AND CallObject != null
									ORDER BY CreatedDate DESC];
		for(Task task : tasks)
		{
			phoneTasks.add(new TaskwithPhone(contactNum.get(task.WhoId), task));
		}
		return phoneTasks;
	}

	public class TaskwithPhone
	{
		public String phoneNumber {get;set;}
		public Task task {get;set;}

		public TaskWithPhone(String phoneNumber, Task task)
		{
			this.phoneNumber = phoneNumber;
			this.task = task;
		}
	}

}