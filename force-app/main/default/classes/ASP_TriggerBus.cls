/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

public class ASP_TriggerBus {
  public static void dispatch(AU_TriggerEntry.Args args) {

    SObjectType key = (SObjectType) args.objectType;
    if (dispatcherByType.containsKey(key)) {
      dispatcherByType.get(key).dispatch(args);
    }
  }

  private static List<AU_TriggerEntry.ITriggerEntry> caseHandlers = new AU_TriggerEntry.ITriggerEntry[]{
      new ASP_CaseLatestQueueHandler(),
      new ASP_CaseCallTaskHandler()
  };

  private static List<AU_TriggerEntry.ITriggerEntry> agentWorkHandlers = new AU_TriggerEntry.ITriggerEntry[]{
      new ASP_ManageEmailInteractionOnAgentWork(),
      new ASP_LiveAgentInteractionHandler()
  };

  private static List<AU_TriggerEntry.ITriggerEntry> emailMessageHandlers = new AU_TriggerEntry.ITriggerEntry[]{
      new ASP_UpdateSegmentOnEmailReply()
  };

  private static List<AU_TriggerEntry.ITriggerEntry> liveChatTranscriptHandlers = new AU_TriggerEntry.ITriggerEntry[]{
      new ASP_UpdateSegmentOnChatClosedHandler()
  };

  private static List<AU_TriggerEntry.ITriggerEntry> taskHandlers = new AU_TriggerEntry.ITriggerEntry[] {
      new ASP_UpdateSegmentOnTaskCreatedHandler(),
      new SC_UpdateTranscriptOnTaskUpdateHandler(),
      new ASP_UpdateTaskRecordType(),
      new ASP_CallbackTaskHandler()
  };

  private static List<AU_TriggerEntry.ITriggerEntry> marqueeHandlers = new AU_TriggerEntry.ITriggerEntry[]{
      new ASP_MarqueeHandler()
  };

  private static Map<SObjectType, AU_TriggerEventDispatcher> dispatcherByType = new Map<SObjectType, AU_TriggerEventDispatcher>{
      Case.getSObjectType() => new AU_TriggerEventDispatcher(caseHandlers),
      AgentWork.getSObjectType() => new AU_TriggerEventDispatcher(agentWorkHandlers),
      EmailMessage.getSObjectType() => new AU_TriggerEventDispatcher(emailMessageHandlers),
      LiveChatTranscript.getSObjectType() => new AU_TriggerEventDispatcher(liveChatTranscriptHandlers),
      Task.getSObjectType() => new AU_TriggerEventDispatcher(taskHandlers),
      ASP_Marquee__c.getSObjectType() => new AU_TriggerEventDispatcher(marqueeHandlers)
  };

  @TestVisible private static void resetHandlers() {
    resetHandlersWith(new Map<SObjectType, AU_TriggerEventDispatcher>());
  }
  @TestVisible private static void resetHandlersWith(Map<SObjectType, AU_TriggerEventDispatcher> dispatchers) {
    dispatcherByType = dispatchers;
  }
}