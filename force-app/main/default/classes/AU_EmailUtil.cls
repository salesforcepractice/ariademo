/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 28-Aug-18. 
 */

public class AU_EmailUtil {

  private static final String CLASS_NAME = 'AU_EmailUtil';

  public static EmailTemplate getEmailTemplateByName(String name) {
    try {
      return [SELECT Id FROM EmailTemplate WHERE Name = :name LIMIT 1];
    } catch(QueryException ex) {
      throw new AU_EmailResourceNotFoundException(String.format('Email template with name "{0}" cannot be found', new String[] {name}));
    }
  }

  public static OrgWideEmailAddress getFromEmailAddress(String name)  {
    try {
      return [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = :name LIMIT 1];
    } catch (QueryException ex)  {
      throw new AU_EmailResourceNotFoundException(String.format('Organization wide email address with name "{0}" cannot be found', new String[] {name}));
    }
  }

  public class AU_EmailResourceNotFoundException extends Exception { }
}