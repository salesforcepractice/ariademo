/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

/*
 * Utility class for any Map<> related transformation functions.
 */
public class AU_MapUtil {

  private static final String CLASS_NAME = 'AU_MapUtil:';

  public static Map<Id, SObject> retainAll(Map<Id, SObject> src, Set<Id> idsToKeep) {
    if (src == null) {
      return null;
    }

    AU_Debugger.debug(CLASS_NAME + 'Ids in src:' + src.keySet());
    AU_Debugger.debug(CLASS_NAME + 'Ids to keep:' + idsToKeep);

    Map<Id, SObject> result = new Map<Id, SObject>(src);

    result.keySet().retainAll(idsToKeep);

    return result;
  }
}