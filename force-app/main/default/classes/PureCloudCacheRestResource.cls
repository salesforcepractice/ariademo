@RestResource(urlMapping='/PureCloudCache/*')
global class PureCloudCacheRestResource {
    @HttpPost
    global static void storeData(PureCloudCacheService.PureCloudData data, String key) {
        if (String.isBlank(key))
            PureCloudCacheService.storeData(data);
        else
            PureCloudCacheService.storeData(data, key);
    }
    
    @HttpGet
    global static PureCloudCacheService.PureCloudData retrieveData() {
        String key = RestContext.request.params.get('key');

        PureCloudCacheService.PureCloudData data = PureCloudCacheService.retrieveData(key);
        if (data == null) {
            RestContext.response.statusCode = 400;
            RestContext.response.responseBody = Blob.valueOf('Invalid key');
            return null;
        }
        
        return data;
    }
}