/** Copyright 2019, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 04-Nov-19. 
 */

@IsTest
public class ASP_AssignUserGroupsControllerTest {
  @isTest
  static void getUserGroupSelectedOptionsTest() {
    ASP_AssignUserGroupsController controller = new ASP_AssignUserGroupsController();
    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(controller.selectedUserGroups.size(), [SELECT Id, Name FROM Group WHERE Type = 'Regular'].size() + 2);
  }

  @isTest
  static void saveGroupsTest()  {
    ASP_AssignUserGroupsController controller = new ASP_AssignUserGroupsController();
    controller.saveGroups();
    ATU_DebugInfoUtil.assertNoErrors();
  }
}