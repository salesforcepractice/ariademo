/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

public class AU_TriggerUtil {

  public static FilterChain filter(AU_TriggerEntry.Args args) {
    return new FilterChain(args);
  }

  public class FilterChain {

    private final String CLASS_NAME = 'FilterChain:';

    @TestVisible
    private AU_TriggerEntry.Args args { get; set; }

    private final List<AU_ITriggerObjectsFilter> filters = new List<AU_ITriggerObjectsFilter>();

    public FilterChain(AU_TriggerEntry.Args args) {
      this.args = args;
    }

    public FilterChain byValueChanged(String fieldName) {
      return byValuesChanged(new String[] { fieldName });
    }

    public FilterChain byValuesChanged(String[] fieldNames) {
      this.filters.add(new AU_TriggerObjectsFilters.ValuesChangedFilter(fieldNames));
      return this;
    }

    public FilterChain byNewValueEquals(String fieldName, Object expectedValue) {
      this.filters.add(new AU_TriggerObjectsFilters.NewValueEqualsFilter(fieldName, expectedValue));
      return this;
    }

    public FilterChain byNewValueIsType(String idFieldName, Schema.SObjectType expectedType) {
      this.filters.add(new AU_TriggerObjectsFilters.NewIdValueIsTypeFilter(idFieldName, expectedType));
      return this;
    }

    public AU_TriggerEntry.Args run() {
      AU_Debugger.debug(String.format(CLASS_NAME + 'Executing filter chain with {0} filters', new String[] {String.valueOf(filters.size())}));

      AU_TriggerEntry.Args filteredArgs = this.args;
      for (AU_ITriggerObjectsFilter filter : filters) {
        filteredArgs = filter.run(filteredArgs);
      }
      return filteredArgs;
    }
  }
}