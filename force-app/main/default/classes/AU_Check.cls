/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public class AU_Check {

  public static String notEmpty(String value, String name) {
    if (value == null)
      throw new AU_Check.ArgumentException('\'' + name + '\' is null');

    if (value == '')
      throw new AU_Check.ArgumentException('\'' + name + '\' is empty');

    return value;
  }

  public static Object notNull(Object value, String name) {
    if (value == null)
      throw new AU_Check.ArgumentException('\'' + name + '\' is null');

    return value;
  }

  public static Boolean isNull(Object value) {
    return value == null;
  }

  public static Boolean isNullOrEmpty(String value) {
    return value == null || value == '';
  }

  public static void assert(Boolean assertion, String message) {
    if (!assertion) {
      throw new AssertException(message);
    }
  }

  public static Boolean isFieldEqual(SObject sObjA, SObject sObjB, String fieldName) {
    if (sObjA == null || sObjB == null) {
      return sObjA == sObjB;
    }

    return sObjA.get(fieldName) == sObjB.get(fieldName);
  }

  public class ArgumentException extends Exception { }

  public class AssertException extends Exception {}
}