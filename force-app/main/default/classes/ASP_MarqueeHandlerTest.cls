/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Tuesday September 3rd 2019
 * Author: varonov
 * File type: '.cls'
 */


@isTest
public class ASP_MarqueeHandlerTest {

  @isTest
  static void invokeMainTestAfterInsert()  {
    List<ASP_Marquee__c> marquees = createMarquees();
    TriggerOperation operation = TriggerOperation.AFTER_INSERT;
    AU_TriggerEntry.Args args = createArgs(marquees, operation);

    Test.startTest();
    ASP_MarqueeHandler handler = new ASP_MarqueeHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void invokeMainTestAfterUpdate()  {
    List<ASP_Marquee__c> marquees = createMarquees();
    TriggerOperation operation = TriggerOperation.AFTER_UPDATE;
    AU_TriggerEntry.Args args = createArgs(marquees, operation);

    Test.startTest();
    ASP_MarqueeHandler handler = new ASP_MarqueeHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void invokeMainTestBeforeUpdate()  {
    List<ASP_Marquee__c> marquees = createMarquees();
    TriggerOperation operation = TriggerOperation.BEFORE_UPDATE;
    AU_TriggerEntry.Args args = createArgs(marquees, operation);

    Test.startTest();
    ASP_MarqueeHandler handler = new ASP_MarqueeHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void updateTriggerTest() {
    List<ASP_Marquee__c> marquees = createMarquees();
    for (ASP_Marquee__c marquee : marquees) {
      switch on marquee.User_Group__c {
        when 'Community'  {
          marquee.Active__c = false;
        }
        when 'Internal' {
          marquee.Active__c = true;
          marquee.Title__c = 'Test';
        }
      }
    }
    update marquees;
    System.assertEquals(3, [SELECT Id FROM ASP_Marquee__c WHERE Active__c = true].size());
  }

  @isTest
  static void deleteTriggerTest() {
    List<ASP_Marquee__c> marquees = createMarquees();
    delete marquees[0];
    System.assertEquals(3, [SELECT Id FROM ASP_Marquee__c].size());
  }

  @isTest
  static void notSupportedTriggers() {

    List<ASP_Marquee__c> marquees = createMarquees();
    TriggerOperation operation = TriggerOperation.BEFORE_INSERT;
    AU_TriggerEntry.Args args = createArgs(marquees, operation);

    Test.startTest();
    ASP_MarqueeHandler handler = new ASP_MarqueeHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  static void invokeWhileInProgressAndCompleted() {
    AU_TriggerEntry.Args args;
    ASP_MarqueeHandler handler = new ASP_MarqueeHandler();

    handler.invokeWhileInProgress(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
    handler.invokeCompleted(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  // Helper methods

  private static List<ASP_Marquee__c> createMarquees() {
    List<ASP_Marquee__c> insertedPosts = new List<ASP_Marquee__c> {
        new ASP_Marquee__c(
            Title__c = 'Internal Message',
            Start_Date__c = Date.today(),
            User_Group__c = 'Internal',
            Active__c = true
        ),
        new ASP_Marquee__c(
            Title__c = 'Internal Inactive Message',
            Start_Date__c = Date.today(),
            User_Group__c = 'Internal',
            Active__c = false
        ),
        new ASP_Marquee__c(
            Title__c = 'Community Message',
            Start_Date__c = Date.today(),
            User_Group__c = 'Community',
            Active__c = true
        ),
        new ASP_Marquee__c(
            Title__c = 'Internal and Community Message',
            Start_Date__c = Date.today(),
            User_Group__c = 'Internal;Community',
            Show_Posted_Date__c = false,
            Active__c = true
        )
    };
    insert insertedPosts;
    return insertedPosts;
  }

  static AU_TriggerEntry.Args createArgs(List<ASP_Marquee__c> items, TriggerOperation operation) {
    return new AU_TriggerEntry.Args(
        ASP_Marquee__c.SObjectType,
        operation,
        true,
        items,
        null,
        new Map<Id, SObject>(items),
        new Map<Id, Sobject> (items)
    );
  }
    
}