/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 11-Apr-19. 
 */

public with sharing class ASP_GetCallCampaignController {

  @AuraEnabled
  public static ACT_Call_Campaign__c getCallCampaign(Id campaignId)  {
    return [SELECT Phone_Number__c FROM ACT_Call_Campaign__c WHERE Id = :campaignId];
  }

}