/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 12-Apr-19. 
 */

public with sharing class ASP_CallbackController {
  @AuraEnabled
  public static List<Callback__c> getCallBack(Id callbackId)  {
    List<Callback__c> callbacks = [SELECT Phone__c FROM Callback__c WHERE Id = :callbackId];
    for (Callback__c callback : callbacks)  {
      callback.Stage__c = 'Completed';
    }
    update callbacks;
    return callbacks;
  }
}