global class PureCloudCacheService {
    private static String KEY_PREFIX = 'PCCS';
    private static Integer TTL_SECONDS = 300; // 5 minutes
    
    public static String storeData(PureCloudData data) {
        return storeData(data, generateKey());
    }
    
    public static String storeData(PureCloudData data, String sessionKey) {
        String key = KEY_PREFIX + sessionKey;
        key = key.remove('-');
        Cache.Org.put(key, data, TTL_SECONDS);
        
        return sessionKey;
    }
    
    public static PureCloudData retrieveData(String sessionKey) {
        String key = KEY_PREFIX + sessionKey;
        key = key.remove('-');
        if (!Cache.Org.contains(key)) {
            return null;
        }
        
        return (PureCloudData) Cache.Org.get(key);
    }
    
    private static String generateKey() {
        String key;
        
        // generate a unique key
        key = String.valueOf(UserInfo.getUserId()) + String.valueOf(System.now().getTime());
        System.debug('sessionKey: ' + key);
        return key;
    }
    
    // adjust this as needed
    global class PureCloudData {
        public String contactId;
        public String newCaseSubject;

        public PureCloudData() {}
        
        public PureCloudData(String contactId, String newCaseSubject) {
            this.contactId = contactId;
            this.newCaseSubject = newCaseSubject;
        }
    }
}