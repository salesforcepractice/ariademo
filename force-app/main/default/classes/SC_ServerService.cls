/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday February 7th 2020
 * Author: Vacheslav Aronov
 * File type: '.cls'
 */


global with sharing class SC_ServerService {
  public static TokenDetails getToken(String conversationId)  {
    Map<String, String> interactionSettings = buildMapOfPureCloudInteraction();
    TokenDetails token = new TokenDetails();
    String clientId;
    String clientSecret;
    for (String id : interactionSettings.keySet())  {
      clientId = id;
      clientSecret = interactionSettings.get(id);
    }
    if (clientId != null && clientSecret != null) {
      HttpRequest request = new HttpRequest();
      request.setMethod('POST');
      request.setHeader('Authorization', 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(clientId + ':' + clientSecret)));
      request.setHeader('Host','https://login.mypurecloud.com/oauth');
      request.setHeader('content-type', 'application/x-www-form-urlencoded');
      request.setEndpoint('https://login.mypurecloud.com/oauth/token');
      request.setBody('grant_type=client_credentials');

      Http http = new Http();
      try {
        HTTPResponse response = http.send(request);
        if (response.getStatusCode() != 200) {
          AU_Debugger.debug('SC_TranscriptService: The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
        } else {
          Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
          token.accessToken = (String) results.get('access_token');
          token.tokenType = (String) results.get('token_type');
        }
      } catch(System.CalloutException e){
        AU_Debugger.debug('SC_TranscriptService: Response error: ' + e.getMessage());
      }
    }
   
    return token;
  }

  private static Map<String, String> buildMapOfPureCloudInteraction() {
    Map<String, String> purecloudInteraction = new Map<String, String>();
    for(PureCloud_Interaction_Setting__mdt setting : [SELECT ClientId__c, Client_Secret__c FROM PureCloud_Interaction_Setting__mdt])	{
      purecloudInteraction.put(setting.ClientId__c, setting.Client_Secret__c);
    }
    return purecloudInteraction;
  }

  public class TokenDetails {
    public String tokenType {get; set;}
    public String accessToken {get; set;}

    public TokenDetails() {}
  }
}