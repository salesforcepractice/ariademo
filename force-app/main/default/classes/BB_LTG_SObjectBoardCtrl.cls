/** *****************************************************************************
* Author kbowden
* Date 10 Jan 2016
********************************************************************************/
public class BB_LTG_SObjectBoardCtrl {
  private static final String CLASS_NAME = 'BB_LTG_SObjectBoardCtrl';
  private static final List<String> CONTACT_FIELDS = new List<String> {
    'Phone', 'Email', 'MailingStreet', 'MailingState', 'MailingCity', 'MailingPostalCode', 'Name', 'Title'
  };

  @AuraEnabled
  public static List<BB_LTG_BoardStage> GetStages(
    String sobjectType,
    String stageValueField,
    String stageConfigField,
    String contactLookupField,
    String warningIconField,
    String secondIconField,
    String includeValues,
    String excludeValues,
    String fieldNames,
    String filter,
    String orderBy
  ) {
    List<BB_LTG_BoardStage> stages = new List<BB_LTG_BoardStage>();
    try {
      AU_Debugger.enterFunction(CLASS_NAME + '.GetStages');

      if (stageConfigField == null) {
        stageConfigField = stageValueField;
      }

      Set<String> includeValuesSet = convertToSet(includeValues);
      Set<String> excludeValuesSet = convertToSet(excludeValues);
      Map<String, BB_LTG_BoardStage> stagesByName = new Map<String, BB_LTG_BoardStage>();
      Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(sobjectType).
        getDescribe().fields.getMap();

      Schema.DescribeFieldResult fieldRes = fieldMap.get(stageConfigField).getDescribe();
      List<Schema.PicklistEntry> ples = fieldRes.getPicklistValues();

      for (Schema.PicklistEntry ple : ples) {
        String stageName = ple.GetLabel();
        if (!excludeValuesSet.contains(stageName) && (includeValuesSet.isEmpty() || includeValuesSet.contains(stageName))) {
          BB_LTG_BoardStage stg = new BB_LTG_BoardStage();
          stg.stageName = ple.GetLabel();
          stagesByName.put(stg.stageName, stg);
          stages.add(stg);
        }
      }

      List<String> fieldNamesList = fieldNames.split(',');
      String queryStr = 'select Id, ' + String.escapeSingleQuotes(stageValueField) + ', ' +
        String.escapeSingleQuotes(fieldNames);
      
      if (String.isNotBlank(contactLookupField)) {
        for (String contactField : CONTACT_FIELDS) {
          queryStr += ' , ' + String.escapeSingleQuotes(contactLookupField) + '.' + contactField;
        }
      }

      if (String.isNotBlank(warningIconField)) {
        queryStr += ', ' + String.escapeSingleQuotes(warningIconField);
      }

      if (String.isNotBlank(secondIconField)) {
        queryStr += ', ' + String.escapeSingleQuotes(secondIconField);
      }

      queryStr += ' from ' + String.escapeSingleQuotes(sobjectType);

      if (String.isNotBlank(filter)) {
        queryStr += ' where ' + filter;
      }

      if (String.isNotBlank(orderBy)) {
        queryStr += ' order by ' + orderBy;
      }

      AU_Debugger.debug('Query: ' + queryStr);
      System.debug('Query: '+ queryStr);
      List<SObject> sobjects = Database.query(queryStr);

      AU_Debugger.debug('Number of records found: ' + sobjects.size());
      for (SObject sobj : sobjects) {
        String value = String.valueOf(sobj.get(stageValueField));
        BB_LTG_BoardStage stg = stagesByName.get(value);

        if (stg != null) {
          BB_LTG_BoardStage.StageSObject sso = new BB_LTG_BoardStage.StageSObject();
          sso.id = (String) sobj.get('Id');

          if (String.isNotBlank(warningIconField)) {
            String[] warning = splitString((String)sobj.get(warningIconField));

            if (warning != null && warning.size() >= 2) {
              sso.warningIconName = warning[0];
              sso.warningIconVariant = warning[1];
            }
          }

	        if (String.isNotBlank(secondIconField)) {
            String[] secondIcon = splitString((String)sobj.get(secondIconField));

            if (secondIcon != null && secondIcon.size() >= 2) {
              sso.secondIconName = secondIcon[0];
              sso.secondIconVariant = secondIcon[1];
            }
	        }

          Integer idx = 0;
          for (String fieldName : fieldNamesList) {
            fieldName = fieldName.trim();
            fieldRes = fieldMap.get(fieldName).getDescribe();
            BB_LTG_BoardStage.StageSObjectField ssoField =
              new BB_LTG_BoardStage.StageSObjectField(fieldRes, sobj.get(fieldName));
            if (idx == 0) {
              sso.titleField = ssoField;
            }
            else {
              sso.fields.add(ssoField);
            }
            idx++;
          }
          
          if (String.isNotBlank(contactLookupField)) {
            SObject contact = sobj.getSObject(contactLookupField);
            if (contact != null) {
              sso.contactJSON = JSON.serialize(contact);
            }
          }
          
          stg.sobjects.add(sso);
        }
      }
    }
    catch (Exception ex) {
      AU_Debugger.reportException(CLASS_NAME, ex);
      AU_Debugger.leaveFunction();
      throw ex;
    } 
    finally {
      AU_Debugger.leaveFunction();
    }
    return stages;
  }

  @AuraEnabled
  public static void CreateAgentWorkRecord(String workItemId) {
    AU_Debugger.enterFunction(CLASS_NAME + '.createAgentWorkRecord');

    String myUserId = UserInfo.getUserId();

    try {
      ServiceChannel serviceChannel = [SELECT Id FROM ServiceChannel WHERE DeveloperName = 'ASP_AgentBacklogChannel'];

      AgentWork aw = new AgentWork(
        UserId = myUserId,
        ServiceChannelId = serviceChannel.Id,
        WorkItemId = workItemId,
        CapacityWeight = 1
      );

      insert aw;
    } catch (DmlException ex) {
      AU_Debugger.reportException('AgentWork insertion failed', ex);
    }
    AU_Debugger.leaveFunction();
  }

  private static Set<String> convertToSet(String values) {
    Set<String> result = new Set<String>();
    if (String.isBlank(values)) {
      return result;
    }

    for (String value : values.split(',')) {
      result.add(value.normalizeSpace());
    }
    return result;
  }

  private static String[] splitString(String stringToSplit) {
    if (String.isNotBlank(stringtoSplit)) {
      String[] splitString = stringToSplit.split('\\|');
      return splitString;
    }
    else {
      return null;
    }
  }
}