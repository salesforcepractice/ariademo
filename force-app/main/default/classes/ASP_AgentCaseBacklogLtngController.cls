/**
 * Created by jfischer on 10/3/2016.
 */

global with sharing class ASP_AgentCaseBacklogLtngController {
  private static final String CLASS_NAME = 'ASP_AgentCaseBacklogLtngController';

  @RemoteAction
  public static void createAgentWorkRecord(String caseId) {
    AU_Debugger.enterFunction(CLASS_NAME + '.createAgentWorkRecord');

    String myUserId = UserInfo.getUserId();

    try {
	    ServiceChannel serviceChannel = [SELECT Id FROM ServiceChannel WHERE DeveloperName = 'ASP_AgentBacklogChannel'];
	
	    AgentWork aw = new AgentWork(
	      UserId = myUserId,
	      ServiceChannelId = serviceChannel.Id,
	      WorkItemId = caseId
	    );

    	insert aw;
    } catch (DmlException ex) {
      AU_Debugger.reportException('AgentWork insertion failed', ex);
    }
    AU_Debugger.leaveFunction();
  }
}