/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 07-Aug-18. 
 */

public with sharing class ATU_AccountFactory {

  public static ATU_Account.Builder createDefaultBuilder(String name) {
    return ATU_Account.build(name);
  }
}