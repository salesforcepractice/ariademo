/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

global with sharing class ASP_UpdateSentimentInvoker {

  private static final String CLASS_NAME = 'ASP_UpdateSentimentInvoker';

  @TestVisible
  private static TokenProvider tokenProvider = EinsteinTokenProvider.forDefaultProfile();

  @InvocableMethod(label='Update Einstein Sentiment' description='Sends a text to Einstein for sentiment analysis using the community model and updates the record with the result.')
  global static void updateSentiments(List<SentimentArg> args) {
    AU_Debugger.enterFunction(CLASS_NAME + '.updateSentiments, number of args=' + args.size());
    for (SentimentArg arg : args) {
      System.enqueueJob(new ASP_UpdateSentimentQueueable(arg.objectType, arg.sentimentFieldName, arg.sentimentProbabilityFieldName, arg.recordId, arg.text, tokenProvider));
    }
    AU_Debugger.leaveFunction();
  }

  global class SentimentArg {
    @InvocableVariable(label='Record Id' required=true Description='Id of the record that contains the text to be analyzed')
    public Id recordId;

    @InvocableVariable(label='Text to be analyzed' required=true Description='The text to be analyzed for its sentiment')
    public String text;

    @InvocableVariable(label='Object Type' required=true Description='Name of the object associated with the given Record Id, i.e. Case')
    public String objectType;

    @InvocableVariable(label='Name of the field for sentiment result' required=true Description='Name of the field where the sentiment should be stored in, i.e. Sentiment__c')
    public String sentimentFieldName;

    @InvocableVariable(label='Name of the field for sentiment probability' required=false Description='Name of the field where the sentiment\'s probability should be stored in, i.e. Sentiment_Probability__c')
    public String sentimentProbabilityFieldName;
  }
}