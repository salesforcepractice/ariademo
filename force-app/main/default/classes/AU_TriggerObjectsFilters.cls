/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

/* This class contains common filters for object records inside of a AU_TriggerEntry.Args.
 * The list is expected to expand in the future.
 */
public class AU_TriggerObjectsFilters {

  public class ValuesChangedFilter implements AU_ITriggerObjectsFilter {
    private final String CLASS_NAME = 'ValuesChangedFilter:';

    private final String[] fieldNames;

    public ValuesChangedFilter(String[] fieldNames) {
      this.fieldNames = fieldNames;
    }

    public AU_TriggerEntry.Args run(AU_TriggerEntry.Args args) {
      AU_Debugger.debug(CLASS_NAME + 'Args to filter:' + args);

      Set<Id> idsToKeep = new Set<Id>();
      for (SObject newObj : args.newObjects) {
        if (args.oldObjectMap == null || checkIfAllFieldsChanged(newObj, args)) {
          idsToKeep.add(newObj.Id);
        }
      }
      AU_TriggerEntry.Args result = args.reduce(idsToKeep);
      AU_Debugger.debug(CLASS_NAME + 'Reduced args to:' + result);
      return result;
    }

    private Boolean checkIfAllFieldsChanged(SObject newObj, AU_TriggerEntry.Args args) {
      for (String fieldName : fieldNames) {
        if (AU_Check.isFieldEqual(newObj, args.oldObjectMap.get(newObj.Id), fieldName)) {
          return false;
        }
      }
      return true;
    }
  }

  public class NewValueEqualsFilter implements AU_ITriggerObjectsFilter {

    private final String CLASS_NAME = 'NewValueEqualsFilter:';

    private final String fieldName;
    private final Object expectedValue;

    public NewValueEqualsFilter(String fieldName, Object expectedValue) {
      this.fieldName = fieldName;
      this.expectedValue = expectedValue;
    }

    public AU_TriggerEntry.Args run(AU_TriggerEntry.Args args) {
      AU_Debugger.debug(CLASS_NAME + 'Args to filter:' + args);

      Set<Id> idsToKeep = new Set<Id>();
      for (SObject newObj : args.newObjects) {
        Object newObjVal = newObj.get(fieldName);

        if ((newObjVal == null && expectedValue == null)
          || (newObjVal != null && newObjVal.equals(expectedValue))) {
          idsToKeep.add(newObj.Id);
        }
      }
      AU_TriggerEntry.Args result = args.reduce(idsToKeep);
      AU_Debugger.debug(CLASS_NAME + 'Reduced args to:' + result);
      return result;
    }
  }

  public class NewIdValueIsTypeFilter implements AU_ITriggerObjectsFilter {
    private final String CLASS_NAME = 'NewIdValueIsTypeFilter:';

    private final String idFieldName;
    private final Schema.SObjectType expectedType;

    public NewIdValueIsTypeFilter(String idFieldName, Schema.SObjectType expectedType) {
      this.idFieldName = idFieldName;
      this.expectedType = expectedType;
    }

    public AU_TriggerEntry.Args run(AU_TriggerEntry.Args args) {
      AU_Debugger.debug(CLASS_NAME + 'Args to filter:' + args);

      Set<Id> idsToKeep = new Set<Id>();
      for (SObject newObj : args.newObjects) {
        Object newObjVal = newObj.get(idFieldName);

        if (newObjVal instanceOf Id && ((Id) newObjVal).getSobjectType() == expectedType) {
          idsToKeep.add(newObj.Id);
        }
      }
      AU_TriggerEntry.Args result = args.reduce(idsToKeep);
      AU_Debugger.debug(CLASS_NAME + 'Reduced args to:' + result);
      return result;
    }
  }
}