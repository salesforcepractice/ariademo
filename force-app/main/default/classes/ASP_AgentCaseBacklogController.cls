/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
global with sharing class ASP_AgentCaseBacklogController {
  private static final String CLASS_NAME = 'ASP_AgentCaseBacklogController';

  @RemoteAction
  @AuraEnabled
  public static void createAgentWorkRecord(String caseId) {
    AU_Debugger.enterFunction(CLASS_NAME + '.createAgentWorkRecord');

    String myUserId = UserInfo.getUserId();

    try {
      ServiceChannel serviceChannel = [SELECT Id FROM ServiceChannel WHERE DeveloperName = 'ASP_AgentBacklogChannel'];

      AgentWork aw = new AgentWork(
        UserId = myUserId,
        ServiceChannelId = serviceChannel.Id,
        WorkItemId = caseId,
        CapacityWeight = 1
      );

      insert aw;
    } catch (DmlException ex) {
      AU_Debugger.reportException('AgentWork insertion failed', ex);
    }
    AU_Debugger.leaveFunction();
}
}