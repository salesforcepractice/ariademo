/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

global class ACP_SendBotResponseAction {

  private static final String API_KEY = 'vyfW6KJR1s9EGQMLYPzBM6ZLvDuQLoXx5GZpIkjg'; //FIXME make API key configurable

  private static final String CLASS_NAME = 'ACP_SendBotResponseAction.';

  @InvocableMethod(label='Send ACP Bot Response')
  global static List<String> sendBotResponse(List<ResponseArgs> args) {

    List<String> result = new List<String>();

    for (ResponseArgs arg : args) {
      System.enqueueJob(new ASP_RequestExecutionQueueable(new ACP_BotResponseBuilder(arg.callbackUrl, API_KEY)
        .withStatus(arg.statusCode)
        .withMessage(arg.message)
      ));

      result.add('SomeRequestGuid'); // FIXME: Create Request GUID and handle it in the RequestExecutionQueuable => Async HTTP Request handling
    }

    return result;
  }

  global class ResponseArgs {

    @InvocableVariable(label='Callback URL' required=true)
    global String callbackUrl;

    @InvocableVariable(label='Status Code' required=true)
    global Integer statusCode;

    @InvocableVariable(label='Message' required=true)
    global String message;
  }
}