/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Thursday November 7th 2019
 * Author: varonov
 * File type: '.cls'
 */


public with sharing class SC_UpdateTranscriptOnTaskUpdateHandler implements AU_TriggerEntry.ITriggerEntry {

  private static final String CLASS_NAME = 'SC_UpdateTranscriptOnTaskUpdateHandler';

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
  }
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount)  {
    AU_Debugger.enterFunction(CLASS_NAME + '.invokeMain:');
    AU_Debugger.debug('TriggerOperation: ' + args.operation);

    if (args.operation != TriggerOperation.AFTER_UPDATE) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<Task> newTasks = (List<Task>) args.newObjects;
    List<SC_TranscriptService.RelatedObjects> relatedObjects = new List<SC_TranscriptService.RelatedObjects>();
    for (Task t : newTasks) {
      relatedObjects.add(new SC_TranscriptService.RelatedObjects(t.CallObject, t.Id, t.WhatId, t.Interaction_Segment__c));
    }
    if (!Test.isRunningTest() && relatedObjects.size() <= 50) {
      System.enqueueJob(new SC_TranscriptService(relatedObjects));
    }
    AU_Debugger.leaveFunction();
  }
}