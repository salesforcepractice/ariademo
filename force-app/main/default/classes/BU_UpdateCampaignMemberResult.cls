global class BU_UpdateCampaignMemberResult {
    @InvocableMethod(label='Update Campaign Member Result')
    global static void updateResult(List<UpdateResultRequest> requests) {
        List<CampaignMember> membersToUpdate = new List<CampaignMember>();
        Map<String, CampaignMember> membersByCombinedIds = new Map<String, CampaignMember>();
        Set<String> pcCampaignIds = new Set<String>();
        Set<Id> contactIds = new Set<Id>();
        
        for (UpdateResultRequest request : requests) {
            pcCampaignIds.add(request.pcCampaignId);
            contactIds.add(request.contactId);
        }
        pcCampaignIds.remove(null);
        pcCampaignIds.remove('');
        contactIds.remove(null);
        
        for (CampaignMember cm : [
            SELECT 	Campaign.purecloud__PureCloud_Campaign_ID__c, ContactId
            FROM 	CampaignMember
            WHERE 	Campaign.purecloud__PureCloud_Campaign_ID__C in :pcCampaignIds AND
            		ContactId in :contactIds
        ]) {
            String key = cm.Campaign.purecloud__PureCloud_Campaign_ID__c + ' ' + String.valueOf(cm.ContactId);
            membersByCombinedIds.put(key, cm);
        }
        
        for (UpdateResultRequest request : requests) {
            String key = request.pcCampaignId + ' ' + String.valueOf(request.contactId);
            if (!membersByCombinedIds.containsKey(key))
                continue;
            
            CampaignMember cm = membersByCombinedIds.get(key);
            cm.purecloud__Last_Result__c = request.result;
            cm.purecloud__Last_Sync__c = System.now();
            membersToUpdate.add(cm);
        }
        
		if (!membersToUpdate.isEmpty())
            update membersToUpdate;
    }
    
    global class UpdateResultRequest {
        @InvocableVariable(required=true label='PureCloud Campaign Id')
        global String pcCampaignId;
        
        @InvocableVariable(required=true label='Contact Id')
        global Id contactId;
        
        @InvocableVariable(required=true label='Result')
        global String result;
    }
}