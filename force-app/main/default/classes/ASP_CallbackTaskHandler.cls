/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Wednesday April 8th 2020
 * Author: Vacheslav Aronov
 * File type: '.cls'
 */


public with sharing class ASP_CallbackTaskHandler implements AU_TriggerEntry.ITriggerEntry {

  private static final String CLASS_NAME = 'ASP_CallbackTaskHandler';

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
  }
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount)  {
    AU_Debugger.enterFunction(CLASS_NAME + '.invokeMain:');
    AU_Debugger.debug('TriggerOperation: ' + args.operation);

    if (args.operation != TriggerOperation.BEFORE_INSERT) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<Task> newTasks = (List<Task>) args.newObjects;
    for (Task t : newTasks) {
      if (t.WhatId != null) {
        Id objId = t.WhatId;
        if (String.valueOf(objId.getSObjectType()) == 'Callback__c' && t.CallType == 'Outbound')  {
          t.Subject = t.Subject.replace('Call', 'Callback');
        }
      }      
    }
  } 
}