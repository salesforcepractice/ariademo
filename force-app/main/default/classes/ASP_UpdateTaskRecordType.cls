/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Monday March 30th 2020
 * Author: Vacheslav Aronov
 * File type: '.cls'
 */


public with sharing class ASP_UpdateTaskRecordType implements AU_TriggerEntry.ITriggerEntry {

  private static final String CLASS_NAME = 'ASP_UpdateTaskRecordType';

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
  }
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount)  {
    AU_Debugger.enterFunction(CLASS_NAME + '.invokeMain:');
    AU_Debugger.debug('TriggerOperation: ' + args.operation);

    if (args.operation != TriggerOperation.BEFORE_INSERT && args.operation != TriggerOperation.BEFORE_UPDATE) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<Task> newTasks = (List<Task>) args.newObjects;
    for (Task t : newTasks) {
      AU_Debugger.debug(CLASS_NAME + ': Task Type: ' + t.Type);
      switch on t.Type {
        when 'Call' {
            t.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Call').getRecordTypeId();
        }
        when 'Message' {
            t.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
        }
        when 'Email' {
            t.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        }
        when 'Chat' {
           t.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Chat').getRecordTypeId();
        }    
        when else {
          t.RecordTypeId = null;
        }
      }
    }
  } 
}