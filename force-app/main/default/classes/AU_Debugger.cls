/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public class AU_Debugger {

  private static List<DebugEntry> DebugLog = new List<DebugEntry>();
  private static Integer CurrentLevel = 0;

  public static void enterFunction(String functionName) {
    CurrentLevel += 1;
    Debug('Invoked: ' + functionName);
  }

  public static void leaveFunction() {
    if (CurrentLevel > 0)
      CurrentLevel -= 1;

    if (CurrentLevel == 0)
      System.debug(LoggingLevel.Info, 'Callstack completed:\n' + getCurrentLogStatement());
  }

  public static void resetAndPrint() {
    CurrentLevel = 0;
    System.debug(LoggingLevel.Info, 'Reset stack:\n' + getCurrentLogStatement());
  }

  public static void debug(String debugString) {
    System.debug(debugString);
    DebugLog.add(new DebugEntry(debugString));
  }

  public static void reportException(String component, Exception ex) {
    reportException(component, ex, null);
  }

  public static void reportException(String component, Exception ex, Map<string, Object> extraInfo) {
    resetAndPrint();
    System.debug(LoggingLevel.Error, parseException(ex));
    insert createDebugInfo(component, ex, extraInfo);
  }

  public static void reportSaveErrors(String component, List<Database.SaveResult> results, Map<String, Object> extraInfo) {
    String description = '';

    for (Database.SaveResult result : results) {
      if (!result.isSuccess()) {
        description += 'Save error(s) on record ID ' + result.getId() + '\n';

        for (Database.Error err : result.getErrors()) {
          description += '  Status Code: ' + err.getStatusCode() + '\n';
          description += '  Fields: ' + String.join(err.getFields(), ',') + '\n';
          description += '  Message: ' + err.getMessage() + '\n';
        }

        description += '\n';
      }
    }

    if (description == '')
      return;

    string serializedExtra = null;
    try {
      if (extraInfo != null)
        serializedExtra = JSON.serialize(extraInfo);
    } catch (Exception serializeEx) {
      System.debug(LoggingLevel.Error, 'Error serializing extraInfo: ' + serializeEx.getMessage());
    }

    AU_DebugInfo__c debugInfo = new AU_DebugInfo__c(
      Component__c = component,
      Data__c = getCurrentLogStatement() + '\n' + description,
      Level__c = 'Error',
      ExtraInfo__c = serializedExtra
    );

    insert debugInfo;
  }

  public static void reportWarning(String component, String message) {
    AU_DebugInfo__c debugInfo = new AU_DebugInfo__c(
      Component__c = component,
      Data__c = getCurrentLogStatement() + '\n' + message,
      Level__c = 'Warning'
    );

    System.enqueueJob(new QueueableDebugInfoInserter(debugInfo));
  }

  public static AU_DebugInfo__c createDebugInfo(String component, Exception ex, Map<string, Object> extraInfo) {
    AU_DebugInfo__c debugInfo = new AU_DebugInfo__c();
    debugInfo.Component__c = component;
    debugInfo.Data__c = getCurrentLogStatement() + '\n' + parseException(ex);
    debugInfo.Level__c = 'Error';

    string serializedExtra = null;
    try {
      if (extraInfo != null)
        serializedExtra = JSON.serialize(extraInfo);
    } catch (Exception serializeEx) {
      System.debug(LoggingLevel.Error, 'Error serializing extraInfo: ' + serializeEx.getMessage());
    }

    debugInfo.ExtraInfo__c = serializedExtra;
    return debugInfo;
  }


  public static String getCurrentLogStatement() {
    if (DebugLog.size() == 0)
      return null;

    String spaces = '                                      ';
    String result = '';
    for (DebugEntry de : DebugLog) {
      Integer endIndex = 3 * de.level;
      if (endIndex >= spaces.length())
        endIndex = spaces.length() - 1;

      result += spaces.substring(0, endIndex) + de.debugData + '\n';
    }
    return result;
  }

  public static Map<string, Object> deserializeExtraInfo(AU_DebugInfo__c debugInfo) {
    if(debugInfo.ExtraInfo__c == null || debugInfo.ExtraInfo__c.length() == 0)
      return null;

    try {
      return (Map<string, Object>)JSON.deserializeUntyped(debugInfo.ExtraInfo__c);
    } catch(Exception ex) {
      System.debug(LoggingLevel.Error, 'Error deserializing DebugInfo.ExtraInfo: ' + ex.getMessage());
      return null;
    }
  }

  private static String parseException(Exception ex) {
    if (ex == null) {
      return 'No exception was provided.';
    }

    return 'Exception in ' + ex.getTypeName() + ' (' + ex.getLineNumber() + '): ' + ex.getMessage() + ' > ' + ex.getStackTraceString();
  }

  private class DebugEntry {
    Integer level;
    String debugData;

    public DebugEntry(String entryDesciption) {
      level = CurrentLevel;
      debugData = entryDesciption;
    }
  }

  private class QueueableDebugInfoInserter implements Queueable {

    private final AU_DebugInfo__c debugInfo;

    public QueueableDebugInfoInserter(AU_DebugInfo__c debugInfo) {
      this.debugInfo = debugInfo;
    }

    public void execute(QueueableContext context) {
      insert debugInfo;
    }

  }
}