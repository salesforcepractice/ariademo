/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class ASP_TriggerBusTest {

  @IsTest
  static void testDispatch() {
    SObjectType type = Account.getSObjectType();

    ASP_TriggerBus.resetHandlersWith(new Map<SObjectType, AU_TriggerEventDispatcher>{
      type => new AU_TriggerEventDispatcher(new ATU_TriggerEntryMock[]{
        new ATU_TriggerEntryMock()
      })
    });
    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      type,
      TriggerOperation.BEFORE_INSERT,
      false,
      new List<Sobject>{},
      new List<Sobject>{},
      new Map<Id,Sobject>{},
      new Map<Id,Sobject>{}
    );

    ASP_TriggerBus.dispatch(args);

    System.assertEquals(1, ATU_TriggerEntryMock.getMethodCount('invokeMain'), 'invokeMain calls');
    System.assertEquals(1, ATU_TriggerEntryMock.getMethodCount('invokeCompleted'), 'invokeCompleted calls');
  }
}