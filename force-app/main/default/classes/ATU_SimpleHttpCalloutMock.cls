/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@IsTest
global class ATU_SimpleHttpCalloutMock implements HttpCalloutMock {

  private final Options options;
  private final HttpRequest expectedRequest;
  private final HttpResponse responseToReturn;

  public ATU_SimpleHttpCalloutMock (
    HttpRequest expectedRequest,
    HttpResponse responseToReturn)
  {
    this(expectedRequest, responseToReturn, new Options());
  }

  public ATU_SimpleHttpCalloutMock (
    HttpRequest expectedRequest,
    HttpResponse responseToReturn,
    Options options
  ) {
    this.expectedRequest = expectedRequest;
    this.responseToReturn = responseToReturn;
    this.options = options;
  }

  global HTTPResponse respond(HTTPRequest actualRequest) {
    System.assertEquals(expectedRequest.getEndpoint(), actualRequest.getEndpoint());
    System.assertEquals(expectedRequest.getMethod(), actualRequest.getMethod());
    System.assertEquals(expectedRequest.getCompressed(), actualRequest.getCompressed());

    if (options.checkBody) System.assertEquals(expectedRequest.getBody(), actualRequest.getBody());

    return responseToReturn;
  }

  public class Options {
    public Boolean checkBody;

    public Options() {
      checkBody = true;
    }
  }
}