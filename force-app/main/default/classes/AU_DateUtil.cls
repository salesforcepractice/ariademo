/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

public class AU_DateUtil {

    public static Boolean getIsWeekendDay(Date dateToValidate)
    {
        Boolean isWeekendDay = false;
        AU_Debugger.debug('dateParam = ' + dateToValidate);
        //Recover the day of the week
        Date startOfWeek   = dateToValidate.toStartOfWeek();
        AU_Debugger.debug('startOfWeek: ' + startOfWeek);
        Integer dayOfWeek  = dateToValidate.day() - startOfWeek.day();
        AU_Debugger.debug('dayOfWeek: ' + dayOfWeek);
        isWeekendDay = dayOfWeek == 0 || dayOfWeek == 6 ? true : false;
        AU_Debugger.debug('isWeekendDay: '+ isWeekendDay);
        return isWeekendDay;
    }

    public static Date addBusinessDays(Date startDate, Integer businessDaysToAdd)
    {
        //Add or decrease in BusinessDaysToAdd days
        Date finalDate = startDate;
        AU_Debugger.debug('finalDate = ' + finalDate);
        integer direction = businessDaysToAdd < 0 ? -1 : 1;
        AU_Debugger.debug('direction = ' + direction);
        while(businessDaysToAdd != 0)
        {
            finalDate = finalDate.AddDays(direction);
            AU_Debugger.debug('BusinessDaysToAdd = ' + businessDaysToAdd);
            AU_Debugger.debug('finalDate = ' + finalDate);
            if (!getIsWeekendDay(finalDate))
            {
                businessDaysToAdd -= direction;
            }
        }

        return finalDate;
    }
}