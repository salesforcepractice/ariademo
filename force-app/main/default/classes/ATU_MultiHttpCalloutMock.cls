/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@IsTest
global class ATU_MultiHttpCalloutMock implements HttpCalloutMock {

  private final Options options;
  private final Map<HttpRequest, HttpResponse> requestResponseMap;

  public ATU_MultiHttpCalloutMock(Map<HttpRequest, HttpResponse> requestResponseMap) {
    this(requestResponseMap, new Options());
  }

  public ATU_MultiHttpCalloutMock(Map<HttpRequest, HttpResponse> requestResponseMap, Options options) {
    this.requestResponseMap = requestResponseMap;
    this.options = options;
  }

  global HttpResponse respond(HTTPRequest actualRequest) {
    System.assert(requestResponseMap.size() > 0,
      String.format('There are no requests expected. Actual call: {0} {1} => {2}', new String[] {
          actualRequest.getMethod(),
          actualRequest.getEndpoint(),
          actualRequest.getBody()
        }
      )
    );

    for (HttpRequest req : requestResponseMap.keySet()) {
      if (req.getEndpoint() == actualRequest.getEndpoint()
        && req.getMethod() == actualRequest.getMethod()
        && (!options.checkBody || req.getBody() == actualRequest.getBody())) {

        HttpResponse resp = requestResponseMap.get(req);
        requestResponseMap.remove(req);

        return resp;
      }
    }

    throw new AssertException(String.format('Could not find any mapping for request. Actual call: {0} {1} => {2}', new String[] {
      actualRequest.getMethod(),
      actualRequest.getEndpoint(),
      actualRequest.getBody()
    }));
  }

  public void assertAllRequestsInvoked() {
    System.assertEquals(0, requestResponseMap.size(), 'Not all requests have been invoked');
  }

  public class Options {
    public Boolean checkBody;

    public Options() {
      checkBody = true;
    }
  }
}