/********************************************************************************
 * Author kbowden
 * Date 10 Jan 2016
 ********************************************************************************/
@isTest
private class BB_LTG_SObjectBoardCtrlTest
{
  // FIXME: Re-enable test and fix compiler errors.
  TestMethod static void TestController()
  {
    List<Case> cases=new List<Case>();

    // insert a test case that will be included
    cases.add(new Case(Subject = 'Unit Test',
      Status = 'New',
      Description = 'Test'));

    // insert a test case that will be excluded
    cases.add(new Case(Subject = 'Unit Test',
      Status = 'Closed',
      Description = 'Test'));
    insert cases;

    List<BB_LTG_BoardStage> stages =
      BB_LTG_SObjectBoardCtrl.GetStages('Case', 'Status', null, null, null, null,
        'Closed', null, 'Subject,Description', 'IsClosed = false', 'CreatedDate DESC');

    ATU_DebugInfoUtil.assertNoErrors();

    System.assert(stages.size()>0);
    BB_LTG_BoardStage stage = stages[0];
    //System.assertEquals(1, stage.sobjects.size());
    //System.assertEquals('Subject', stage.sobjects[0].titleField.fieldName);
  }

}