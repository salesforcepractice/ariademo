/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 10/24/2018.
 */

public without sharing class ASP_ManageEmailInteractionOnAgentWork implements AU_TriggerEntry.ITriggerEntry {
  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
    AU_Debugger.enterFunction('ASP_ManageEmailInteractionOnAgentWork.invokeMain');

    if(args.operation != TriggerOperation.AFTER_UPDATE) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    AU_TriggerEntry.Args caseArgs = AU_TriggerUtil.filter(args).byNewValueIsType('WorkItemId', Case.SObjectType).run();

    List<AgentWork> acceptedWorks = (List<AgentWork>) AU_TriggerUtil.filter(caseArgs).byValueChanged('AcceptDateTime').run().newObjects;
    if (!acceptedWorks.isEmpty()) {
      createInteractions(acceptedWorks);
    }

    List<AgentWork> closedWorks = (List<AgentWork>) AU_TriggerUtil.filter(caseArgs).byValueChanged('CloseDateTime').run().newObjects;
    if (!closedWorks.isEmpty()) {
      updateSegments(closedWorks);
    }

    AU_Debugger.leaveFunction();
  }

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {}
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {}

  private void createInteractions(List<AgentWork> works) {
    AU_Debugger.enterFunction('ASP_CreateEmailInteractionOnAgentWork.createInteractions');

    Set<Id> caseIds = new Set<Id>();
    Map<Id, Id> agentWorkIdsByCaseId = new Map<Id, Id>();
    for (AgentWork work : works) {
      caseIds.add(work.WorkItemId);
      agentWorkIdsByCaseId.put(work.WorkItemId, work.Id);
    }

    List<ASP_Interaction__c> interactions = new List<ASP_Interaction__c>();
    List<ASP_Interaction_Segment__c> segments = new List<ASP_Interaction_Segment__c>();

    final Datetime NOW = System.now();
    
    for (Case c : [
      SELECT  Latest_Queue__c, Latest_Queued_Date__c, (
        SELECT  Subject, TextBody
        FROM    EmailMessages
        WHERE   Incoming = true
        ORDER BY CreatedDate
        LIMIT 1
      )
      FROM  Case
      WHERE Id in :caseIds AND Origin = 'Email'
    ]) {
      if (c.EmailMessages.isEmpty())
        continue;
      
      EmailMessage email = c.EmailMessages[0];
      ASP_Interaction__c interaction = new ASP_Interaction__c(
        Interaction_Id__c = email.Id
      );
      // TODO: Remove text copy of email and provide link to original EmailMessage
      ASP_Interaction_Segment__c segment = new ASP_Interaction_Segment__c(
        ASP_Interaction__r = interaction.clone(),
        EmailMessage_Id__c = email.Id,
        Interaction_Segment_Id__c = agentWorkIdsByCaseId.get(c.Id),
        Start_Time__c = NOW,
        Queue__c = c.Latest_Queue__c,
        Queue_Time__c = getSecondsBetween(c.Latest_Queued_Date__c, NOW),
        RecordTypeId = AU_RecordTypes.getId('ASP_Interaction_Segment__c', 'Email'),
        Email_Subject__c = email.Subject != null ? email.Subject.left(255) : '',
        Email_Message__c = email.TextBody != null ? email.TextBody.left(255) : ''
      );

      interactions.add(interaction);
      segments.add(segment);
    }

    if (!interactions.isEmpty()) {
      AU_Debugger.debug('Inserting ' + segments.size() + ' segments...');
      Database.upsert(interactions, ASP_Interaction__c.fields.Interaction_Id__c, true);
      insert segments;
    }

    AU_Debugger.leaveFunction();
  }

  private void updateSegments(List<AgentWork> works) {
    List<ASP_Interaction_Segment__c> segments = new List<ASP_Interaction_Segment__c>();
    for (AgentWork work : works) {
      segments.add(new ASP_Interaction_Segment__c(
        Interaction_Segment_Id__c = work.Id,
        End_Time__c = work.CloseDateTime,
        Active_Time__c = work.ActiveTime
      ));
    }
    // If segments were not already created, then there would be partial failures which is expected
    Database.upsert(segments, ASP_Interaction_Segment__c.Fields.Interaction_Segment_Id__c, false);
  }

  private Long getSecondsBetween(Datetime dt1, Datetime dt2) {
      return 
        dt1 != null && dt2 != null ?
          Math.abs(dt1.getTime() - dt2.getTime()) / 1000
        : null;
  }
}