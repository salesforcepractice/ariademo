/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Wednesday November 13th 2019
 * Author: varonov
 * File type: '.cls'
 */


public with sharing class SC_RecordingJsonDeserializationCall {
  public String id;
  public String conversationId;
  public String startTime;
  public String endTime;
  public String media;
  public List<Annotations> annotations;
  public String fileState;
  public MediaUris mediaUris;
	
  public class MediaData {
    public String mediaUri;
    public List<Double> waveformData;
  }

  public class MediaUris {
	public MediaData mediaData;
  } 

  public class Annotations {}

  public static List<SC_RecordingJsonDeserializationCall> parse(String json) {
	return (List<SC_RecordingJsonDeserializationCall>) System.JSON.deserialize(json.replace('"0":', '"mediaData":'), List<SC_RecordingJsonDeserializationCall>.class);
  }
}