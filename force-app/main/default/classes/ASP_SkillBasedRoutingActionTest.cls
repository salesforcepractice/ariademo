/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 7/10/2018.
 * Unit test for ASP_SkillBasedRoutingAction and ASP_AddSkillsToRoutingAction
 * If skill-based routing for Omni is not enabled, this test (with the rest of the package) should be removed.
 * If there are no Case Service Channels in the org, the test method needs to be re-written.
 */

@isTest
private class ASP_SkillBasedRoutingActionTest {
  public static Skill testSkill {
    get {
      if (testSkill == null) {
        List<Skill> skills = [SELECT DeveloperName FROM Skill ORDER BY DeveloperName];
        if (skills.size() > 0)
          testSkill = skills[0];
      }
      return testSkill;
    }
    set;
  }

  class Test_CaseSkillResolver extends ASP_SkillRequirementResolver {
    public override SObjectType getSObjectType() {
      return Case.SObjectType;
    }

    public override String getSkillDeveloperName() {
      return testSkill != null ? testSkill.DeveloperName : '';
    }

    public override Set<String> getRequiredFields() {
      List<String> fields = new List<String> {
        'Subject'
      };

      return new Set<String>(fields);
    }
    public override Integer getSkillLevel(SObject workItem) {
      Case c = (Case) workItem;
      Integer level = Math.mod(c.Subject.length(), 10);
      return level;
    }
  }

  static ASP_SkillBasedRoutingAction.RoutingItem createRoutingItem(Id workItemId, String serviceChannelName) {
    ASP_SkillBasedRoutingAction.RoutingItem routingItem = new ASP_SkillBasedRoutingAction.RoutingItem();
    routingItem.workItemId = workItemId;
    routingItem.capacityWeight = 1;
    routingItem.routingModel = 'MostAvailable';
    routingItem.routingPriority = 1;
    routingItem.serviceChannelName = serviceChannelName;
    return routingItem;
  }

  @testSetup
  static void setup() {
    List<Case> cases = new List<Case> {
      new Case(Subject = '123'),
      new Case(Subject = '12345')
    };
    insert cases;
  }


  @isTest
  static void testRouteWithSkillsAsync() {
    ASP_GlobalResolver.resetWithResolver(Case.SObjectType, new Test_CaseSkillResolver());

    List<ServiceChannel> channels = [SELECT DeveloperName FROM ServiceChannel WHERE RelatedEntity = 'Case'];
    // if there are no Case Service Channels, re-write this method

    if (testSkill == null || channels.size() == 0)
      return;

    ServiceChannel channel = channels[0];
    List<Case> cases = [SELECT Id FROM Case];

    List<ASP_SkillBasedRoutingAction.RoutingItem> routingItems = new List<ASP_SkillBasedRoutingAction.RoutingItem> {
      createRoutingItem(cases[0].Id, channel.DeveloperName),
      createRoutingItem(cases[1].Id, channel.DeveloperName)
    };

    Test.startTest();
    ASP_SkillBasedRoutingAction.routeWithSkillsAsync(routingItems);
    Test.stopTest();

    List<PendingServiceRouting> psrs = [
      SELECT  WorkItemId
      FROM    PendingServiceRouting
      WHERE   ServiceChannelId = :channel.Id
          AND WorkItemId IN :new Map<Id, Case>(cases).keySet()
    ];
    system.assertEquals(2, psrs.size());
    List<SkillRequirement> srs = [
      SELECT  SkillLevel
      FROM    SkillRequirement
      WHERE   RelatedRecordId IN :new Map<Id, PendingServiceRouting>(psrs).keySet()
      ORDER BY SkillLevel
    ];
    system.assertEquals(2, srs.size());
    system.assertEquals(3, srs[0].SkillLevel);
    system.assertEquals(5, srs[1].SkillLevel);
  }

  @isTest
  static void testAddSkillRequirements() {
    ASP_GlobalResolver.resetWithResolver(Case.SObjectType, new Test_CaseSkillResolver());

    List<ServiceChannel> channels = [SELECT DeveloperName FROM ServiceChannel WHERE RelatedEntity = 'Case'];
    // if there are no Case Service Channels, re-write this method

    if (testSkill == null || channels.size() == 0)
      return;

    ServiceChannel channel = channels[0];
    List<Case> cases = [SELECT Id FROM Case ORDER BY Subject];

    List<PendingServiceRouting> psrs = new List<PendingServiceRouting> {
      new PendingServiceRouting(
        IsReadyForRouting = false,
        CapacityWeight = 1,
        RoutingType = 'SkillsBased',
        RoutingModel = 'MostAvailable',
        RoutingPriority = 1,
        ServiceChannelId = channel.Id,
        WorkItemId = cases[0].Id
      ),
      new PendingServiceRouting(
        IsReadyForRouting = false,
        CapacityWeight = 1,
        RoutingType = 'SkillsBased',
        RoutingModel = 'MostAvailable',
        RoutingPriority = 1,
        ServiceChannelId = channel.Id,
        WorkItemId = cases[1].Id
      )
    };
    insert psrs;
    List<Id> psrIds = new List<Id>(new Map<Id, SObject>(psrs).keySet());

    ASP_AddSkillsToRoutingAction.addSkillRequirements(psrIds);

    List<SkillRequirement> srs = [
      SELECT  SkillLevel
      FROM    SkillRequirement
      WHERE   RelatedRecordId IN :new Map<Id, PendingServiceRouting>(psrs).keySet()
      ORDER BY SkillLevel
    ];
    system.assertEquals(2, srs.size());
    system.assertEquals(3, srs[0].SkillLevel);
    system.assertEquals(5, srs[1].SkillLevel);
  }
}