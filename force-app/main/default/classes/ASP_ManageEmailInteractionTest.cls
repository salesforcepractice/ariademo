/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 10/26/2018.
 */

@isTest
private class ASP_ManageEmailInteractionTest {
  @testSetup
  private static void setup() {
    List<Case> cases = new List<Case> {
      ATU_Case.build('1. Test Email Case')
              .withField('Origin', 'Email')
              .withField('Latest_Queue__c', 'ASP_ManageEmailInteractionTestQueue')
              .withField('Latest_Queued_Date__c', System.now())
              .create(),
      ATU_Case.build('2. Test Phone Case')
              .withField('Origin', 'Phone')
              .withField('Latest_Queue__c', 'ASP_ManageEmailInteractionTestQueue')
              .withField('Latest_Queued_Date__c', System.now())
              .create()
    };
    insert cases;
    
    EmailMessage em = new EmailMessage(
      ParentId = cases[0].Id,
      Incoming = true
    );
    insert em;
  }

  @isTest
  private static void testUpdate() {
    List<Case> cases = [SELECT Id FROM Case ORDER BY Subject];
    List<AgentWork> work = new List<AgentWork> {
      new AgentWork(
        Id = '0Bz000000000000001',
        UserId = UserInfo.getUserId(),
        WorkItemId = cases[0].Id
      ),
      new AgentWork(
        Id = '0Bz000000000000002',
        UserId = UserInfo.getUserId(),
        WorkItemId = cases[1].Id
      )
    };

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      AgentWork.SObjectType,
      TriggerOperation.AFTER_UPDATE,
      true,
      work,
      null,
      null,
      null
    );

    ASP_ManageEmailInteractionOnAgentWork handler = new ASP_ManageEmailInteractionOnAgentWork();
    handler.invokeMain(args, 1);

    List<ASP_Interaction_Segment__c> segments = [
      SELECT  ASP_Interaction__r.Interaction_Id__c, Agent_Identifier_Name__c, Interaction_Segment_Id__c,
              Start_Time__c, Queue__c, Queue_Time__c
      FROM    ASP_Interaction_Segment__c
    ];
    EmailMessage em = [SELECT Id from EmailMessage];
    System.assertEquals(1, segments.size());
    System.assertEquals(em.Id, segments[0].ASP_Interaction__r.Interaction_Id__c);
    System.assertEquals(work[0].Id, segments[0].Interaction_Segment_Id__c);
    System.assertNotEquals(null, segments[0].Start_Time__c);
    System.assertEquals('ASP_ManageEmailInteractionTestQueue', segments[0].Queue__c);
    System.assertNotEquals(null, segments[0].Queue_Time__c);

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void testUnsupportedEvents() {
    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      AgentWork.SObjectType,
      TriggerOperation.BEFORE_DELETE,
      true,
      null,
      null,
      null,
      null
    );
    ASP_ManageEmailInteractionOnAgentWork handler = new ASP_ManageEmailInteractionOnAgentWork();
    handler.invokeMain(args, 1);
    handler.invokeWhileInProgress(args, 1);
    handler.invokeCompleted(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
  }
}