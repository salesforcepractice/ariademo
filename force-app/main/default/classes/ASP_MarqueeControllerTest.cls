/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 03-Apr-18. 
 */

@IsTest
private class ASP_MarqueeControllerTest {
  @isTest
  static void getRecordsTest() {
    List<ASP_Marquee__c> insertedPosts = createMarquees();

    List<ASP_Marquee__c> displayedPosts = ASP_MarqueeController.getRecords();
    Set<Id> displayedPostIds = (new Map<Id, ASP_Marquee__c>(displayedPosts)).keySet();

    System.assertEquals(2, ASP_MarqueeController.getRecords().size());
    System.assert(displayedPostIds.contains(insertedPosts[0].Id));
    System.assert(!displayedPostIds.contains(insertedPosts[1].Id));
    System.assert(displayedPostIds.contains(insertedPosts[2].Id));
  }

  // Helper methods

  private static List<ASP_Marquee__c> createMarquees() {
    List<ASP_Marquee__c> insertedPosts = new List<ASP_Marquee__c> {
        new ASP_Marquee__c(
            Title__c = 'Internal Message',
            Start_Date__c = Date.today(),
            User_Group__c = 'Internal',
            Active__c = true
        ),
        new ASP_Marquee__c(
            Title__c = 'Community Message',
            Start_Date__c = Date.today(),
            User_Group__c = 'Community',
            Active__c = true
        ),
        new ASP_Marquee__c(
            Title__c = 'Internal and Community Message',
            Start_Date__c = Date.today(),
            User_Group__c = 'Internal;Community',
            Show_Posted_Date__c = false,
            Active__c = true
        )
    };
    insert insertedPosts;
    return insertedPosts;
  }
}