/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 8/7/2018.
 */

public class AWS_ConnectContact {
  private static final String ENDPOINT_BASE = 'https://connect.us-east-1.amazonaws.com';
  private static final String AWS_SERVICE = 'connect';
  private static final String CLASS_NAME = 'AWS_ConnectContact';

  private AWS_Connector connector;

  public AWS_ConnectContact(AWS_Connector connector) {
    this.connector = connector;
  }

  public String StartOutboundVoiceContact(StartContactRequestBody body) {
    AU_Debugger.enterFunction(CLASS_NAME + '.StartOutboundVoiceContact');
    AU_Debugger.debug('Arguments: ' + String.valueOf(body));

    String contactId;

    HttpRequest request = connector.signedRequest(
      AWS_SERVICE,
      'PUT',
      new Url(ENDPOINT_BASE + '/contact/outbound-voice'),
      new Map<String, String> {
        'Content-type' => 'application/json'
      },
      Blob.valueOf(JSON.serialize(body)),
      false
    );

    Http http = new Http();
    HttpResponse response = http.send(request);

    if (response.getStatusCode() == 200) {
      Map<String, Object> respMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
      contactId = (String) respMap.get('ContactId');
    }
    else {
      AU_Debugger.leaveFunction();
      throw new AWS_ConnectRequestException(response);
    }

    AU_Debugger.leaveFunction();
    return contactId;
  }

  public void StopContact(StopContactRequestBody body) {
    AU_Debugger.enterFunction(CLASS_NAME + '.StopContact');
    AU_Debugger.debug('Arguments: ' + String.valueOf(body));

    HttpRequest request = connector.signedRequest(
      AWS_SERVICE,
      'POST',
      new Url(ENDPOINT_BASE + '/contact/stop'),
      new Map<String, String> { 'Content-Type' => 'application/json' },
      Blob.valueOf(JSON.serialize(body)),
      false
    );

    Http http = new Http();
    HttpResponse response = http.send(request);

    AU_Debugger.leaveFunction();
    if (response.getStatusCode() != 200) {
      throw new AWS_ConnectRequestException(response);
    }
  }

  public class StartContactRequestBody {
    public Map<String, String> Attributes;
    public String ClientToken;
    public String ContactFlowId;
    public String DestinationPhoneNumber;
    public String InstanceId;
    public String QueueId;
    public String SourcePhoneNumber;
  }

  public class StopContactRequestBody {
    public String ContactId;
    public String InstanceId;
  }

  public class AWS_ConnectRequestException extends Exception {
    public AWS_ConnectRequestException(HttpResponse response) {
      setMessage(String.format(
        'Status Code: {0} \nStatus: {1} \nResponse Body: {2}',
        new List<String> {
          String.valueOf(response.getStatusCode()),
          response.getStatus(),
          response.getBody()
        }
      ));
    }
  }
}