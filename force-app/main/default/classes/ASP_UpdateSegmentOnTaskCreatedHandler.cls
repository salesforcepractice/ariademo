/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 15-May-19. 
 */

public class ASP_UpdateSegmentOnTaskCreatedHandler implements AU_TriggerEntry.ITriggerEntry {

  private static final String CLASS_NAME = 'ASP_UpdateSegmentOnTaskCreatedHandler';

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
  }
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount)  {
    AU_Debugger.enterFunction(CLASS_NAME + '.invokeMain:');
    AU_Debugger.debug('TriggerOperation: ' + args.operation);

    if ((args.operation != TriggerOperation.AFTER_UPDATE && args.operation != TriggerOperation.AFTER_INSERT) || invocationCount > 1) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<Task> newTasks = (List<Task>) args.newObjects;
    List<Task> interactionTasks = new List<Task>();

    for (Task t : newTasks)  {
      if ((t.Subject.contains('Call') || t.Subject.contains('Email') || t.Subject.contains('SMS')) && t.CallObject.contains('-')) {
        interactionTasks.add(t);
      }
    }
    if (!interactionTasks.isEmpty()) {
      switch on args.operation  {
        when AFTER_INSERT {
          createInteractionSegment(interactionTasks);
        }
        when AFTER_UPDATE {
          updateInteractionSegment(interactionTasks);
        }
      }
    }
    AU_Debugger.leaveFunction();
  }

  private void createInteractionSegment(List<Task> tasks)  {
    List<ASP_Interaction__c> interactions = new List<ASP_Interaction__c>();
    List<ASP_Interaction_Segment__c> segments = new List<ASP_Interaction_Segment__c>();
    List<ASP_Related_Objects__c> relatedObjects = new List<ASP_Related_Objects__c>();
    Map<Id, ASP_Interaction_Segment__c> taskIdsToSegments = new Map<Id, ASP_Interaction_Segment__c>();
    Set<Id> taskRelatedItemsIds = new Set<Id>();
    String interactionType = 'Phone';

    for (Task t : tasks) {
      if (t.Subject.contains('Call'))  {
        interactionType = 'Phone';
      }
      else if (t.Subject.contains('Email'))  {
        interactionType = 'Email';
      }
      else if (t.Subject.contains('SMS')) {
        interactionType = 'Chat';
      }

      ASP_Interaction__c interaction = new ASP_Interaction__c(Interaction_Id__c = t.CallObject);
      ASP_Interaction_Segment__c segment = new ASP_Interaction_Segment__c(
          Interaction_Segment_Id__c = t.Id,
          RecordTypeId = AU_RecordTypes.getId('ASP_Interaction_Segment__c', interactionType),
          ASP_Interaction__r = interaction.clone()
      );
      interactions.add(interaction);
      segments.add(segment);
      taskIdsToSegments.put(t.Id, segment);

      if (t.WhatId != null) {
        taskRelatedItemsIds.add(t.WhatId);
      }
      if (t.WhoId != null) {
        taskRelatedItemsIds.add(t.WhoId);
      }
    }
    if (!interactions.isEmpty())  {
      Database.upsert(interactions, ASP_Interaction__c.fields.Interaction_Id__c, true);
      insert segments;
    }
    List<Task> tasksWithSegments = updateSegmentLookupOnTask(taskIdsToSegments);

    Map<Id, Case> caseIdToCase = new Map<Id, Case> ([SELECT Id, AccountId, ContactId FROM Case WHERE Id IN :taskRelatedItemsIds]);
    Map<Id, Opportunity> opportunityIdToOpportunity = new Map<Id, Opportunity> ([SELECT Id, AccountId FROM Opportunity WHERE Id IN :taskRelatedItemsIds]);
    Map<Id, Contact> contactIdToContact = new Map<Id, Contact> ([SELECT Id, AccountId FROM Contact WHERE Id IN :taskRelatedItemsIds]);

    for (Task t : tasksWithSegments) {
      Set<Id> relatedIds = new Set<Id>();

      if (t.WhatId != null) {
        relatedIds.add(t.WhatId);
        if (t.WhatId.getSObjectType() == Schema.Case.SObjectType) {
          Case c = caseIdToCase.get(t.WhatId);
          relatedIds.add(c.AccountId);
          relatedIds.add(c.ContactId);
        }
        else if (t.WhatId.getSObjectType() == Schema.Opportunity.SObjectType) {
          Opportunity opp = opportunityIdToOpportunity.get(t.WhatId);
          relatedIds.add(opp.AccountId);
        }
      }
      if (t.WhoId != null)  {
        relatedIds.add(t.WhoId);
        if (t.WhoId.getSobjectType() == Schema.Contact.SObjectType) {
          Contact c = contactIdToContact.get(t.WhoId);
          relatedIds.add(c.AccountId);
        }
      }

      relatedIds.remove(null);
      for (Id relatedId : relatedIds) {
        relatedObjects.add(createRelatedObject(t.Interaction_Segment__c, relatedId));
      }
    }

    insert relatedObjects;
  }

  private void updateInteractionSegment(List<Task> tasks) {
    List<ASP_Interaction_Segment__c> segments = new List<ASP_Interaction_Segment__c>();

    for (Task t : tasks) {
      String sentiment = 'Neutral';
      if (t.Sentiment_Score__c < -0.15)  {
        sentiment = 'Negative';
      }
      else if (t.Sentiment_Score__c > -0.15) {
        sentiment = 'Positive';
      }
      segments.add(new ASP_Interaction_Segment__c (
          Id = t.Interaction_Segment__c,
          After_Work_Duration__c = t.After_Work_Duration__c,
          Disposition__c = t.CallDisposition,
          Call_Duration__c = t.CallDurationInSeconds,
          Queue__c = t.Queue__c,
          Sentiment_Probability__c = t.Sentiment_Score__c,
          Sentiment__c = sentiment,
          SubType__c = t.CallType,
          Transcription__c = t.Transcript__c,
          Notes__c = t.Description,
          Interaction_Details_Link__c = t.Interaction_Details_Link__c,
          Client_Name__c = t.Interaction_Name__c,
          Client_Email__c = t.Caller_Phone__c,
          Client_Phone__c = t.Caller_Phone__c,
          Queue_Time__c = t.QueueTime__c
      )
      );
    }
    update segments;
  }

  private List<Task> updateSegmentLookupOnTask(Map<Id, ASP_Interaction_Segment__c> taskIdsToSegments) {
    List<Task> tasks = [SELECT Id, Interaction_Segment__c, WhatId, WhoId FROM Task WHERE Id IN :taskIdsToSegments.keySet()];
    for (Task t : tasks) {
      ASP_Interaction_Segment__c segment = taskIdsToSegments.get(t.Id);
      t.Interaction_Segment__c = segment.Id;
    }
    update tasks;
    return tasks;
  }

  private ASP_Related_Objects__c createRelatedObject(Id segmentId, Id relatedObjectId)  {
    ASP_Related_Objects__c relatedObject = new ASP_Related_Objects__c();
    String objectId = String.valueOf(relatedObjectId);
    switch on objectId.substring(0, 3) {
      when '001' {
        relatedObject = new ASP_Related_Objects__c(Interaction_Segment__c = segmentId, Account__c = objectId, Object_Type__c = 'Account');
      }
      when '003' {
        relatedObject = new ASP_Related_Objects__c(Interaction_Segment__c = segmentId, Contact__c = objectId, Object_Type__c = 'Contact');
      }
      when '500' {
        relatedObject = new ASP_Related_Objects__c(Interaction_Segment__c = segmentId, Case__c = objectId, Object_Type__c = 'Case');
      }
      when '006' {
        relatedObject = new ASP_Related_Objects__c(Interaction_Segment__c = segmentId, Opportunity__c = objectId, Object_Type__c = 'Opportunity');
      }
      when '00Q' {
        relatedObject = new ASP_Related_Objects__c(Interaction_Segment__c = segmentId, Lead__c = objectId, Object_Type__c = 'Lead');
      }
    }
    return relatedObject;
  }
}