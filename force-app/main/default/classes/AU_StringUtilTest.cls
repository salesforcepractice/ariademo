/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
private class AU_StringUtilTest {

  @isTest static void testTruncate() {
    System.assertEquals(null, AU_StringUtil.truncate(null, 1));
    System.assertEquals('', AU_StringUtil.truncate('', 1));
    System.assertEquals('', AU_StringUtil.truncate('Foo', 0));
    System.assertEquals('Foo', AU_StringUtil.truncate('Foo', 4));
    System.assertEquals('F', AU_StringUtil.truncate('FooBar', 1));
  }
}