/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 7/9/2018.
 */

@isTest
private class ASP_SkillRequirementManagerTest {
  public static Skill testSkill {
    get {
      if (testSkill == null) {
        List<Skill> skills = [SELECT DeveloperName FROM Skill ORDER BY DeveloperName];
        if (skills.size() > 0)
          testSkill = skills[0];
      }
      return testSkill;
    }
    set;
  }

  class Test_CaseSkillResolver extends ASP_SkillRequirementResolver {
    public override SObjectType getSObjectType() {
      return Case.SObjectType;
    }

    public override String getSkillDeveloperName() {
      return testSkill != null ? testSkill.DeveloperName : '';
    }

    public override Set<String> getRequiredFields() {
      List<String> fields = new List<String> {
        'Subject'
      };

      return new Set<String>(fields);
    }
    public override Integer getSkillLevel(SObject workItem) {
      Case c = (Case) workItem;
      Integer level = Math.mod(c.Subject.length(), 10);
      return level;
    }
  }

  @isTest
  static void testCreateSkillRequirements() {
    if (testSkill == null)
      return; // abort test if no Skills are created in org; consider deleting this package if skills are not used

    ASP_SkillRequirementManager manager = new ASP_SkillRequirementManager(
      new Map<SObjectType, List<ASP_SkillRequirementResolver> > {
        Case.SObjectType => new List<ASP_SkillRequirementResolver> {
          new Test_CaseSkillResolver()
        }
      }
    );

    List<Case> cases = new List<Case> {
      new Case(Subject = '123'),
      new Case(Subject = '12345')
    };
    insert cases;

    List<PendingServiceRouting> routingItems = new List<PendingServiceRouting> {
      new PendingServiceRouting(WorkItemId = cases[0].Id),
      new PendingServiceRouting(WorkItemId = cases[1].Id),
      new PendingServiceRouting()
    };
    List<SkillRequirement> srs = manager.createSkillRequirements(routingItems);

    system.assertEquals(2, srs.size());
    system.assertEquals(testSkill.Id, srs[0].SkillId);
    system.assertEquals(3, srs[0].SkillLevel);
    system.assertEquals(testSkill.Id, srs[1].SkillId);
    system.assertEquals(5, srs[1].SkillLevel);
  }
}