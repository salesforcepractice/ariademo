/** Copyright 2018, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this 
* code in all of their Salesforce Orgs (Production, Sandboxes), but 
* any form of distribution to other Salesforce Orgs not belonging to 
* the customer require a written permission from Aria Solutions. 
*
* Created by jliu on 02/26/2020.
*/

public without sharing class ASP_CaseCallTaskHandler implements AU_TriggerEntry.ITriggerEntry {
    public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount) {
        AU_Debugger.enterFunction('ASP_CaseCallTaskHandler.invokeMain');
        
        if (args.operation != TriggerOperation.AFTER_INSERT) {
            AU_Debugger.debug('Operation not supported.');
            AU_Debugger.leaveFunction();
            return;
        }
        
        Map<String, Case> casesByInteractionIds = new Map<String, Case>();
        for (Case c : (List<Case>) args.newObjects) {
            if (String.isNotBlank(c.PureCloud_Interaction_Id__c)) {
                casesByInteractionIds.put(c.PureCloud_Interaction_Id__c, c);
            }
        }
        
        if (!casesByInteractionIds.isEmpty()) {
            List<Task> toUpdate = new List<Task>();
            for (Task t : [
                SELECT 	CallObject
                FROM 	Task
                WHERE 	WhatId = null AND CallObject in :casesByInteractionIds.keySet()
            ]) {
                Case c = casesByInteractionIds.get(t.CallObject);
                t.WhatId = c.Id;
                toUpdate.add(t);
            }
            
            if (!toUpdate.isEmpty()) {
                update toUpdate;
            }
        }
        
        AU_Debugger.leaveFunction();
    }
    
    public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {}
    public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {}
}