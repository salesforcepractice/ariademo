/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Defines how a specific skill level is calculated for a given skill/sobject combination.
 * Created by jliu on 6/11/2018.
 */

public abstract without sharing class ASP_SkillRequirementResolver {
  private static Map<String, Id> skillIdsByDeveloperName;
  static {
    skillIdsByDeveloperName = new Map<String, Id>();
    for (Skill skill : [
      SELECT  DeveloperName
      FROM    Skill
    ]) {
      skillIdsByDeveloperName.put(skill.DeveloperName, skill.Id);
    }
  }

  private Id skillId;

  public abstract SObjectType getSObjectType();
  public abstract String getSkillDeveloperName();
  public abstract Set<String> getRequiredFields();
  public abstract Integer getSkillLevel(SObject workItem);

  protected ASP_SkillRequirementResolver() {
    String skillName = getSkillDeveloperName();
    if (!skillIdsByDeveloperName.containsKey(skillName))
      throw new SkillNotFoundException('No Skill named ' + skillName);

    skillId = skillIdsByDeveloperName.get(skillName);
  }

  public SkillRequirement createSkillRequirement(SObject workItem, Id relatedRecordId) {
    if (workItem.getSObjectType() != getSObjectType())
      return null;

    Integer level = getSkillLevel(workItem);
    SkillRequirement requirement = new SkillRequirement(
      RelatedRecordId = relatedRecordId,
      SkillId = skillId,
      SkillLevel = level
    );

    return requirement;
  }

  public class SkillNotFoundException extends Exception {}
}