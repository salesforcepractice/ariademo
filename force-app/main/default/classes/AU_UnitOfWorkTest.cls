/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
private class AU_UnitOfWorkTest {
  @IsTest
  static void testMonitorNew() {
    AU_UnitOfWork UOW = new AU_UnitOfWork(
      new Schema.SObjectType[]{
        Account.SObjectType,
        Opportunity.SObjectType,
        Contact.SObjectType
      });

    Account a                 = ATU_Account.build('TestAccount').create();
    Account a1                = ATU_Account.build('TestAccount1').create();
    Account a2                = ATU_Account.build('TestAccount1').create();
    Opportunity o1            = ATU_Opportunity.build(null).withName('SalesOrdersTestAccount - Op #1').withStageName('Qualification').create();
    Contact c                 = ATU_Contact.build(null,null).create();
    c.FirstName               = 'al';
    c.LastName                = 'Jo';
    c.Email                   = 'name@company.com';
    c.Phone                   = '4543345443';

    UOW.registerNew(a);
    UOW.monitorNew(a1);
    UOW.monitorNew(a2);
    UOW.registerNew(o1, Opportunity.AccountId, a);
    UOW.monitorNew(c, Contact.AccountId, a);
    UOW.commitWork();

    List<Contact> contacts = UOW.getMonitoredRecords(Contact.SObjectType);
    List<Account> accounts = UOW.getMonitoredRecords(Account.SObjectType);

    System.assertEquals(2, accounts.size(), 'MonitorNew contains the incorrect number of records');
    System.assertEquals(1, contacts.size(), 'MonitorNew contains the incorrect number of records');

    UOW = new AU_UnitOfWork(
      new Schema.SObjectType[]{
        Account.SObjectType,
        Contact.SObjectType
      });

    for (Contact con : contacts) {
      accounts[0].Name = 'New Name';
      UOW.monitorDirty(accounts[0]);
      UOW.monitorDirty(con, Contact.AccountId, accounts[0]);
      UOW.commitWork();
    }
  }
}