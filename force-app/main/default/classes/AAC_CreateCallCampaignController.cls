/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 7/17/2018.
 */

public with sharing class AAC_CreateCallCampaignController {
  private static final String CLASS_NAME = 'AAC_CreateCallCampaignController';

  @AuraEnabled
  public static Contact getUserContact() {
    Id userId = UserInfo.getUserId();
    List<User> users = [
      select  Contact.Phone, Contact.Name
      from    User
      where   Id = :userId
      and ContactId != null
    ];

    if (users.size() != 1)
      return null;

    return users[0].Contact;
  }

  @AuraEnabled
  public static void createCallCampaignWithContact(Id contactId, String phone, String comments) {
    AU_Debugger.enterFunction(CLASS_NAME + '.createCallCampaignWithContact');

    try {
      AAC_Call_Campaign__c cc = new AAC_Call_Campaign__c(
        Contact__c = contactId,
        Phone_Number__c = phone,
        Comments__c = comments
      );
      AU_Debugger.debug('Creating Call Campaign with field values: ' + String.valueOf(cc));
      insert cc;
    }
    catch (Exception e) {
      AU_Debugger.reportException(CLASS_NAME, e);
    }
    finally {
      AU_Debugger.leaveFunction();
    }
  }

  @AuraEnabled
  public static void createCallCampaign(String firstName, String lastName, String phone, String comments) {
    AU_Debugger.enterFunction(CLASS_NAME + '.createCallCampaign');

    try {
      String combinedComments =
        'First Name: ' + firstName + '\n' +
          'Last Name: ' + lastName + '\n\n' +
          'Comments:\n' + comments;

      AAC_Call_Campaign__c cc = new AAC_Call_Campaign__c(
        Phone_Number__c = phone,
        Comments__c = combinedComments
      );
      AU_Debugger.debug('Creating Call Campaign with field values: ' + String.valueOf(cc));
      insert cc;
    }
    catch (Exception e) {
      AU_Debugger.reportException(CLASS_NAME, e);
    }
    finally {
      AU_Debugger.leaveFunction();
    }
  }
}