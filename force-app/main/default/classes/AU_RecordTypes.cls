/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

public class AU_RecordTypes {

  private static Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();

  public static Map<String, Schema.RecordTypeInfo> getAll(String sObjectType) {
    if (schemaMap.containsKey(sObjectType)) {
      return schemaMap.get(sObjectType).getDescribe().getRecordTypeInfosByDeveloperName();
    } else {
      throw new SObjectNotFoundException('AU_RecordTypes.get(String): sObject :' + sObjectType + ' is not a valid object API Name.');
    }
  }

  public static RecordTypeInfo get(String sObjectType, String developerName) {
    return getRecordTypeInfo(sObjectType, developerName);
  }

  public static Id getId(String sObjectType, String developerName) {
    return getRecordTypeInfo(sObjectType, developerName).getRecordTypeId();
  }

  private static RecordTypeInfo getRecordTypeInfo(String sObjectType, String developerName) {
    Map<String, RecordTypeInfo> recordTypeInfo = getAll(sObjectType);

    if (recordTypeInfo.get(developerName) != null) {
      return recordTypeInfo.get(developerName);
    }

    throw new RecordTypeNotFoundException('AU_RecordTypes.get(String, String): RecordTypeInfo :' + developerName + ' is not a valid Record Type API name for sObject:' + sObjectType);
  }

  public class RecordTypeNotFoundException extends Exception {}
  public class SObjectNotFoundException extends Exception {}
}