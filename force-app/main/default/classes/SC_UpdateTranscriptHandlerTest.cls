/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday November 8th 2019
 * Author: varonov
 * File type: '.cls'
 */

@isTest
public with sharing class SC_UpdateTranscriptHandlerTest {
  private static Account testAccount {
    get {
      if(testAccount == null) {
        testAccount = [SELECT Id FROM Account LIMIT 1];
      }
      return testAccount;
    }
    set;
  }
  private static Contact testContact {
    get {
      if(testContact == null) {
        testContact = [SELECT Id FROM Contact LIMIT 1];
      }
      return testContact;
    }
    set;
  }

  @testSetup
  private static void setup() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();
  }

  @isTest
  private static void updateInteractionSegmentTranscript()  {
    Test.startTest();
    Task smsTask = createTask();
    update smsTask;
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction__c].size());
    System.assertEquals(1, [SELECT Id FROM ASP_Interaction_Segment__c].size());
    System.assertEquals(2, [SELECT Id FROM ASP_Related_Objects__c].size());
  }

  @isTest
  private static void notSupportedTriggers() {
    Task smsTask = createTask();
    TriggerOperation operation = TriggerOperation.BEFORE_INSERT;
    AU_TriggerEntry.Args args = createUpdateArgs(new List<Task> {smsTask}, operation);

    Test.startTest();
    SC_UpdateTranscriptOnTaskUpdateHandler handler = new SC_UpdateTranscriptOnTaskUpdateHandler();
    handler.invokeMain(args, 1);
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void invokeWhileInProgressAndCompleted() {
    AU_TriggerEntry.Args args;
    SC_UpdateTranscriptOnTaskUpdateHandler handler = new SC_UpdateTranscriptOnTaskUpdateHandler();

    handler.invokeWhileInProgress(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
    handler.invokeCompleted(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void bulkTest()  {
    Test.startTest();
    List<Task> tasks = new List<Task> {
        new Task(
            WhatId = testAccount.Id,
            WhoId = testContact.Id,
            Subject = 'SMS',
            CallObject = '1234-5678'
        ),
        new Task(
            WhatId = testAccount.Id,
            WhoId = testContact.Id,
            Subject = 'SMS',
            CallObject = '1234-5687'
        ),
        new Task(
          WhatId = testAccount.Id,
          WhoId = testContact.Id,
          Subject = 'SMS',
          CallObject = '1234-5699'
        )
    };
    insert tasks;
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
    System.assertEquals(3, [SELECT count() FROM ASP_Interaction__c]);
    System.assertEquals(3, [SELECT count() FROM ASP_Interaction_Segment__c]);
    System.assertEquals(3, [SELECT count() FROM ASP_Related_Objects__c WHERE Object_Type__c = 'Contact']);
    System.assertEquals(3, [SELECT count() FROM ASP_Related_Objects__c WHERE Object_Type__c = 'Account']);
  }

  // Helper methods

  private static AU_TriggerEntry.Args createUpdateArgs(List<Task> items, TriggerOperation operation) {
    return new AU_TriggerEntry.Args(
        Task.SObjectType,
        operation,
        false,
        items,
        items,
        new Map<Id, SObject>(items),
        new Map<Id, SObject>(items)
    );
  }

  private static Task createTask()  {
    String subject = 'SMS';

    Task smsTask = new Task(
        Subject = subject,
        CallObject = '1234-5678',
        Description = 'test',
        WhatId = testAccount.Id,
        WhoId = testContact.Id
    );
    insert smsTask;
    return smsTask;
  }
}