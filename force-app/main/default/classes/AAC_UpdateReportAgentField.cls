/** Copyright 2017 Aria Solutions Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

public class AAC_UpdateReportAgentField {

  public static void linkToSalesforceUser(List<ACT_HistoricalReportData__c> records) {

    Map<String, Id> connectUsers = getConnectUsers();

    for (ACT_HistoricalReportData__c rec : records) {
      if (rec.Type__c == 'Agent' && connectUsers.containsKey(rec.AC_Object_Name__c)) {
        rec.Agent__c = connectUsers.get(rec.AC_Object_Name__c);
      }
    }
  }

  private static Map<String, Id> getConnectUsers() {
    List<User> connectUsers = [SELECT Amazon_Connect_Username__c FROM User WHERE Amazon_Connect_Username__c <> ''];

    Map<String, Id> result = new Map<String, Id>();
    for (User u : connectUsers) {
      result.put(u.Amazon_Connect_Username__c, u.Id);
    }
    return result;
  }
}