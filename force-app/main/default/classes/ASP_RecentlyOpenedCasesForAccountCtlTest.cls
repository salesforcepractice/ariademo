/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
private class ASP_RecentlyOpenedCasesForAccountCtlTest {

  /*@isTest static void testGetOpenCasesForCaseAccount_NoAccountAssigned() {
    Case c = ATU_Case.build('A Case').persist();

    List<Case> result = ASP_RecentlyOpenedCasesForAccountCtlr.getOpenCasesForCaseAccount(c.Id);
    System.assertEquals(null, result.AccountId);
    System.assertEquals(null, result.Count);

    System.assertEquals(null, ASP_RecentlyOpenedCasesForAccountCtlr.getNumberOfOpenCasesForCaseAccount(c.Id));
  }*/

  @isTest static void testGetOpenCasesForCaseAccount_AllCasesOlderThan7Days() {
    AccountAndCase accountAndCase = insertAccountWithCase('Test Account', 'Case Subject', getTodayAsDateTime().addDays(-8));

    List<Case> result = ASP_RecentlyOpenedCasesForAccountCtlr.getOpenCasesForCaseAccount(accountAndCase.AssociatedCase.Id);
    System.assertEquals(0, result.size());

    System.assertEquals(0, ASP_RecentlyOpenedCasesForAccountCtlr.getNumberOfOpenCasesForCaseAccount(accountAndCase.AssociatedCase.Id));
  }

  @isTest static void testGetOpenCasesForCaseAccount_OnlyCurrentCaseExists() {
    AccountAndCase accountAndCase = insertAccountWithCase('Test Account', 'Case Subject', getTodayAsDateTime().addDays(-7));

    List<Case> result = ASP_RecentlyOpenedCasesForAccountCtlr.getOpenCasesForCaseAccount(accountAndCase.AssociatedCase.Id);
    System.assertEquals(0, result.size());
    
    System.assertEquals(0, ASP_RecentlyOpenedCasesForAccountCtlr.getNumberOfOpenCasesForCaseAccount(accountAndCase.AssociatedCase.Id));
  }

  @isTest static void testGetOpenCasesForCaseAccount_CaseRecentlyOpened() {
    AccountAndCase accountAndCase = insertAccountWithCase('Test Account', 'Case Subject 1', getTodayAsDateTime().addDays(-7));

    Case c = ATU_Case.build('Case Subject 2', accountAndCase.Account.Id).persist();

    Test.setCreatedDate(c.Id, getTodayAsDateTime().addDays(-6));

    List<Case> result = ASP_RecentlyOpenedCasesForAccountCtlr.getOpenCasesForCaseAccount(accountAndCase.AssociatedCase.Id);
    System.assertEquals(accountAndCase.Account.Id, result[0].AccountId);
    System.assertEquals(1, result.size());

    System.assertEquals(1, ASP_RecentlyOpenedCasesForAccountCtlr.getNumberOfOpenCasesForCaseAccount(accountAndCase.AssociatedCase.Id));
  }

  private static AccountAndCase insertAccountWithCase(String accountName, String caseSubject, DateTime caseCreationDate) {
    Account a = ATU_Account.createAccount(accountName);
    insert a;

    Case c = ATU_Case.build(caseSubject, a.Id).persist();

    Test.setCreatedDate(c.Id, caseCreationDate);

    return new AccountAndCase(a, c);
  }

  private static DateTime getTodayAsDateTime() {
    return DateTime.newInstance(Date.today(), Time.newInstance(0,0,0,0));
  }

  private class AccountAndCase {
    public Account Account { get; set; }
    public Case AssociatedCase { get; set; }

    public AccountAndCase(Account a, Case c) {
      Account = a;
      AssociatedCase = c;
    }
  }
}