/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 25-Oct-19. 
 */

public class ASP_AssignUserGroupsController {

  public List<SelectOption> availableUserGroups { get; set; }
  public List<SelectOption> selectedUserGroups { get; set; }
  public String objectName { get; set; }
  public String picklistName { get; set; }
  private static final String CLASS_NAME = 'ASP_AssignUserGroupsController';

  public ASP_AssignUserGroupsController() {
    getUserGroupSelectedOptions();
  }

  public void saveGroups() {
    updatePicklistField(objectName, picklistName, selectedUserGroups);
  }

  private void getUserGroupSelectedOptions() {
    availableUserGroups = new List<SelectOption>();
    selectedUserGroups = new List<SelectOption>();
    List<SelectOption> userGroups = new List<SelectOption>();
    List<String> userGroupList = new List<String>();
    List<String> selectedUserGroupList = new List<String>();
    Map<String, String> objectDetails = buildMapOfMarqueeUserGroup();

    if (objectDetails.size() > 0) {
      for (String id : objectDetails.keySet()) {
        objectName = id;
        picklistName = objectDetails.get(id);
        break;
      }
    }

    for (Group u : [SELECT Id, Name FROM Group WHERE Type = 'Regular']) {
      userGroups.add(new SelectOption(u.Id, u.Name));
      userGroupList.add(u.Name);
    }
    SobjectType objType = Schema.getGlobalDescribe().get(objectName);
    Map<String, Schema.SObjectField> objFields = objType.getDescribe().fields.getMap();
    SObjectField fieldName = objFields.get(picklistName);
    Schema.DescribeFieldResult fieldResult = fieldName.getDescribe();
    fieldResult = fieldResult.getSObjectField().getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    for (Schema.PicklistEntry p : ple) {
      selectedUserGroups.add(new SelectOption(p.getLabel(), p.getValue()));
      selectedUserGroupList.add(p.getValue());
    }

    for (String s : userGroupList) {
      if (!selectedUserGroupList.contains(s)) {
        availableUserGroups.add(new SelectOption(s, s));
      }
    }
  }

  private void updatePicklistField(String objectName, String picklistName, List<SelectOption> newPicklistValues) {

    MetadataService.MetadataPort service = new MetadataService.MetadataPort();
    service.SessionHeader = new MetadataService.SessionHeader_element();
    service.SessionHeader.sessionId = UserInfo.getSessionId();

    MetadataService.CustomField customField = new MetadataService.CustomField();

    // Test method doesn't support web service callout
    if (!Test.isRunningTest()) {
      customField = (MetadataService.CustomField) service.readMetadata('CustomField', new String[]{
          objectName + '.' + picklistName
      }).getRecords()[0];
    }
    MetadataService.ValueSet picklistValueSet = new MetadataService.ValueSet();
    MetadataService.ValueSetValuesDefinition valueDefinition = new MetadataService.ValueSetValuesDefinition();
    List<MetadataService.CustomValue> newValues = new List<MetadataService.CustomValue>();
    for (SelectOption newValue : newPicklistValues) {
      MetadataService.CustomValue picklistValue = new MetadataService.CustomValue();
      picklistValue.fullName = newValue.getLabel();
      picklistValue.label = newValue.getLabel();
      picklistValue.default_x = false;
      picklistValue.isActive = true;
      newValues.add(picklistValue);
    }

    valueDefinition.value = newValues;
    valueDefinition.sorted = false;

    picklistValueSet.valueSetDefinition = valueDefinition;
    customField.valueSet = picklistValueSet;

    if (!Test.isRunningTest()) {
      List<MetadataService.UpsertResult> results =
          service.upsertMetadata(new MetadataService.Metadata[]{
              customField
          });

      for (MetadataService.UpsertResult objResult : results) {
        if (objResult.success) {
          AU_Debugger.debug(CLASS_NAME + ': Successfully updated');
        } else {
          if (objResult.errors.size() > 0) {
            AU_Debugger.debug(CLASS_NAME + ': Upsert error : ' + objResult.errors[0].message);
          }
        }
      }
    }
  }

  private static Map<String, String> buildMapOfMarqueeUserGroup() {
    Map<String, String> marqueeUserGroup = new Map<String, String>();
    for (Marquee_User_Group__mdt setting : [SELECT Object_Name__c, Picklist_Name__c FROM Marquee_User_Group__mdt]) {
      marqueeUserGroup.put(setting.Object_Name__c, setting.Picklist_Name__c);
    }
    return marqueeUserGroup;
  }
}