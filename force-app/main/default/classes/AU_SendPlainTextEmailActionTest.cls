/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 30-Aug-18. 
 */

@isTest
public class AU_SendPlainTextEmailActionTest {

  private static Account testAccount { get; set; }
  private static Contact testContact { get; set; }
  private static Case testCase { get; set; }

  @isTest static void sendTest_Success() {
    System.assertEquals(0, Limits.getEmailInvocations());

    Test.startTest();
    AU_SendPlainTextEmailAction.send(createArgs(true));
    System.assertEquals(1, Limits.getEmailInvocations());
    Test.stopTest();
  }
  
  private static List<AU_SendPlainTextEmailAction.EmailArgs> createArgs(Boolean isRecipientExists) {
    createContact();
    createCase();

    String emailAddressName = [SELECT DisplayName FROM OrgWideEmailAddress LIMIT 1].DisplayName;

    AU_SendPlainTextEmailAction.EmailArgs args = new AU_SendPlainTextEmailAction.EmailArgs();

    args.fromAddressDisplayName = emailAddressName;
    args.recipientId = (isRecipientExists) ? testContact.Id : null;
    args.emailSubject = 'Test Subject';
    args.emailBody = 'Email body as plain text';
    args.WhatId = testAccount.Id;
    args.replyToAddress = 'support@test.com';

    return new List<AU_SendPlainTextEmailAction.EmailArgs>{
        args
    };
  }

  private static void createContact() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();
  }

  private static void createCase()  {
    testCase = ATU_CaseFactory.createDefaultBuilder('testCase')
        .withField('ContactId', testContact.Id)
        .persist();
  }
}