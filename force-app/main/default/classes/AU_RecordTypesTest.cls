/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

@isTest
private class AU_RecordTypesTest {

  @IsTest
  static void testGetAll_InvalidObject() {
    try {
      AU_RecordTypes.getAll('NotAnObject');
      System.assert(false, 'AU_RecordTypes.SObjectNotFoundException should have been thrown.');
    } catch (AU_RecordTypes.SObjectNotFoundException e) {
      System.assert(true);
    }
  }

  @IsTest
  static void testGetAll_InvalidRecordType() {
    try {
      AU_RecordTypes.get('Account', 'NotARecordType');
      System.assert(false, 'AU_RecordTypes.RecordTypeNotFoundException should have been thrown.');
    } catch (AU_RecordTypes.RecordTypeNotFoundException e) {
      System.assert(true);
    }
  }

  @isTest
  static void testGet() {
    List<RecordType> recordTypes = [SELECT Id, DeveloperName, SObjectType, Name FROM RecordType LIMIT 1];
    if (recordTypes.size() > 0) {
      RecordType existingRecordType = recordTypes[0];

      System.assertEquals(recordTypes[0].Id, AU_RecordTypes.getId(existingRecordType.SobjectType, recordTypes[0].DeveloperName));
      System.assertEquals(recordTypes[0].Id, AU_RecordTypes.get(existingRecordType.SobjectType, existingRecordType.DeveloperName).getRecordTypeId());
    }
    else {
      try {
        AU_RecordTypes.getId('Account', 'TestDeveloperName');
        System.assert(false, 'AU_RecordTypes.RecordTypeNotFoundException should have been thrown.');
      } catch (AU_RecordTypes.RecordTypeNotFoundException e) {
        System.debug('error: ' + e);
        System.assertEquals('AU_RecordTypes.get(String, String): RecordTypeInfo :TestDeveloperName is not a valid Record Type API name for sObject:Account', e.getMessage());
      }
    }
  }
}