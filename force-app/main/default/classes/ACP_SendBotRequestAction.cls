/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

global class ACP_SendBotRequestAction {

  private static final String API_KEY = 'vyfW6KJR1s9EGQMLYPzBM6ZLvDuQLoXx5GZpIkjg'; //FIXME make API key configurable

  private static final String CLASS_NAME = 'ACP_SendBotRequestAction.';

  @InvocableMethod(label='Send ACP Bot Request')
  global static List<String> sendBotRequest(List<RerquestArgs> args) {

    List<String> result = new List<String>();

    for (RerquestArgs arg : args) {
      ACP_BotRequestBuilder req = new ACP_BotRequestBuilder(arg.serviceUrl, API_KEY)
        .usingChannel(arg.channel)
        .withMessage(arg.message);

      addSessionKvpsIfAvailable(req, arg.sessionKvps);
      addSessionKvpsIfAvailable(req, (AU_Check.isNull(arg.serializedSessionKvps) ? null : arg.serializedSessionKvps.split(';')));

      System.enqueueJob(new ASP_RequestExecutionQueueable(req));

      result.add('SomeRequestGuid'); // FIXME: Create Request GUID and handle it in the RequestExecutionQueuable => Async HTTP Request handling
    }

    return result;
  }

  private static void addSessionKvpsIfAvailable(ACP_BotRequestBuilder req, List<String> sessionKvps) {
    if (AU_Check.isNull(sessionKvps)) {
      return;
    }

    AU_Debugger.debug('Session KVPs: ' + JSON.serialize(sessionKvps));
    for (String kvpAsString : sessionKvps) {
      String[] kvp = kvpAsString.split(':');

      req.withSessionEntry(kvp[0], kvp[1]);
    }
  }

  global class RerquestArgs {

    @InvocableVariable(label='Service URL' required=true)
    global String serviceUrl;

    @InvocableVariable(label='Channel' required=true)
    global String channel;

    @InvocableVariable(label='Message' required=true)
    global String message;

    @InvocableVariable(label='Session KVPs' required=false)
    global List<String> sessionKvps;

    @InvocableVariable(label='Serialized Session KVPs' required=false)
    global String serializedSessionKvps;

  }
}