/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest
private class AU_JsonImprovedConverterTest {
  @isTest static void testRoundtrip() {
    String origString = '{"otherReservedWord":"foo bar","currency":"123.45"}';
    AU_JsonImprovedConverterTest.MySerializerType obj = (AU_JsonImprovedConverterTest.MySerializerType)
      new AU_JsonImprovedConverterTest.MySerializer().deserialize(
        origString,
        AU_JsonImprovedConverterTest.MySerializerType.class
      );
    String newString = new AU_JsonImprovedConverterTest.MySerializer().serialize(obj);
    System.assertEquals(origString, newString);
  }

  public class MySerializer extends AU_JsonImprovedConverter {

    private MySerializer() {
      //setup mappings
      super(
        new Map<String,String>{
          'currencyType' => 'currency',
          'myReservedWordProp' => 'otherReservedWord'
        }
      );
      //turn off null serialization
      setSerializeNulls(false);
    }
  }

  public class MySerializerType{
    public String currencyType {get; set;}
    public String myReservedWordProp {get; set;}
  }
}