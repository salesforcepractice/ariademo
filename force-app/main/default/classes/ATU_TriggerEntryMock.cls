/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
public class ATU_TriggerEntryMock implements AU_TriggerEntry.ITriggerEntry {
  private static ATU_InvocationCounter counter = new ATU_InvocationCounter();

  public static Integer getMethodCount(String method){
    return counter.getMethodCount(method);
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount){
    counter.countMethod('invokeMain');
  }

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount){
    counter.countMethod('invokeWhileInProgress');
  }

  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount){
    counter.countMethod('invokeCompleted');
  }
}