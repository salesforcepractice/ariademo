/** *****************************************************************************
* Author kbowden
* Date 10 Jan 2016
********************************************************************************/
public class BB_LTG_BoardStage
{
  @AuraEnabled
  public String stageName {get; set;}

  @AuraEnabled
  public List<StageSObject> sobjects {get; set;}

  public BB_LTG_BoardStage()
  {
    sobjects=new List<StageSObject>();
  }

  public class StageSObject
  {
    @AuraEnabled
    public String id { get; set; }

	  @AuraEnabled
	  public String warningIconName {get;set;}

	  @AuraEnabled
	  public String warningIconVariant {get;set;}

	  @AuraEnabled
	  public String secondIconName {get;set;}

	  @AuraEnabled
	  public String secondIconVariant {get;set;}

	  @AuraEnabled
	  public String thirdIconName {get;set;}

	  @AuraEnabled
	  public String thirdIconVariant {get;set;}

    @AuraEnabled
    public StageSObjectField titleField {get; set;}

    @AuraEnabled
    public List<StageSObjectField> fields {get; set;}

    @AuraEnabled
    public String contactJSON {get; set;}

    public StageSObject()
    {
      fields=new List<StageSObjectField>();
    }
  }

  public class StageSObjectField
  {
    @AuraEnabled
    public String fieldName {get; set;}

    @AuraEnabled
    public String abbreviatedFieldName {get; set;}

    @AuraEnabled
    public String rawFieldValue {get; set;}

    @AuraEnabled
    public String adjustedFieldValue {get; set;}

    public StageSObjectField(Schema.DescribeFieldResult describeFieldResult, Object fValue)
    {
      fieldName = describeFieldResult.getLabel();
      rawFieldValue = getRawFieldValue(describeFieldResult, fValue);
      adjustedFieldValue = getAdjustedFieldValue(describeFieldResult, fValue);

      String abbrName = '';
      for (String s : fieldName.split(' ')) {
        abbrName += s.left(1).toUpperCase();
      }
      abbreviatedFieldName = abbrName;
    }

    private String getRawFieldValue(Schema.DescribeFieldResult describeFieldResult, Object fValue) {
      if (fValue == null) {
        return String.valueOf(fValue);
      }

      if (describeFieldResult.getType() == Schema.DisplayType.DATETIME) {
        return ((Datetime) fValue).format();
      }

      return String.valueOf(fValue);
    }

    private String getAdjustedFieldValue(Schema.DescribeFieldResult describeFieldResult, Object fValue) {
      if (fValue == null) {
        return 'N/A';
      }

      if (describeFieldResult.getName() == 'LastViewedDate') {
        Long millisecondsBetween = Datetime.now().getTime() - ((Datetime) fValue).getTime();
        Long hoursBetween = millisecondsBetween / 3600000;
        return (hoursBetween < 24 ? hoursBetween + ' h' : (hoursBetween / 24) + ' d') + ' ago';
      }

      if (describeFieldResult.getType() == Schema.DisplayType.DATETIME) {
        return ((Datetime) fValue).format();
      }

      if (describeFieldResult.getType() == Schema.DisplayType.CURRENCY) {
        return '$ ' + (fValue != null ? String.valueOf(fValue) : '0');
      }

      return String.valueOf(fValue);
    }
  }
}