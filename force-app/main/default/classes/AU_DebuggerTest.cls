/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest private class AU_DebuggerTest {
  
  @isTest static void testGetCurrentLogStatment_NoStatements() {
  	System.assertEquals(null, AU_Debugger.getCurrentLogStatement());
  }
  
  @isTest static void testGetCurrentLogStatment_WithStatements() {
  	
  	AU_Debugger.enterFunction('funcA');
  	AU_Debugger.enterFunction('funcB');
  	AU_Debugger.debug('some debug info');
  	
  	System.assertEquals('   Invoked: funcA\n      Invoked: funcB\n      some debug info\n', AU_Debugger.getCurrentLogStatement());
  	
  	AU_Debugger.leaveFunction();
  	AU_Debugger.leaveFunction();
  	AU_Debugger.debug('more info');
  	
  	System.assertEquals('   Invoked: funcA\n      Invoked: funcB\n      some debug info\nmore info\n', AU_Debugger.getCurrentLogStatement());
  }
  
  @isTest static void testGetCurrentLogStatment_LevelsExceedSpaces() {
    
    AU_Debugger.enterFunction('funcA');
    AU_Debugger.enterFunction('funcB');
    AU_Debugger.enterFunction('funcC');
    AU_Debugger.enterFunction('funcD');
    AU_Debugger.enterFunction('funcE');
    AU_Debugger.enterFunction('funcF');
    AU_Debugger.enterFunction('funcG');
    AU_Debugger.enterFunction('funcH');
    AU_Debugger.enterFunction('funcI');
    AU_Debugger.enterFunction('funcJ');
    AU_Debugger.enterFunction('funcK');
    AU_Debugger.enterFunction('funcL');
    AU_Debugger.enterFunction('funcM');
    AU_Debugger.enterFunction('funcN');
    
    System.assertEquals('>>>Invoked:>funcA\n>>>>>>Invoked:>funcB\n>>>>>>>>>Invoked:>funcC\n>>>>>>>>>>>>Invoked:>funcD\n>>>>>>>>>>>>>>>Invoked:>funcE\n>>>>>>>>>>>>>>>>>>Invoked:>funcF\n>>>>>>>>>>>>>>>>>>>>>Invoked:>funcG\n>>>>>>>>>>>>>>>>>>>>>>>>Invoked:>funcH\n>>>>>>>>>>>>>>>>>>>>>>>>>>>Invoked:>funcI\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Invoked:>funcJ\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Invoked:>funcK\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Invoked:>funcL\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Invoked:>funcM\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Invoked:>funcN\n', AU_Debugger.getCurrentLogStatement().replace(' ', '>'));
  }
  
  @isTest static void testResetAndPrint() {
  	AU_Debugger.enterFunction('funcA');
    AU_Debugger.enterFunction('funcB');
    AU_Debugger.debug('some debug info');
    
    System.assertEquals('   Invoked: funcA\n      Invoked: funcB\n      some debug info\n', AU_Debugger.getCurrentLogStatement());
  	
  	AU_Debugger.resetAndPrint();
  	
  	AU_Debugger.debug('more info');
    
    System.assertEquals('   Invoked: funcA\n      Invoked: funcB\n      some debug info\nmore info\n', AU_Debugger.getCurrentLogStatement());
  }
  
  @isTest static void testReportException() {
  	SomeException ex = new SomeException('my error message');

    Test.startTest();
  	AU_Debugger.reportException('AU_DebuggerTest', ex);
    TEst.stopTest();
  	
  	List<AU_DebugInfo__c> debugLogs = [SELECT OwnerId, Level__c, Component__c, Data__c FROM AU_DebugInfo__c];
    System.assertEquals(1, debugLogs.size());
    
    AU_DebugInfo__c errorEntry = debugLogs[0];
    System.assertEquals('Error', errorEntry.Level__c);
    System.assertEquals('AU_DebuggerTest', errorEntry.Component__c);
    
    System.assert(errorEntry.Data__c.contains('SomeException'));
    System.assert(errorEntry.Data__c.contains('my error message'));
  }

  @IsTest static void testDeserializeExtraInfo() {
    AU_DebugInfo__c info = new AU_DebugInfo__c(
      Component__c = 'Component',
      Level__c = 'Error',
      ExtraInfo__c = '{ "foo": "bar"}'
    );

    Map<String, Object> result = AU_Debugger.deserializeExtraInfo(info);

    System.assertEquals(1, result.size());
    System.assertEquals('bar', result.get('foo'));
  }

  @IsTest static void testReportSaveErrors() {
    Lead l1 = new Lead( LastName = 'Smith', Company = 'Foo');
    Lead l2 = new Lead( LastName = 'Foo');
    Lead l3 = new Lead( LastName = 'Bar');

    List<Database.SaveResult> result = Database.insert(new List<Lead> {l1, l2, l3}, false);

    Test.startTest();
    AU_Debugger.reportSaveErrors('MyComponent', result, null);
    Test.stopTest();

    List<AU_DebugInfo__c> errors = ATU_DebugInfoUtil.assertErrorsRecorded();

    System.assertEquals(1, errors.size());
  }
  
  public class SomeException extends Exception {}
}