/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 05-Jun-18. 
 */

@isTest
public class AU_EmailNotificationUtilTest {

  private static String jobName = 'testScheduledApex';
  private static String cronExpression = '0 0 0 3 9 ? 2099';

  @isTest static void testSendBatchJobDoneConfirmationSuccess()  {

    String email = 'test@emailtest.com';

    String jobId = System.schedule(jobName, cronExpression, new TestApexJob());
    List<CronJobDetail> scheduledJob = [SELECT Id, Name, JobType FROM CronJobDetail WHERE Name = :jobName];

    Test.startTest();
    System.assertEquals(0, Limits.getEmailInvocations());
    AU_EmailNotificationUtil.sendBatchJobDoneConfirmation(scheduledJob[0], email, null);
    System.assertEquals(1, Limits.getEmailInvocations());
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest static void testSendBatchJobDoneConfirmationPartialSuccess()  {

    String email = 'test@emailtest.com;test';

    String jobId = System.schedule(jobName, cronExpression, new TestApexJob());
    List<CronJobDetail> scheduledJob = [SELECT Id, Name, JobType FROM CronJobDetail WHERE Name = :jobName];

    Test.startTest();
    System.assertEquals(0, Limits.getEmailInvocations());
    AU_EmailNotificationUtil.sendBatchJobDoneConfirmation(scheduledJob[0], email, null);
    System.assertEquals(1, Limits.getEmailInvocations());
    Test.stopTest();

    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest static void testSendBatchJobDoneConfirmationUnsuccessful()  {

    String jobId = System.schedule(jobName, cronExpression, new TestApexJob());
    List<CronJobDetail> scheduledJob = [SELECT Id, Name, JobType FROM CronJobDetail WHERE Name = :jobName];

    Test.startTest();
    System.assertEquals(0, Limits.getEmailInvocations());
    AU_EmailNotificationUtil.sendBatchJobDoneConfirmation(scheduledJob[0], null, null);
    System.assertEquals(0, Limits.getEmailInvocations());
    Test.stopTest();

    System.assertEquals(1, ATU_DebugInfoUtil.assertErrorsRecorded().size());
  }

  public class TestApexJob implements Schedulable {
    public void execute(SchedulableContext context) {}
  }
}