/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 08-Nov-18. 
 */

public class ASP_UpdateSegmentOnChatClosedHandler implements AU_TriggerEntry.ITriggerEntry {

  private static final String CLASS_NAME = 'ASP_UpdateSegmentOnChatClosedHandler';

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
  }
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount)  {
    AU_Debugger.enterFunction(CLASS_NAME + '.invokeMain:');
    AU_Debugger.debug('TriggerOperation: ' + args.operation);

    if (args.operation != TriggerOperation.AFTER_UPDATE) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<LiveChatTranscript> newTranscripts = (List<LiveChatTranscript>) args.newObjects;

    List<ASP_Interaction_Segment__c> segments =  [
        SELECT Id, Queue_Time__c, Transcription__c
        FROM ASP_Interaction_Segment__c
        WHERE ASP_Interaction__r.Interaction_Id__c IN :args.newObjectMap.keySet()
        ORDER BY CreatedDate
    ];

    for(LiveChatTranscript transcript : newTranscripts) {
      if(transcript.EndTime != null && !segments.isEmpty())  {
        segments[0].Queue_Time__c = transcript.WaitTime;
      }
    }

    update segments;

    AU_Debugger.leaveFunction();
  }

}