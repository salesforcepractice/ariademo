/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday March 27th 2020
 * Author: Vacheslav Aronov
 * File type: '.cls'
 */

public class SC_CallRecordingSegment  {
  private ApexPages.StandardController controller;
  public ASP_Interaction_Segment__c segment {get;set;}

  public SC_CallRecordingSegment(ApexPages.StandardController controllerParam){
    controller = controllerParam;
    segment = (ASP_Interaction_Segment__c) controller.getRecord();
  }
  public void RetrieveRecording(){
    System.debug('SC_CallRecordingSegment: Retrieve record');
    System.debug('SC_CallRecordingSegment: segment: ' + segment);
    List<ASP_Interaction_Segment__c> segmentDetails = [SELECT Id, Interaction_Segment_Id__c FROM ASP_Interaction_Segment__c WHERE Id = :this.segment.Id];
    List<Task> relatedTasks = new List<Task>();
    for (ASP_Interaction_Segment__c s : segmentDetails) {
      if (String.valueOf(s.Interaction_Segment_Id__c).startsWith('00T'))  {
        relatedTasks.add(new Task(Id = s.Interaction_Segment_Id__c));
      }
    }
    update relatedTasks;
  }
}