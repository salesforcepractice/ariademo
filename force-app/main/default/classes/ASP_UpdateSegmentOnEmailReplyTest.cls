/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 10/29/2018.
 */

@isTest
private class ASP_UpdateSegmentOnEmailReplyTest {
  @testSetup
  private static void setup() {
    Case c = ATU_Case.build('Test Email Case')
                     .withField('Origin', 'Email')
                     .persist();
    EmailMessage em = new EmailMessage(
      ParentId = c.Id,
      Incoming = true
    );
    insert em;
    ASP_Interaction__c interaction = new ASP_Interaction__c(
      Interaction_Id__c = em.Id
    );
    insert interaction;
    ASP_Interaction_Segment__c segment = new ASP_Interaction_Segment__c(
      ASP_Interaction__c = interaction.Id,
      Agent_Identifier_Name__c = UserInfo.getUserId(),
      EmailMessage_Id__c = em.Id,
      Interaction_Segment_Id__c = em.Id + '-' + UserInfo.getUserId()
    );
    insert segment;
  }

  @isTest
  private static void testReply() {
    

    Case c = [SELECT Id from Case];
    EmailMessage originalEmail = [SELECT Id from EmailMessage];

    List<EmailMessage> replies = new List<EmailMessage> {
      new EmailMessage(
        Incoming = false,
        ParentId = c.Id,
        ReplyToEmailMessageId = originalEmail.Id
      ),
      new EmailMessage(
        Incoming = false,
        ParentId = c.Id
      )
    };
    insert replies;

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      EmailMessage.SObjectType,
      TriggerOperation.AFTER_INSERT,
      true,
      replies,
      null,
      null,
      null
    );
    ASP_UpdateSegmentOnEmailReply handler = new ASP_UpdateSegmentOnEmailReply();
    handler.invokeMain(args, 1);

    List<ASP_Interaction_Segment__c> segments = [SELECT Reply_Date__c from ASP_Interaction_Segment__c];
    System.assertEquals(1, segments.size());
    System.assertNotEquals(null, segments[0].Reply_Date__c);
    ATU_DebugInfoUtil.assertNoErrors();
  }

  @isTest
  private static void testUnsupportedEvents() {
    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      EmailMessage.SObjectType,
      TriggerOperation.BEFORE_DELETE,
      true,
      null,
      null,
      null,
      null
    );
    ASP_UpdateSegmentOnEmailReply handler = new ASP_UpdateSegmentOnEmailReply();
    handler.invokeMain(args, 1);
    handler.invokeWhileInProgress(args, 1);
    handler.invokeCompleted(args, 1);
    ATU_DebugInfoUtil.assertNoErrors();
  }
}