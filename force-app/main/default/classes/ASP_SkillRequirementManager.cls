/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Manager class for creating skill requirements on omni routing items.
 * Created by jliu on 6/11/2018.
 */

public without sharing class ASP_SkillRequirementManager {
  private Map<SObjectType, List<ASP_SkillRequirementResolver>> resolversBySObjectType;
  private Map<SObjectType, Set<String>> requiredFieldsBySObjectType;

  public ASP_SkillRequirementManager() {
    resolversBySObjectType = new Map<SObjectType, List<ASP_SkillRequirementResolver>>();
    requiredFieldsBySObjectType = new Map<SObjectType, Set<String>>();
  }

  public ASP_SkillRequirementManager(Map<SObjectType, List<ASP_SkillRequirementResolver>> resolversBySObjectType) {
    this();

    for (SObjectType type : resolversBySObjectType.keySet()) {
      List<ASP_SkillRequirementResolver> resolvers = resolversBySObjectType.get(type);
      for (ASP_SkillRequirementResolver resolver : resolvers) {
        if (resolver.getSObjectType() == type)
          addResolver(resolver);
      }
    }
  }

  private void addResolver(ASP_SkillRequirementResolver resolver) {
    AU_Debugger.enterFunction('ASP_SkillRequirementManager.addResolver');

    SObjectType type = resolver.getSObjectType();

    List<ASP_SkillRequirementResolver> resolvers = resolversBySObjectType.get(type);
    if (resolvers == null) {
      resolvers = new List<ASP_SkillRequirementResolver>();
      resolversBySObjectType.put(type, resolvers);
    }

    Set<String> fields = requiredFieldsBySObjectType.get(type);
    if (fields == null) {
      fields = new Set<String>();
      fields.add('Id');
      requiredFieldsBySObjectType.put(type, fields);
    }

    resolvers.add(resolver);
    fields.addAll(resolver.getRequiredFields());

    AU_Debugger.leaveFunction();
  }

  public List<SkillRequirement> createSkillRequirements(List<PendingServiceRouting> routingItems) {
    AU_Debugger.enterFunction('ASP_SkillRequirementManager.createSkillRequirements');

    Map<Id, PendingServiceRouting> routingItemsByWorkItemId = new Map<Id, PendingServiceRouting>();
    Map<SObjectType, Set<Id>> workItemIdsBySObjectType = new Map<SObjectType, Set<Id>>();

    for (PendingServiceRouting routingItem : routingItems) {
      Id workItemId = routingItem.WorkItemId;
      if (workItemId == null)
        continue;

      SObjectType type = workItemId.getSobjectType();

      Set<Id> workItemIds = workItemIdsBySObjectType.get(type);
      if (workItemIds == null) {
        workItemIds = new Set<Id>();
        workItemIdsBySObjectType.put(type, workItemIds);
      }

      routingItemsByWorkItemId.put(workItemId, routingItem);
      workItemIds.add(workItemId);
    }

    List<SkillRequirement> skillRequirements = new List<SkillRequirement>();

    for (SObjectType type : workItemIdsBySObjectType.keySet()) {
      Set<Id> workItemIds = workItemIdsBySObjectType.get(type);
      List<SObject> workItems = queryWorkItems(type, workItemIds);
      List<ASP_SkillRequirementResolver> resolvers = resolversBySObjectType.get(type);

      for (SObject workItem : workItems) {
        PendingServiceRouting routingItem = routingItemsByWorkItemId.get(workItem.Id);

        for (ASP_SkillRequirementResolver resolver : resolvers) {
          skillRequirements.add(resolver.createSkillRequirement(workItem, routingItem.Id));
        }
      }
    }

    AU_Debugger.leaveFunction();
    return skillRequirements;
  }

  private List<SObject> queryWorkItems(SObjectType type, Set<Id> workItemIds) {
    AU_Debugger.enterFunction('ASP_SkillBasedRoutingAction.queryWorkItems');

    List<String> requiredFields = new List<String>(requiredFieldsBySObjectType.get(type));
    String query = 'select ' + String.join(requiredFields, ',');
    query += ' from ' + type.getDescribe().getName();
    query += ' where Id in :workItemIds';

    AU_Debugger.debug('Query string: ' + query);

    AU_Debugger.leaveFunction();
    return Database.query(query);
  }
}