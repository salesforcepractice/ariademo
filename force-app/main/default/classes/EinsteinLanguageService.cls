/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

public class EinsteinLanguageService {

  private static final String CLASS_NAME = 'EinsteinLanguageService';

  private final TokenProvider tokenProvider;

  public EinsteinLanguageService(TokenProvider tokenProvider) {
    this.tokenProvider = tokenProvider;
  }

  public SentimentResult getCommunitySentiment(String text) {
    AU_Debugger.enterFunction(CLASS_NAME + '.getCommunitySentiment');
    try {
      HttpRequest req = buildRequest()
        .withToken(tokenProvider.getToken())
        .withParam('modelId', 'CommunitySentiment')
        .withParam('document', text)
        .create();

      AU_Debugger.debug('Authorization: ' + req.getHeader('Authorization'));
      AU_Debugger.debug('Request: ' + req.getBody());

      Http http = new Http();
      HttpResponse respone = http.send(req);

      String sentimentResultJson = respone.getBody();
      AU_Debugger.debug('Sentiment response: ' + sentimentResultJson);

      return (SentimentResult) JSON.deserialize(sentimentResultJson, SentimentResult.class);
    } catch (Exception ex) {
      throw new EinsteinLanguageIntendException(ex);
    } finally {
      AU_Debugger.leaveFunction();
    }
  }

  private static EinsteinLanguageService.EinsteinRequestBuilder buildRequest() {
    return new EinsteinLanguageService.EinsteinRequestBuilder('https://api.einstein.ai/v2/language/sentiment');
  }

  public class SentimentResult {
    public List<Sentiment> probabilities;
  }

  public class Sentiment {
    public String label;
    public Decimal probability;
  }

  public class EinsteinRequestBuilder {

    private final HttpRequest request = new HttpRequest();

    private final Map<String, String> params = new Map<String, String>();

    private EinsteinRequestBuilder(String endpoint) {
      request.setEndpoint(endpoint);
    }

    public EinsteinLanguageService.EinsteinRequestBuilder withToken(String token) {
      request.setHeader('Authorization', 'Bearer ' + token);
      return this;
    }

    public EinsteinLanguageService.EinsteinRequestBuilder withParam(String key, String val) {
      params.put(key, val);
      return this;
    }

    public HttpRequest create() {
      request.setMethod('POST');
      request.setHeader('Cache-Control', 'no-cache');
      request.setHeader('Content-Type', HttpFormDataBodyPart.GetContentType());

      String bodyString = '';
      for (String key : params.keySet()) {
        bodyString += HttpFormDataBodyPart.WriteBoundary();
        bodyString += HttpFormDataBodyPart.WriteBodyParameter(key, params.get(key));
      }

      bodyString += HttpFormDataBodyPart.WriteBoundary(HttpFormDataBodyPart.EndingType.CrLf);
      Blob bodyBlob = EncodingUtil.base64Decode(bodyString);

      request.setBodyAsBlob(bodyBlob);

      return request;
    }
  }

  public class EinsteinLanguageIntendException extends Exception { }
}