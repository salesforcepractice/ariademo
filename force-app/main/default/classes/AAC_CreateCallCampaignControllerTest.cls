/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 *
 * Created by jliu on 7/25/2018.
 */

@isTest
private class AAC_CreateCallCampaignControllerTest {
  @testSetup
  static void setup() {
    Account acc = ATU_Account.build('Test Account').persist();
    Contact con = ATU_Contact.build('Test Contact', acc.Id).persist();
  }

  @isTest
  static void testGetUserContact() {
    // not testing the positive case as the org may not have communities set up
    Contact c = AAC_CreateCallCampaignController.getUserContact();
    system.assertEquals(null, c); // community users can't run unit tests
  }

  @isTest
  static void testCreateCallCampaignWithContact() {
    Contact c = [select Id from Contact];
    AAC_CreateCallCampaignController.createCallCampaignWithContact(c.Id, '6041111111', 'Test comment');
    AAC_Call_Campaign__c cc = [select Contact__c, Phone_Number__c, Comments__c from AAC_Call_Campaign__c];

    system.assertEquals(c.Id, cc.Contact__c);
    system.assertEquals('6041111111', cc.Phone_Number__c);
    system.assertEquals('Test comment', cc.Comments__c);
  }

  @isTest
  static void testCreateCallCampaign() {
    AAC_CreateCallCampaignController.createCallCampaign('John', 'Smith', '6041111111', 'Test comment');
    AAC_Call_Campaign__c cc = [select Phone_Number__c, Comments__c from AAC_Call_Campaign__c];

    system.assertEquals('6041111111', cc.Phone_Number__c);
    system.assert(cc.Comments__c.contains('John'));
    system.assert(cc.Comments__c.contains('Smith'));
    system.assert(cc.Comments__c.contains('Test comment'));
  }

  @isTest
  static void testErrors() {
    // use an Account Id instead of a Contact Id
    AAC_CreateCallCampaignController.createCallCampaignWithContact('0016C00000AUb4h', null, null);
    ATU_DebugInfoUtil.assertErrorsRecorded();

    delete [select Id from AU_DebugInfo__c];

    // how about a random paragraph for a first name?
    AAC_CreateCallCampaignController.createCallCampaign(
      'The wish recovers after the collective! The pedantry mobs a phrase under the eligible mankind. A growing controversy stumbles inside an infinite consent. Does the touched thread stray beneath the alcoholic? A scrap romance oils the gulf on top of a hazy appearance.',
      null,
      null,
      null
    );
    ATU_DebugInfoUtil.assertErrorsRecorded();
  }
}