/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class AU_TriggerBusTest {

  @IsTest
  static void testDispatch() {
    SObjectType type = Account.getSObjectType();
    
    AU_TriggerBus.triggerHandlerSettingMocks = getTriggerHandlingSettingsForSObjectType('Account');
    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      type,
      TriggerOperation.BEFORE_INSERT,
      false,
      new List<Sobject>{},
      new List<Sobject>{},
      new Map<Id,Sobject>{},
      new Map<Id,Sobject>{}
    );

    AU_TriggerBus.dispatch(args);

    System.assertEquals(1, ATU_TriggerEntryMock.getMethodCount('invokeMain'), 'invokeMain calls');
    System.assertEquals(1, ATU_TriggerEntryMock.getMethodCount('invokeCompleted'), 'invokeCompleted calls');
  }

  private static List<TriggerSetting__mdt> getTriggerHandlingSettingsForSObjectType(String objTypeName){
		List<TriggerSetting__mdt> triggerHandlerSettings = new List<TriggerSetting__mdt>();
		
    triggerHandlerSettings.add(getTriggerSetting(objTypeName, System.TriggerOperation.BEFORE_INSERT, 'ATU_TriggerEntryMock'));

		return triggerHandlerSettings;
	}

	private static TriggerSetting__mdt getTriggerSetting(String objTypeName, System.TriggerOperation event, String className){
		JSONObject jsonObj = new JSONObject(new TriggerSetting__mdt());
		jsonObj.set('DeveloperName','TestHandler'+event.name());
		jsonObj.set('Event__c', event.name(), true);
		jsonObj.set('sObject__c', objTypeName, true);
		jsonObj.set('Class__c', className, true);
    TriggerSetting__mdt triggerHandlerSetting = (TriggerSetting__mdt) jsonObj.deserialize(TriggerSetting__mdt.class);

		return triggerHandlerSetting;
	}
}