/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 27-Mar-18.
 *
 */

public class ASP_MarqueeController {
  private static final String CLASS_NAME = 'ASP_MarqueeController';

  @AuraEnabled
  public static List<ASP_Marquee__c> getRecords() {
    String userGroup = isRunningOnCommunity() ? 'Community' : 'Internal';
    List<String> userGroups = new List<String>();
    List<String> publicGroups = getPublicGroups();

    List<ASP_Marquee__c> marquees = new List<ASP_Marquee__c>();
    for (ASP_Marquee__c marquee : [
          SELECT  Id, Name, Title__c, User_Group__c, Start_Date__c, End_Date__c, Message_Body__c, Active__c, Show_Posted_Date__c
          FROM    ASP_Marquee__c
          WHERE   Active__c = true AND
          Start_Date__c <= TODAY AND
          (End_Date__c = null OR End_Date__c >= TODAY)
          ORDER BY Start_Date__c DESC
          ]
        )
    {
      if (userGroup == 'Community') {
        marquee.User_Group__c.contains(userGroup);
        marquees.add(marquee);
      }
      else  {
        for (String s : marquee.User_Group__c.split(';'))  {
          if (publicGroups.contains(s.trim()))  {
            marquees.add(marquee);
            break;
          }
        }
      }
    }
    return marquees;
  }

  @AuraEnabled
  public static Boolean isRunningOnCommunity()  {
     return (Site.getSiteId() != null);
  }

  @AuraEnabled
  public static List<String> getPublicGroups()  {
    List<String> publicGroups = new List<String>();
    Id currentUser = UserInfo.getUserId();

    publicGroups.add('Internal');
    for(GroupMember  u :[SELECT Group.Name FROM GroupMember WHERE UserOrGroupId =:currentUser AND Group.Type = 'Regular'])
    {
      publicGroups.add(u.Group.Name);
    }

    return publicGroups;
  }
}