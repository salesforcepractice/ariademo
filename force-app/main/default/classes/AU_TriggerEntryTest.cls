/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
@isTest class AU_TriggerEntryTest {

  private static Case newCase = createCase('A new Case');
  private static Case oldCase = createCase('An old Case');

  private static List<Sobject> newObjects = new Sobject[] { newCase };
  private static List<Sobject> oldObjects = new Sobject[] { oldCase };
  private static Map<Id, Sobject> newObjectMap = new Map<Id, Sobject>{ newCase.Id => newCase };
  private static Map<Id, Sobject> oldObjectMap = new Map<Id, Sobject>{ oldCase.Id => oldCase };

  @isTest static void testTriggerEntryArgs_insert() {
    Test.startTest();

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.BEFORE_INSERT,
      true,
      newObjects,
      null,
      newObjectMap,
      null
    );
    System.assertEquals('AU_TriggerEntry.Args[Case, operationType=BEFORE_INSERT, isExecuting=true, numObjects=1]', args.toString());

    Test.stopTest();
  }

  @isTest static void testTriggerEntryArgs_update() {
    Test.startTest();

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.BEFORE_UPDATE,
      true,
      newObjects,
      oldObjects,
      newObjectMap,
      oldObjectMap
    );
    System.assertEquals('AU_TriggerEntry.Args[Case, operationType=BEFORE_UPDATE, isExecuting=true, numObjects=1]', args.toString());

    Test.stopTest();
  }

  @isTest static void testTriggerEntryArgs_delete() {
    Test.startTest();

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.BEFORE_DELETE,
      false,
      null,
      oldObjects,
      null,
      oldObjectMap
    );
    System.assertEquals('AU_TriggerEntry.Args[Case, operationType=BEFORE_DELETE, isExecuting=false, numObjects=1]', args.toString());

    Test.stopTest();
  }

  @isTest static void testTriggerEntryArgs_undelete() {
    Test.startTest();

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.AFTER_UNDELETE,
      false,
      newObjects,
      null,
      newObjectMap,
      null
    );
    System.assertEquals('AU_TriggerEntry.Args[Case, operationType=AFTER_UNDELETE, isExecuting=false, numObjects=1]', args.toString());

    Test.stopTest();
  }

  @isTest static void testTriggerEntryArgs_equals() {
    Test.startTest();

    AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.BEFORE_UPDATE,
      true,
      newObjects,
      oldObjects,
      newObjectMap,
      oldObjectMap
    );

    AU_TriggerEntry.Args args2 = new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.BEFORE_UPDATE,
      true,
      new List<SObject>(newObjects),
      new List<SObject>(oldObjects),
      new Map<Id, Sobject>(newObjectMap),
      new Map<Id, Sobject>(oldObjectMap)
    );

    AU_TriggerEntry.Args args3 = new AU_TriggerEntry.Args(
      Case.getSObjectType(),
      TriggerOperation.BEFORE_INSERT,
      true,
      new List<SObject>(newObjects),
      new List<SObject>(oldObjects),
      new Map<Id, Sobject>(newObjectMap),
      new Map<Id, Sobject>(oldObjectMap)
    );

    System.assert(args.equals(args2) == true);
    System.assertEquals(args.hashCode(), args2.hashCode());

    System.assert(args.equals(args3) == false);
    System.assertNotEquals(args.hashCode(), args3.hashCode());

    Test.stopTest();
  }

  private static Case createCase(String subject) {
    Case c = new Case();
    c.Subject = subject;

    insert c;

    return c;
  }
}