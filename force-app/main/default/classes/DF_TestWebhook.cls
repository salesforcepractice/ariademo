@RestResource(urlMapping='/TestWebhook/*')
global class DF_TestWebhook {
    @HttpPost
    global static void test() {
        RestRequest request = RestContext.request;
        system.debug(request.requestBody);
    }
}