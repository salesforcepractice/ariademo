/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 24-Jul-18. 
 */

public with sharing class ASPC_CaseController {
  @AuraEnabled
  public static List<Case> getCases(Id thisContactId) {
    if (thisContactId == null)  {
      thisContactId = [SELECT ContactId, AccountId FROM User WHERE Id = :UserInfo.getUserId()].ContactId;
    }
    return [
        SELECT Id, CaseNumber, Subject, Status, CreatedDate
        FROM Case
        WHERE ContactId = :thisContactId];
  }

  @AuraEnabled
  public static List<String> getCaseStatuses(String selectedObject, String field) {
    return AU_PicklistUtil.getPicklistValues(selectedObject, field);
  }

  @AuraEnabled
  public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
    return AU_AttachmentUtil.save(parentId, fileName, base64Data, contentType, fileId);
  }
}