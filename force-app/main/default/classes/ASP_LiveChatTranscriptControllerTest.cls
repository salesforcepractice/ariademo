/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 15-Dec-18. 
 */

@isTest
private class ASP_LiveChatTranscriptControllerTest {
  @isTest
  static void getRecordsTest() {
    List<LiveChatTranscript> testTranscripts = createLiveChatTranscript();
    Id interactionId = createInteraction(testTranscripts[0].Id).Id;
    Id segmentId = createSegment(interactionId, testTranscripts[0].Id).Id;

    Test.startTest();
    List<LiveChatTranscript> transcripts = ASP_LiveChatTranscriptController.getRecords(segmentId);
    Test.stopTest();

    System.assertEquals(testTranscripts[0].Id, transcripts[0].Id);
    System.assertEquals(testTranscripts[0].StartTime, transcripts[0].StartTime);
    System.assertEquals(testTranscripts[0].EndTime, transcripts[0].EndTime);
  }

  // Helper methods

  private static List<LiveChatTranscript> createLiveChatTranscript()  {
    LiveChatVisitor visitor = new LiveChatVisitor();
    insert visitor;
    List<LiveChatTranscript> transcripts = new List<LiveChatTranscript> {
        new LiveChatTranscript(
            StartTime = DateTime.newInstance(2018, 11, 18, 3, 4, 3),
            EndTime = DateTime.newInstance(2018, 11, 18, 3, 5, 3),
            LiveChatVisitorId = visitor.Id)
    };
    insert transcripts;
    return transcripts;
  }

  private static ASP_Interaction__c createInteraction(Id transcriptId)  {
    ASP_Interaction__c interaction = new ASP_Interaction__c(Interaction_Id__c = transcriptId);
    insert interaction;
    return interaction;
  }

  private static ASP_Interaction_Segment__c createSegment(Id interactionId, Id transcriptId) {
    ASP_Interaction_Segment__c segment = new ASP_Interaction_Segment__c(
        Live_Chat_Transcript__c = transcriptId,
        ASP_Interaction__c = interactionId,
        Interaction_Segment_Id__c = transcriptId + '' + UserInfo.getUserId()
    );
    insert segment;
    return segment;
  }
}