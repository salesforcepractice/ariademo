/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public class AU_PicklistUtil {
  private static final String CLASS_NAME = 'AU_PicklistUtil.' ;

  // Returns providedValue if it is a valid picklist value.  Otherwise, returns defaultValue.
  public static String validPicklistValue(Schema.SObjectField picklistField, String providedValue, String defaultValue) {
    try {
      AU_Debugger.enterFunction(CLASS_NAME + 'validPicklistValue');

      String returnValue;

      if (String.isBlank(providedValue)) {
        return '';
      }

      if (picklistContainsValue(picklistField, providedValue)) {
        return providedValue;
      }

      AU_Debugger.reportWarning(
        CLASS_NAME + 'validPicklistValue',
        String.format(
          'Picklist value is not available.  Picklist field: {0}, Provided value: {1}',
          new List<String> {picklistField.getDescribe().getName(), providedValue}
        )
      );

      return defaultValue;
    } finally {
      AU_Debugger.leaveFunction();
    }
  }

  private static Boolean picklistContainsValue(Schema.SObjectField picklistField, String value) {
    for (Schema.PicklistEntry ple : picklistField.getDescribe().getPicklistValues()) {
      if (ple.getValue() == value)
        return true;
    }

    return false;
  }

  // Returns all picklist values from selectedObject
  public static List<String> getPicklistValues(String selectedObject, String field) {
    try {
      AU_Debugger.enterFunction(CLASS_NAME + 'getPicklistValues');
      List<String> options = new List<String>();
      Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
      Map<String, Schema.SObjectField> fieldMap = schemaMap.get(selectedObject).getDescribe().fields.getMap();
      List<Schema.PicklistEntry> values = fieldMap.get(field).getDescribe().getPickListValues();

      for (Schema.PicklistEntry a: values) {
        options.add(a.getValue());
      }
      return options;
    } catch (Exception ex)  {
      AU_Debugger.reportException(CLASS_NAME + 'getPicklistValues', ex);
      return new List<String>();
    } finally {
      AU_Debugger.leaveFunction();
    }
  }
}