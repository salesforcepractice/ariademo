/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday August 30th 2019
 * Author: varonov
 * File type: '.cls'
 */


public class ASP_MarqueeHandler implements AU_TriggerEntry.ITriggerEntry {
  
  private static final String CLASS_NAME = 'ASP_MarqueeHandler';

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
  }
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount)  {
    AU_Debugger.enterFunction(CLASS_NAME + '.invokeMain:');
    AU_Debugger.debug(CLASS_NAME + '.TriggerOperation: ' + args.operation);

    if (args.operation != TriggerOperation.BEFORE_UPDATE && args.operation != TriggerOperation.AFTER_INSERT && args.operation != TriggerOperation.AFTER_UPDATE && args.operation != TriggerOperation.AFTER_DELETE) {
      AU_Debugger.debug(CLASS_NAME + '.Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<ASP_Marquee__c> activatedRecords = new List<ASP_Marquee__c>();
    List<ASP_Marquee__c> deactivatedRecords = new List<ASP_Marquee__c>();
    List<ASP_Marquee__c> updatedRecords = new List<ASP_Marquee__c>();
    List<ASP_Marquee__c> newRecords = new List<ASP_Marquee__c>();
    List<ASP_Marquee__c> deletedRecords = new List<ASP_Marquee__c>();

    switch on args.operation {
      when AFTER_INSERT {
        newRecords = (List<ASP_Marquee__c>) args.newObjects;
      }
      when AFTER_UPDATE {
        List<ASP_Marquee__c> newMarquees = (List<ASP_Marquee__c>) args.newObjects;
        for (ASP_Marquee__c marquee : newMarquees)  {
          ASP_Marquee__c oldMarquee = (ASP_Marquee__c)args.oldObjectMap.get(marquee.Id);
          if (!oldMarquee.Active__c && marquee.Active__c) {
            activatedRecords.add(marquee);
          }
          if (oldMarquee.Active__c && !marquee.Active__c) {
            deactivatedRecords.add(marquee);
          }
          else {
            updatedRecords.add(marquee);
          }
        }
      }
      when AFTER_DELETE {
        deletedRecords = (List<ASP_Marquee__c>) args.oldObjects;
      }
      when BEFORE_UPDATE {
        List<ASP_Marquee__c> newMarquees = (List<ASP_Marquee__c>) args.newObjects;
        for (ASP_Marquee__c marquee : newMarquees)  {
          ASP_Marquee__c oldMarquee = (ASP_Marquee__c)args.oldObjectMap.get(marquee.Id);
          marquee.Old_User_Group__c = oldMarquee.User_Group__c;
        }
      }
    }

    if (args.operation != TriggerOperation.BEFORE_UPDATE) {
      if (newRecords.size() > 0)  {
        addMarquee(newRecords, true, false, false);
      }
      if (deletedRecords.size() > 0)  {
        addMarquee(deletedRecords, false, false, true);
      }
      if (activatedRecords.size() > 0)  {
        addMarquee(activatedRecords, true, false, false);
      }
      if (deactivatedRecords.size() > 0)  {
        addMarquee(deactivatedRecords, false, false, true);
      }
      if (updatedRecords.size() > 0)  {
        addMarquee(updatedRecords, false, true, false);
      }
    }
  }

  private static void addMarquee(List<ASP_Marquee__c> marquees, Boolean isNew, Boolean isUpdated, Boolean isDeleted) {
    List<ASP_Marquee__c> marqueeNotifiedList = new List<ASP_Marquee__c>();
    for(ASP_Marquee__c marquee : marquees) {
        marqueeNotifiedList.add(marquee);
    }
    publishNotifications(marqueeNotifiedList, isNew, isUpdated, isDeleted);
  }

  private static void publishNotifications(List<ASP_Marquee__c> marqueeList, Boolean isNewRecord, Boolean isUpdatedRecord, Boolean isDeletedRecord) {

    List<Marquee_Notification__e> notificationList = new List<Marquee_Notification__e>();

    for(ASP_Marquee__c marquee : marqueeList) {
      Marquee_Notification__e notification = new Marquee_Notification__e();
      notification.Object_Id__c = marquee.Id;
      notification.Object_Name__c = marquee.Name;
      notification.Title__c = marquee.Title__c;
      notification.Message_Body__c = marquee.Message_Body__c;
      notification.Start_Date__c = marquee.Start_Date__c;
      notification.User_Group__c = marquee.User_Group__c;
      notification.Old_User_Group__c = marquee.Old_User_Group__c;
      notification.isNew__c = isNewRecord;
      notification.isUpdated__c = isUpdatedRecord;
      notification.isDeleted__c = isDeletedRecord;
      notificationList.add(notification);
    }

    List<Database.SaveResult> results = EventBus.publish(notificationList);

    for (Database.SaveResult result : results) {
      if (!result.isSuccess()) {
        for (Database.Error error : result.getErrors()) {
          AU_Debugger.debug(CLASS_NAME + ': Save results error returned: ' +
              error.getStatusCode() +' - '+
              error.getMessage());
        }
      }
    }
  }
}