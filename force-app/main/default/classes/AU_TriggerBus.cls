/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

public class AU_TriggerBus {

  private static Map<String, List<TriggerSetting__mdt>> handlerClassToHandlersMap;
  
  @TestVisible
  private static List<TriggerSetting__mdt> triggerHandlerSettingMocks;
  

  public static void dispatch(AU_TriggerEntry.Args args){

//    Enable if this feature is implemented in the future

//    if (INO_FeatureSwitches.isTriggerDisabled('ALL_TRIGGERS')) {
//      AU_Debugger.debug('AT_BUS feature switch disabled');
//      return;
//    }

    SObjectType currentSObject = (SObjectType) args.objectType;

    setHandlerMaps(currentSObject);

    List<AU_TriggerEntry.ITriggerEntry> handlers = new AU_TriggerEntry.ITriggerEntry[]{};

    List<TriggerSetting__mdt> triggerHandlers = handlerClassToHandlersMap.get(currentSObject.getDescribe().getName());
    AU_Debugger.debug('Trigger handlers: '+triggerHandlers);
    for(TriggerSetting__mdt handler : triggerHandlers){
      Type className = Type.forName(handler.Class_Name_Prefix__c, handler.Class__c);
      
      handlers.add((AU_TriggerEntry.ITriggerEntry)className.newInstance());
      
    }

    AU_TriggerEventDispatcher dispatcher = new AU_TriggerEventDispatcher(handlers);
    dispatcher.dispatch(args);

  }

  private static void setHandlerMaps(sObjectType objectType){

    String sObjectName = objectType.getDescribe().getName();
    handlerClassToHandlersMap = new Map<String, List<TriggerSetting__mdt>>();

    for(TriggerSetting__mdt triggerSetting : getHandlerSettings(objectType)){

      if(!handlerClassToHandlersMap.containsKey(sObjectName)){
        handlerClassToHandlersMap.put(sObjectName, new List<TriggerSetting__mdt>());
      }

      handlerClassToHandlersMap.get(sObjectName).add(triggerSetting);
      
    }
  }

  private static List<TriggerSetting__mdt> getHandlerSettings(sObjectType objectType){
    String sObjectName = objectType.getDescribe().getName();
    List<TriggerSetting__mdt> triggerSettings;

    if (Test.isRunningTest() && triggerHandlerSettingMocks != null) {
        triggerSettings = triggerHandlerSettingMocks;
    } else {
      triggerSettings = new List<TriggerSetting__mdt>(
            [SELECT  DeveloperName, 
                      Class_Name_Prefix__c, 
                      Event__c, 
                      sObject__c,
                      sObject__r.QualifiedApiName, 
                      Class__c, 
                      Order__c
              FROM    TriggerSetting__mdt
              WHERE   sObject__r.QualifiedApiName = :sObjectName 
                      AND Active__c = true
              ORDER BY Order__c, 
                      DeveloperName ASC]);
    }
    return triggerSettings;
  }

}