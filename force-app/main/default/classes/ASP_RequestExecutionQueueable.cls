/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */

public class ASP_RequestExecutionQueueable implements Queueable, Database.AllowsCallouts {

  private static final String CLASS_NAME = 'ASP_RequestExecutionQueueable';

  private final ASP_DelayedRequestExecution request;

  public ASP_RequestExecutionQueueable(ASP_DelayedRequestExecution request) {
    this.request = request;
  }

  public void execute(QueueableContext param1) {
    AU_Debugger.debug(CLASS_NAME + '.execute');
    try {
      request.send();
    } catch (Exception ex) {
      AU_Debugger.reportException(CLASS_NAME, ex);
    }
    AU_Debugger.leaveFunction();
  }
}