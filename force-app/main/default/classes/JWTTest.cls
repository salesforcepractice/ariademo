/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class JWTTest {

  @isTest static void testJwtNone() {
    JWT jwt = new JWT('none');
    jwt.iss = 'your issuer';
    jwt.sub = 'some subject';
    jwt.aud = 'some audience';

    System.assert(jwt.issue().startsWith('eyJhbGciOiJub25lIn0'));
  }

  @isTest static void testJwtHs256() {
    JWT jwt = new JWT('HS256');
    jwt.privateKey = 'base64 encoded secret';
    jwt.iss = 'your issuer';
    jwt.sub = 'some subject';
    jwt.aud = 'some audience';

    System.assert(jwt.issue().startsWith('eyJhbGciOiJIUzI1NiJ9'));
  }

  @isTest static void testJwtRs256() {
    JWT jwt = new JWT('RS256');
    jwt.pkcs8 = 'MIIBVgIBADANBgkqhkiG9w0BAQEFAASCAUAwggE8AgEAAkEAq7BFUpkGp3+LQmlQ' +
    'Yx2eqzDV+xeG8kx/sQFV18S5JhzGeIJNA72wSeukEPojtqUyX2J0CciPBh7eqclQ' +
    '2zpAswIDAQABAkAgisq4+zRdrzkwH1ITV1vpytnkO/NiHcnePQiOW0VUybPyHoGM' +
    '/jf75C5xET7ZQpBe5kx5VHsPZj0CBb3b+wSRAiEA2mPWCBytosIU/ODRfq6EiV04' +
    'lt6waE7I2uSPqIC20LcCIQDJQYIHQII+3YaPqyhGgqMexuuuGx+lDKD6/Fu/JwPb' +
    '5QIhAKthiYcYKlL9h8bjDsQhZDUACPasjzdsDEdq8inDyLOFAiEAmCr/tZwA3qeA' +
    'ZoBzI10DGPIuoKXBd3nk/eBxPkaxlEECIQCNymjsoI7GldtujVnr1qT+3yedLfHK' +
    'srDVjIT3LsvTqw==';
    jwt.iss = 'your issuer';
    jwt.sub = 'some subject';
    jwt.aud = 'some audience';

    System.assert(jwt.issue().startsWith('eyJhbGciOiJSUzI1NiJ9'));
  }
}