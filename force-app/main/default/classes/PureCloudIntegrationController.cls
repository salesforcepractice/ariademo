public class PureCloudIntegrationController {
    @AuraEnabled
    public static CaseData getCaseData(String interactionId) {
        system.debug(interactionId);
        PureCloudCacheService.PureCloudData data = PureCloudCacheService.retrieveData(interactionId);
        CaseData caseData = new CaseData();
        if (String.isNotBlank(data.contactId)) {
            caseData.contactId = Id.valueOf(data.contactId);
        }
        caseData.newCaseSubject = data.newCaseSubject;
        system.debug(caseData);
        return caseData;
    }
    
    public class CaseData {
        @AuraEnabled
        public Id contactId {get; set;}
        @AuraEnabled
        public String newCaseSubject {get; set;}
    }
}