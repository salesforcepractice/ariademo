/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

/*
 * Utility class for any List<> or Set<> related transformation functions.
 */
public class AU_CollectionsUtil {

  private static final String CLASS_NAME = 'AU_CollectionsUtil:';

  public static List<SObject> retainAll(List<SObject> objList, Set<Id> idsToKeep) {
    if (objList == null) {
      return null;
    }

    AU_Debugger.debug(CLASS_NAME + 'Ids to keep: ' + idsToKeep);

    List<SObject> result = new List<SObject>();
    for (SObject obj : objList) {
      if (idsToKeep.contains(obj.Id)) {
        result.add(obj);
      }
    }

    return result;
  }
}