/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday March 27th 2020
 * Author: Vacheslav Aronov
 * File type: '.cls'
 */

public class SC_CallRecordingTask {
  private ApexPages.StandardController controller;
  public Task task {get;set;}

  public SC_CallRecordingTask(ApexPages.StandardController controllerParam){
    controller = controllerParam;
    task = (Task) controller.getRecord();
  }
  public void RetrieveRecording(){
    System.debug('SC_CallRecordingTask: Retrieve record');
    System.debug('SC_CallRecordingTask: task: ' + this.task);
    List<Task> taskDetails = [SELECT Id, CallObject, WhatId, Interaction_Segment__c FROM Task WHERE Id = :this.task.Id]; 
    List<SC_TranscriptService.RelatedObjects> relatedObjects = new List<SC_TranscriptService.RelatedObjects>();
    System.debug('SC_CallRecordingTask: relatedObjects: ' + relatedObjects);
    for (Task t : taskDetails) {
      relatedObjects.add(new SC_TranscriptService.RelatedObjects(t.CallObject, t.Id, t.WhatId, t.Interaction_Segment__c));
    }
    if (!Test.isRunningTest()) {
      System.enqueueJob(new SC_TranscriptService(relatedObjects));
    }  
  }
}