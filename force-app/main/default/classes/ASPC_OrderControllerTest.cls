/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 25-Jul-18. 
 */

@isTest
public class ASPC_OrderControllerTest {

  private static final String PORTALUSERNAME = 'PortalUser@aria.test';
  private static final String ADMINUSERNAME = 'AdminUser@aria.test';

  private static Contact testContact { get; set; }
  private static Account testAccount { get; set; }

  private static User portalUser {
    get {
      if (portalUser == null) {
        portalUser = [SELECT Id FROM User WHERE UserName = :PORTALUSERNAME];
      }
      return portalUser;
    }
    set;
  }

  private static User adminUser {
    get {
      if (adminUser == null) {
        adminUser = [SELECT Id FROM User WHERE UserName = :ADMINUSERNAME];
      }
      return adminUser;
    }
    set;
  }

  @testSetup static void setup() {

    Id userRoleId = [SELECT Id FROM UserRole LIMIT 1].Id;

    ATU_UserFactory.createDefaultBuilder('Aria', 'Admin')
        .withProfile('System Administrator')
        .withField('Username', ADMINUSERNAME)
        .withField('UserRoleId', userRoleId)
        .persist();
  }

  @isTest static void getOrdersTest() {
    System.runAs(adminUser) {
      createAccountContactAndOpportunity();
      createPortalUser();
    }

    System.runAs(portalUser) {
      List<Opportunity> opportunities = [SELECT Name, StageName, CreatedDate FROM Opportunity];

      Test.startTest();
      System.assertEquals(opportunities, ASPC_OrderController.getOrders(testAccount.Id));
      System.assertEquals(opportunities, ASPC_OrderController.getOrders(null));
      Test.stopTest();
    }
  }

  @isTest static void isObjectAccessibleForPortalUserTest() {
    System.runAs(adminUser) {
      createAccountContactAndOpportunity();
      createPortalUser();
    }

    System.runAs(portalUser) {
      System.assertEquals(false, ASPC_OrderController.isObjectAccessible());
    }
  }

  @isTest static void isObjectAccessibleForAdminUser() {

    System.runAs(adminUser) {
      System.assertEquals(true, ASPC_OrderController.isObjectAccessible());
    }
  }

  // Helper methods

  private static void createAccountContactAndOpportunity() {
    testAccount = ATU_AccountFactory.createDefaultBuilder('testAccount').persist();
    testContact = ATU_ContactFactory.createDefaultBuilder('Doe', testAccount.Id)
        .withFirstName('John')
        .withField('Email', 'john.doe.test@test.com')
        .persist();

    ATU_OpportunityFactory.createDefaultBuilder(testAccount.Id).persist();
  }

  private static void createPortalUser()  {
    String profileName = [SELECT Name FROM Profile WHERE UserType like '%portal%' LIMIT 1].Name;

    ATU_UserFactory.createDefaultBuilder('Portal', 'User')
        .withProfile(profileName)
        .withField('ContactId', testContact.Id)
        .withField('Username', PORTALUSERNAME)
        .persist();
  }
}