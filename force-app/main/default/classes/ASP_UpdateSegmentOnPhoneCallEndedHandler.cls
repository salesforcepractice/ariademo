/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 15-May-19. 
 */

public class ASP_UpdateSegmentOnPhoneCallEndedHandler implements AU_TriggerEntry.ITriggerEntry {

  private static final String CLASS_NAME = 'ASP_UpdateSegmentOnPhoneCallEndedHandler';

  public void invokeWhileInProgress(AU_TriggerEntry.Args args, Integer invocationCount) {
  }
  public void invokeCompleted(AU_TriggerEntry.Args args, Integer invocationCount) {
  }

  public void invokeMain(AU_TriggerEntry.Args args, Integer invocationCount)  {
    AU_Debugger.enterFunction(CLASS_NAME + '.invokeMain:');
    AU_Debugger.debug('TriggerOperation: ' + args.operation);

    if (args.operation != TriggerOperation.AFTER_UPDATE && args.operation != TriggerOperation.AFTER_INSERT) {
      AU_Debugger.debug('Operation not supported.');
      AU_Debugger.leaveFunction();
      return;
    }

    List<Task> newTasks = (List<Task>) args.newObjects;
    List<Task> interactionTasks = new List<Task>();
    if(!newTasks.isEmpty()) {
      for(Task t : newTasks)  {
        if ((t.Subject.contains('Call') || t.Subject.contains('Email') || t.Subject.contains('SMS')) && t.CallObject.contains('-')) {
          interactionTasks.add(t);
        }
      }
    }
    if(!interactionTasks.isEmpty()) {
      switch on args.operation  {
        when AFTER_INSERT {
          createInteractionSegment(interactionTasks);
        }
        when AFTER_UPDATE {
          updateInteractionSegment(interactionTasks);
        }
      }
    }
    AU_Debugger.leaveFunction();
  }

  private void createInteractionSegment(List<Task> tasks)  {
    List<ASP_Interaction__c> interactions = new List<ASP_Interaction__c>();
    List<ASP_Interaction_Segment__c> segments = new List<ASP_Interaction_Segment__c>();
    Map<Id, ASP_Interaction_Segment__c> taskIdsToSegments = new Map<Id, ASP_Interaction_Segment__c>();
    Set<Id> objectIds = new Set<Id>();
    String interactionType = 'Phone';

    for (Task t : tasks) {
      if(t.Subject.contains('Phone'))  {
        interactionType = 'Phone';
      }
      else if(t.Subject.contains('Email'))  {
        interactionType = 'Email';
      }
      else if (t.Subject.contains('SMS')) {
        interactionType = 'Chat';
      }

      ASP_Interaction__c interaction = new ASP_Interaction__c(Interaction_Id__c = t.CallObject);
      ASP_Interaction_Segment__c segment = new ASP_Interaction_Segment__c(
          Interaction_Segment_Id__c = t.Id,
          RecordTypeId = AU_RecordTypes.getId('ASP_Interaction_Segment__c', interactionType),
          ASP_Interaction__r = interaction.clone()
      );
      interactions.add(interaction);
      segments.add(segment);
      taskIdsToSegments.put(t.Id, segment);

      if(t.WhatId != null) {
        objectIds.add(t.WhatId);
      }
      if(t.WhoId != null) {
        objectIds.add(t.WhoId);
      }
    }
    if (!interactions.isEmpty())  {
      Database.upsert(interactions, ASP_Interaction__c.fields.Interaction_Id__c, true);
      insert segments;
    }
    updateTask(taskIdsToSegments);
    if(objectIds.size() > 0)  {
      createRelatedItems(objectIds, segments);
    }
  }

  private void updateInteractionSegment(List<Task> tasks) {
    List<ASP_Interaction_Segment__c> segments = new List<ASP_Interaction_Segment__c>();

    for (Task t : tasks) {
      segments.add(new ASP_Interaction_Segment__c (
          Id = t.Interaction_Segment__c,
          Interaction_Segment_Id__c = t.Id,
          After_Work_Duration__c = t.After_Work_Duration__c,
          Disposition__c = t.CallDisposition,
          Call_Duration__c = t.CallDurationInSeconds,
          Queue__c = t.Queue__c,
          Sentiment_Probability__c = t.Sentiment_Score__c,
          SubType__c = t.CallType,
          Transcription__c = t.Transcript__c,
          Notes__c = t.Description,
          Interaction_Details_Link__c = t.Interaction_Details_Link__c,
          Client_Name__c = t.Interaction_Name__c,
          Client_Email__c = t.Caller_Phone__c,
          Client_Phone__c = t.Caller_Phone__c
          )
      );
    }
    update segments;
  }

  private void updateTask(Map<Id, ASP_Interaction_Segment__c> taskIdsToSegments) {
    List<Task> tasks = [SELECT Id, Interaction_Segment__c FROM Task WHERE Id IN :taskIdsToSegments.keySet()];
    for(Task t : tasks) {
      ASP_Interaction_Segment__c segment = taskIdsToSegments.get(t.Id);
      t.Interaction_Segment__c = segment.Id;
    }
    update tasks;
  }

  private void createRelatedItems(Set<Id> objectIds, List<ASP_Interaction_Segment__c> segments)  {
    List<Case> cases = [SELECT CaseNumber, AccountId, ContactId from Case where Id in :objectIds];
    if(cases.size() > 0)  {
      for(Case c : cases) {
        if(c.AccountId != null) {
          objectIds.add(c.AccountId);
        }
        if(c.ContactId != null) {
          objectIds.add(c.ContactId);
        }
      }
    }

    List<ASP_Related_Objects__c> relatedObjects = new List<ASP_Related_Objects__c>();
    for(ASP_Interaction_Segment__c segment : segments)  {
      for(Id objectId : objectIds)  {
        String currentId = String.valueOf(objectId);
        switch on currentId.substring(0, 3) {
          when '001'  {
            relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c=segment.Id, Account__c = objectId, Object_Type__c = 'Account'));
          }
          when '003'  {
            relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c=segment.Id, Contact__c = objectId, Object_Type__c = 'Contact'));
          }
          when '500'  {
            relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c = segment.Id, Case__c = objectId, Object_Type__c = 'Case'));
          }
          when '006'  {
            relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c = segment.Id, Opportunity__c = objectId, Object_Type__c = 'Opportunity'));
          }
          when '00Q'  {
            relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c = segment.Id, Lead__c = objectId, Object_Type__c = 'Lead'));
          }
        }
      }
    }
    insert relatedObjects;
  }
}