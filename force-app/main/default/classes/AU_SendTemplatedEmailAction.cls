/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 27-Aug-18. 
 */

global class AU_SendTemplatedEmailAction {

  private static final String CLASS_NAME = 'AU_SendTemplatedEmailAction';

  @InvocableMethod(label='Send Templated Email' description='Sending email message when invoked by Process Builder')
  global static void send(List<EmailArgs> args) {
    AU_Debugger.enterFunction(CLASS_NAME);

    for (EmailArgs info : args) {
      EmailTemplate emailTemplate = AU_EmailUtil.getEmailTemplateByName(info.emailTemplateName);
      OrgWideEmailAddress fromEmailAddress = AU_EmailUtil.getFromEmailAddress(info.fromAddressDisplayName);

      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      if (fromEmailAddress != null) {
        mail.setOrgWideEmailAddressId(fromEmailAddress.Id);
      }
      if (emailTemplate != null) {
        mail.setTemplateId(emailTemplate.Id);
      }
      mail.setTargetObjectId(info.recipientId);
      mail.setWhatId(info.whatId);
      mail.setReplyTo(info.replyToAddress);

      List<Messaging.SendEmailResult> emailResults = new List<Messaging.SendEmailResult>();
      emailResults.addAll(Messaging.sendEmail(new Messaging.SingleEmailMessage[]{
          mail
      }, false));

      for (Messaging.SendEmailResult ser : emailResults) {
        if (!ser.isSuccess()) {
          List<Messaging.SendEmailError> errors = ser.getErrors();
          throw new SendEmailException(errors[0].getMessage());
        } else {
          AU_Debugger.debug('Email sent');
        }
      }
    }
    AU_Debugger.leaveFunction();
  }

  global class EmailArgs {
    @InvocableVariable(label='From Address' required=true description='Display name of the Org Wide Email Address to be used as the sender')
    public String fromAddressDisplayName;

    @InvocableVariable(label='Recipient ID' required=true description='ID of the Contact, Lead or User to whom the email will be sent to')
    public Id recipientId;

    @InvocableVariable(label='Email Template Name' required=true description='Name of the template to be used for this email message')
    public String emailTemplateName;

    @InvocableVariable(label='Merged Object Id' required=false description='(Optional) Set to merge the object fields')
    public Id whatId;

    @InvocableVariable(label='Reply back Email Address' required=false description='(Optional) Email Address that a customer could reply to')
    public String replyToAddress;
  }

  public class SendEmailException extends Exception {
  }
}