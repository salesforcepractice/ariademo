/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 30-Oct-18. 
 */

public class ASP_PostWorkUpdateInteractionController {
  private static String NULLCONTACT = '000000000000000';
  private static String NULLACCOUNT = '000000000000000';

  public ASP_Related_Objects__c relatedObjects {
    get{
      if(relatedObjects == null){
        Id segmentId = System.currentPagereference().getParameters().get('segmentId');
        relatedObjects = new ASP_Related_Objects__c(Interaction_Segment__c=segmentId);
      }
      return relatedObjects;
    }
    private set;
  }

  public static SelectOption[] selectedCaseNumbers {get;set;}
  public static SelectOption[] nonSelectedCaseNumbers{get;set;}
  public static SelectOption[] selectedOpps{get;set;}
  public static SelectOption[] nonSelectedOpps{get;set;}
  public static SelectOption[] selectedLeads{get;set;}
  public static SelectOption[] nonSelectedLeads{get;set;}
  public static String callDisposition{get;set;}

  public ASP_PostWorkUpdateInteractionController(){
    List<String> idStrings = System.currentPagereference().getParameters().get('objectIds').split(',');
    Set<Id> objectIds = new Set<Id>();
    for (String idString : idStrings) {
      if (idString instanceOf Id) {
        objectIds.add(Id.valueOf(idString));
      }
    }

    List<Case> cases = [SELECT CaseNumber, AccountId, ContactId from Case where Id in :objectIds];
    if (cases.size() == 1) {
      relatedObjects.Account__c = cases[0].AccountId;
      relatedObjects.Contact__c = cases[0].ContactId;
    }

    for (Id objectId : objectIds) {
      if (objectId.getSObjectType() == Schema.Account.SObjectType) {
        relatedObjects.Account__c = objectId;
      }
      if (objectId.getSObjectType() == Schema.Contact.SObjectType) {
        relatedObjects.Contact__c = objectId;
      }
    }

    selectedCaseNumbers = new List<SelectOption>();
    for(Case c : cases){
      selectedCaseNumbers.add(new SelectOption(c.id, c.CaseNumber));
    }

    selectedOpps = new List<SelectOption>();
    for(Opportunity o : [SELECT Name from Opportunity where Id in :objectIds]){
      selectedOpps.add(new SelectOption(o.id, o.Name));
    }

    selectedLeads = new List<SelectOption>();
    for(Lead l : [SELECT Name from Lead where Id in :objectIds]){
      selectedLeads.add(new SelectOption(l.id, l.Name));
    }

    nonSelectedCaseNumbers = new List<SelectOption>();
    nonSelectedOpps = new List<SelectOption>();
    nonSelectedLeads = new List<SelectOption>();
  }

  @RemoteAction
  public static void quickSave(String properties){

    AU_Debugger.debug('properties: ' + properties);

    SegmentInfo wrapUpInformation = parse(properties);

    String acctId = wrapUpInformation.account;
    String contactId = wrapUpInformation.contact;
    String casesId = wrapUpInformation.cases;
    String oppsId = wrapUpInformation.opps;
    String leadsId = wrapUpInformation.leads;
    String disposition = wrapUpInformation.disposition;
    String notes = wrapUpInformation.notes;
    String interaction_segment_Id = wrapUpInformation.interactionSegmentId;

    List<ASP_Related_Objects__c> relatedObjects = new List<ASP_Related_Objects__c>();
    ASP_Interaction_Segment__c segment = [SELECT Id, Disposition__c, Notes__c, Transcription__c, ASP_Interaction__r.Interaction_Id__c, After_Work_Duration__c, Active_Time__c, RecordType.Name FROM ASP_Interaction_Segment__c WHERE Interaction_Segment_Id__c =: interaction_segment_Id];

    if(acctId != NULLACCOUNT && acctId != ''){
      relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c=segment.Id, Account__c=acctId, Object_Type__c='Account'));
    }

    if(contactId != NULLCONTACT && contactId != ''){
      relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c=segment.Id, Contact__c=contactId, Object_Type__c='Contact'));
    }

    List<String> ids = splitString(casesId);

    for(String caseId : ids){
      relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c = segment.Id, Case__c = caseId, Object_Type__c='Case'));
    }

    ids = splitString(oppsId);

    for(String oppId : ids) {
      relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c = segment.Id, Opportunity__c = oppId, Object_Type__c = 'Opportunity'));
    }

    ids = splitString(leadsId);

    for(String leadId : ids)  {
      relatedObjects.add(new ASP_Related_Objects__c(Interaction_Segment__c = segment.Id, Lead__c = leadId, Object_Type__c = 'Lead'));
    }

    insert relatedObjects;

    segment.Disposition__c = disposition;
    segment.Notes__c = notes;

    update segment;
  }

  public List<SelectOption> getDispositionCodes() {
    List<SelectOption> dispositionCodes = new List<SelectOption>();
    for(AAC_Call_Disposition_Code__mdt dc: [SELECT DeveloperName, MasterLabel, Disposition_Code__c FROM AAC_Call_Disposition_Code__mdt WHERE Active__c = true ORDER BY DeveloperName ASC]) {
      dispositionCodes.add(new SelectOption(dc.Disposition_Code__c, dc.MasterLabel));
    }

    return dispositionCodes;
  }

  private static List<String> splitString(String stringToSplit){
    List<String> splitStrings = new List<String>();

    if(stringToSplit != null && stringToSplit != '') {
      splitStrings = stringToSplit.split(',');
    }
    return splitStrings;
  }

  private static SegmentInfo parse(String json) {
    return (SegmentInfo) System.JSON.deserialize(json, SegmentInfo.class);
  }

  public class SegmentInfo{
    String account;
    String contact;
    String cases;
    String opps;
    String leads;
    String interactionSegmentId;
    String disposition;
    String notes;
  }
}