/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 07-Aug-18. 
 */

public with sharing class ATU_CaseFactory {

  public static ATU_Case.Builder createDefaultBuilder(String subject) {
    return ATU_Case.build(subject);
  }
}