/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions.
 * Created on 14-Dec-18. 
 */

public class ASP_LiveChatTranscriptController {
  @AuraEnabled
  public static List<LiveChatTranscript> getRecords(Id segmentId) {

    return [
        SELECT  Id, Name, Body, StartTime, EndTime, WaitTime, ChatDuration
        FROM    LiveChatTranscript
        WHERE   Id IN (SELECT Live_Chat_Transcript__c FROM ASP_Interaction_Segment__c WHERE Id = :segmentId)
    ];
  }
}