/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
public class ATU_InvocationCounter {
  private Map<String, Integer> calledTimes = new Map<String, Integer>();

  public Integer getMethodCount(String method){
    method = method.toLowerCase();
    if (!calledTimes.containsKey(method)){
      return 0;
    }
    return calledTimes.get(method);
  }

  public void countMethod(String method){
    method = method.toLowerCase();
    if (!calledTimes.containsKey(method)){
      calledTimes.put(method, 0);
    }
    calledTimes.put(method, calledTimes.get(method) + 1);
  }
}