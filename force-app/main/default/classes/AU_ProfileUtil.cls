/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 */
public class AU_ProfileUtil {

  public static String getCurrentUserProfileName() {
    return [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
  }

  public static String getUserProfileName(String userId) {
    User u = [SELECT ProfileId FROM User WHERE Id = :userId];
    return [SELECT Name FROM Profile WHERE Id = :u.ProfileId].Name;
  }
}