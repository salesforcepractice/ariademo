/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */

@isTest
private class JWTBearerFlowTest {

  private static final String ENDPOINT = 'https://login.salesforce.com/services/oauth2/token';

  @isTest static void testGetAccessToken() {
    Test.setMock(HttpCalloutMock.class, new ATU_SimpleHttpCalloutMock(createExpectedRequest(), createMockResponse(), createRequestOptions()));

    JWT jwt = new JWT('HS256');
    jwt.privateKey = 'base64 encoded secret';
    jwt.iss = 'your issuer';
    jwt.sub = 'some subject';
    jwt.aud = 'https://login.salesforce.com/services/oauth2/token';

    String access_token = JWTBearerFlow.getAccessToken(ENDPOINT, jwt);

    System.assertEquals('foo-bar', access_token);
  }

  @isTest static void testGetAccessToken_BadResponse() {
    HttpResponse resp = new HttpResponse();
    resp.setStatusCode(500);
    Test.setMock(HttpCalloutMock.class, new ATU_SimpleHttpCalloutMock(createExpectedRequest(), resp, createRequestOptions()));

    JWT jwt = new JWT('HS256');
    jwt.privateKey = 'base64 encoded secret';
    jwt.iss = 'your issuer';
    jwt.sub = 'some subject';
    jwt.aud = 'https://login.salesforce.com/services/oauth2/token';

    String access_token = JWTBearerFlow.getAccessToken(ENDPOINT, jwt);

    System.assertEquals(null, access_token);
  }

  private static HttpRequest createExpectedRequest() {
    HttpRequest req = new HttpRequest();
    req.setMethod('POST');
    req.setEndpoint(ENDPOINT);
    req.setHeader('Content-type', 'application/x-www-form-urlencoded');

    return req;
  }

  private static HttpResponse createMockResponse() {
    HttpResponse resp = new HttpResponse();
    resp.setStatusCode(200);
    resp.setBody('{ "access_token": "foo-bar" }');

    return resp;
  }

  private static ATU_SimpleHttpCalloutMock.Options createRequestOptions() {
    ATU_SimpleHttpCalloutMock.Options options = new ATU_SimpleHttpCalloutMock.Options();
    options.checkBody = false;
    return options;
  }
}