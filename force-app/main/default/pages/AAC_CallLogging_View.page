<!--
Copyright 2017 Aria Solutions Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->

<apex:page id="ACT_CallLogging_View" showHeader="false" sideBar="false" standardController="Account" extensions="AAC_CallLogging_ViewExt">
  <apex:slds />

  <table width="100%" class="slds-table slds-table_bordered slds-table_cell-buffer">
    <thead>
    <tr class="slds-text-title_caps">
      <th scope="col">
        <div class="slds-truncate" title="Call Date">{!$Label.ACT_Call_Date_Header}</div>
      </th>
      <th scope="col">
        <div class="slds-truncate" title="Phone Number">{!$Label.ACT_Phone_Number_Header}</div>
      </th>
      <th scope="col">
        <div class="slds-truncate" title="Call Type">{!$Label.ACT_Call_Type_Header}</div>
      </th>
      <th scope="col">
        <div class="slds-truncate" title="Phone Call Duration">{!$Label.ACT_Call_Duration_Header}</div>
      </th>
      <th scope="col">
        <div class="slds-truncate" title="Call Identifier">{!$Label.ACT_Call_Identifier_Header}</div>
      </th>
    </tr>
    </thead>
    <tbody>
    <apex:repeat value="{!phoneTasks}" var="t">
      <tr>
        <td data-label="Call Date">
          <div class="slds-truncate" title="Created Date">{!t.task.CreatedDate}</div>
        </td>
        <th scope="row" data-label="Phone Number">
          <div class="slds-truncate" title="Phone number">{!t.phoneNumber}</div>
        </th>
        <td data-label="Call Type">
          <div class="slds-truncate" title="Call Type">{!t.task.CallType}</div>
        </td>
        <td data-label="Phone Call Duration">
          <div class="slds-truncate" title="Duration">{!FLOOR(t.task.CallDurationInSeconds/60)} {!$Label.ACT_Minutes_in_Call_View} {!MOD(t.task.CallDurationInSeconds, 60)} {!$Label.ACT_Seconds_in_Call_View}</div>
        </td>
        <td data-label="Call Identifier">
          <div class="slds-truncate" title="Call Identifier">{!t.task.CallObject}</div>
        </td>
      </tr>
    </apex:repeat>
    </tbody>
  </table>
</apex:page>