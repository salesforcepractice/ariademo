trigger ASP_EmailMessageTrigger on EmailMessage (before insert, after insert, before update, after update, before delete, after delete) {
  AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
    EmailMessage.SObjectType,
    trigger.operationType,
    trigger.isExecuting,
    trigger.new,
    trigger.old,
    trigger.newMap,
    trigger.oldMap
  );

  ASP_TriggerBus.dispatch(args);
}