trigger ASP_LiveChatTranscriptTrigger on LiveChatTranscript (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

  AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
    LiveChatTranscript.getSObjectType(),
    trigger.operationType,
    trigger.isExecuting,
    trigger.new,
    trigger.old,
    trigger.newMap,
    trigger.oldMap
  );

  ASP_TriggerBus.dispatch(args);

}