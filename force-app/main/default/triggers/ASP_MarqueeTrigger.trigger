/**
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions
 * ***************************************************
 * Created Date: Friday August 30th 2019
 * Author: varonov
 * File type: '.cls'
 */


trigger ASP_MarqueeTrigger on ASP_Marquee__c (before update, after insert, after update, after delete ) {
  AU_TriggerEntry.Args args = new AU_TriggerEntry.Args(
      ASP_Marquee__c.getSObjectType(),
      trigger.operationType,
      trigger.isExecuting,
      trigger.new,
      trigger.old,
      trigger.newMap,
      trigger.oldMap
  );

  ASP_TriggerBus.dispatch(args);
}