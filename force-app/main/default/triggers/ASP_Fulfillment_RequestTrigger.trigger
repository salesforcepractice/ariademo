trigger ASP_Fulfillment_RequestTrigger on ASP_Fulfillment_Request__e (after insert) {
  ASP_LaunchFulfillmentRequestFlowAction.launchFlows(Trigger.new);
}