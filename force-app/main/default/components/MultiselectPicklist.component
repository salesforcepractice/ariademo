<!--
  The MultiselectPicklist component implements a multiselect picklist similar
  to that seen when adding tabs to a Force.com application.

  HTML elements use the same classes as the native multiselect picklist, to
  keep visual consistency in the UI.

  In addition to the visible elements, the component contains two hidden input
  elements, the purpose of which is to hold a string representation of the
  contents of each listbox. As options are added, removed or moved within the
  listboxes, the content of the hidden elements is synchronized to the content
  of the listboxes. When the Visualforce page is submitted, the
  MultiselectController updates its SelectOption[] variables from these hidden
  elements.

  Link to respository: https://github.com/metadaddy/Visualforce-Multiselect-Picklist
  Date Grabbed: Monday, ‎July ‎30, ‎2018

  Copyright (c) 2012, salesforce.com, inc. All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

  Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
  Neither the name of salesforce.com, inc. nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
  OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 -->
 <apex:component controller="MultiselectController">
  <apex:attribute name="leftLabel" description="Label on left listbox."
                  type="String" required="true" />
  <apex:attribute name="rightLabel" description="Label on right listbox."
                  type="String" required="true" />
  <apex:attribute name="size" description="Size of listboxes."
                  type="Integer" required="true" />
  <apex:attribute name="width" description="Width of listboxes."
                  type="String" required="true" />
  <apex:attribute name="showUpDownButtons" description="Should Up/Down buttons be displayed or not."
                  type="Boolean" required="false" default="true"/>

  <apex:attribute name="rightHiddenOption" type="String" description="used to identify the hidden string between cases and opportunities picklist"
                  required="true" />

  <apex:attribute name="leftOption"
                  description="Options list for left listbox." type="SelectOption[]"
                  required="true" assignTo="{!leftOptions}" />
  <apex:attribute name="rightOption"
                  description="Options list for right listbox." type="SelectOption[]"
                  required="true" assignTo="{!rightOptions}" />

  <apex:outputPanel id="multiselectPanel" layout="block" styleClass="duelingListBox">
    <table class="layout" style="display:block;">
      <tbody>
      <tr>
        <td class="selectCell" style="vertical-align: text-top;">
          <apex:outputPanel layout="block" styleClass="selectTitle">
            <!--
              Visualforce prepends the correct prefix to the outputLabel's
              'for' attribute
            -->
            <apex:outputLabel value="{!leftLabel}"
                              for="multiselectPanel:leftList" />
          </apex:outputPanel>
          <select id="{!$Component.multiselectPanel}:leftList"
                  class="multilist" multiple="multiple" size="{!size}"
                  style="width: {!width};">
            <apex:repeat value="{!leftOptions}" var="option">
              <option value="{!option.value}">{!option.label}</option>
            </apex:repeat>
          </select>
        </td>
        <td class="buttonCell" >
          <apex:outputPanel layout="block"  styleClass="text">
            <button class="slds-button slds-button_brand" style="margin-right:5px; margin-left:5px; margin-bottom:10px;" onclick="moveSelectedOptions('{!$Component.multiselectPanel}:leftList',
                  '{!$Component.multiselectPanel}:rightList', '{!$Component.leftHidden}',
                  '{!$Component.rightHidden}'); return false;">&#62;</button>
          </apex:outputPanel>
          <apex:outputPanel layout="block" style="margin-right:5px; margin-left:5px;" styleClass="text">
            <button class="slds-button slds-button_brand" onclick="moveSelectedOptions('{!$Component.multiselectPanel}:rightList',
                  '{!$Component.multiselectPanel}:leftList', '{!$Component.rightHidden}',
                  '{!$Component.leftHidden}'); return false;">&#60;</button>

          </apex:outputPanel>
        </td>
        <td class="selectCell" style="vertical-align: text-top;">
          <apex:outputPanel layout="block" styleClass="selectTitle">
            <apex:outputLabel value="{!rightLabel}" for="multiselectPanel:rightList" />
          </apex:outputPanel>
          <select id="{!$Component.multiselectPanel}:rightList"
                  class="multilist" multiple="multiple" size="{!size}"
                  style="width: {!width};">
            <apex:repeat value="{!rightOptions}" var="option">
              <option value="{!option.value}">{!option.label}</option>
            </apex:repeat>
          </select>
        </td>
      </tr>
      </tbody>
    </table>
    <apex:outputText value="{!leftOptionsHidden}" id="leftHidden" />
    <apex:outputText value="{!rightOptionsHidden}" id="rightHidden" styleClass="{!rightHiddenOption}" />
  </apex:outputPanel>
  <script type="text/javascript">
    if (!buildOutputString) {
      // Create a string from the content of a listbox
      var buildOutputString = function(listBox, hiddenInput) {
        var str = '';

        for ( var x = 0; x < listBox.options.length; x++) {
          if(str === ''){
             str += encodeURIComponent(listBox.options[x].value);
          }
          else{
            str += ',' + encodeURIComponent(listBox.options[x].value);
          }
        }
        hiddenInput.value = str;
      }
    }

    if (!moveSelectedOptions) {
      // Move the selected options in the idFrom listbox to the idTo
      // listbox, updating the corresponding strings in idHdnFrom and
      // idHdnTo
      var moveSelectedOptions = function(idFrom, idTo, idHdnFrom, idHdnTo) {

        listFrom = document.getElementById(idFrom);

        listTo = document.getElementById(idTo);

        for ( var x = 0; x < listTo.options.length; x++) {
          listTo.options[x].selected = false;
        }

        for ( var x = 0; x < listFrom.options.length; x++) {
          if (listFrom.options[x].selected == true) {
            listTo.appendChild(listFrom.options[x]);
            x--;
          }
        }

        listTo.focus();

        buildOutputString(listFrom, document.getElementById(idHdnFrom));
        buildOutputString(listTo, document.getElementById(idHdnTo));
      }
    }

    if (!slideSelectedOptionsUp) {
      // Slide the selected options in the idList listbox up by one position,
      // updating the corresponding string in idHidden
      var slideSelectedOptionsUp = function(idList, idHidden) {
        listBox = document.getElementById(idList);

        var len = listBox.options.length;

        if (len > 0 && listBox.options[0].selected == true) {
          return;
        }

        for ( var x = 1; x < len; x++) {
          if (listBox.options[x].selected == true) {
            listBox.insertBefore(listBox.options[x],
                listBox.options[x - 1]);
          }
        }

        listBox.focus();

        buildOutputString(listBox, document.getElementById(idHidden));
      }
    }

    if (!slideSelectedOptionsDown) {
      // Slide the selected options in the idList listbox down by one position,
      // updating the corresponding string in idHidden
      var slideSelectedOptionsDown = function(idList, idHidden) {
        listBox = document.getElementById(idList);

        var len = listBox.options.length;

        if (len > 0 && listBox.options[len - 1].selected == true) {
          return;
        }

        for ( var x = listBox.options.length - 2; x >= 0; x--) {
          if (listBox.options[x].selected == true) {
            listBox.insertBefore(listBox.options[x + 1],
                listBox.options[x]);
          }
        }

        listBox.focus();

        buildOutputString(listBox, document.getElementById(idHidden));
      }
    }

    // initialize the string representations
    buildOutputString(document.getElementById('{!$Component.multiselectPanel}:leftList'),
        document.getElementById('{!$Component.leftHidden}'));
    buildOutputString(document.getElementById('{!$Component.multiselectPanel}:rightList'),
        document.getElementById('{!$Component.rightHidden}'));
  </script>
</apex:component>