/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 13-Jul-18.
 */

({
  init : function(component, event, helper) {
    helper.getCases(component, helper);
  },

  openMoreRecords : function(component, event, helper) {
    var currentRecords = component.get("v.cases");
    var allRecords = component.get("v.allCases");
    var numRecords = ((allRecords.length - currentRecords.length) < 5) ? allRecords.length : currentRecords.length + 5
    var cases = [];
    for(var i = 0; i < numRecords; i++)  {
      cases.push(allRecords[i]);
    }
    component.set("v.isButtonDisabled", (cases.length === allRecords.length));
    component.set("v.cases", cases);
  },

  openAllRecords : function(component, event, helper) {
    aria_ltngUtil.navigateToUrl("/my-cases");
  }
})