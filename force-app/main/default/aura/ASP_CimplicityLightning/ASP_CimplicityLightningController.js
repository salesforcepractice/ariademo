({ 
  initCrossWindow : function(component, event, helper) { 
    aria.apiReady.then(function() { 
      // here is where we are going to setup and run all of our integration code 
      // Import APIs for omni, utility bar, and console 
      var omniAPI = component.find('omniToolkit'); 
      var workspaceAPI = component.find('workspace'); 
      var utilitybarAPI = component.find('utilitybar');

      aria.api.events.interactionStateChanged.bind(function(e) { 
          if(e.CurrentEvent.MediaType !== 'webform' && e.CurrentEvent.Status === "Complete") {
              workspaceAPI.getAllTabInfo().then(function(response) {
                  for(var i = 0; i < response.length; i++) {
                      workspaceAPI.closeTab({tabId: response[i].tabId});
                  }
            })
          }
      });
        
   aria.api.customEvent.subscribe("ali-demo", "*", function (message, eventType) {
				var url = null;
				var tabName = null;
                var focus = false;
                var keepTabOpen = false;
                switch(eventType){
                    case "ucs-history":
                    	url = "https://demosrv.genesyslab.com:5555/UcsHistory.htm?tenant=Environment&interactionId=" + message.interactionId;
                    	tabName = "UCS History"
                    	break;
                    case "supervisor":
                    	//for some dumb reason having this all in one string causes the :8072 to turn into .8072 when saved.
                    	url ="https://localhost" + ":8072",
                    	tabName = "CIMplicity Supervisor";
                        keepTabOpen = true;
                    	focus = true;
                    	break;
                }
                      
                workspaceAPI.openTab({
                	url: url,
                	focus: focus
                }).then(function(tabId) {
                	workspaceAPI.setTabLabel({
                    	tabId: tabId,
                        label: tabName
                    });
                }).catch(function(error) {
                	console.log(error);
                });
            });

      window.cimp = window.cimp || {};
      
      (function omniIntegration() {
        var presenceStatuses = {};
        var defaultNotReady = null;

        function buildServicePresenceStatusMap() {
          return new Promise((res, rej) => {
            var action = component.get('c.getOmniStates');
            action.setCallback(this, resp => {
              var state = resp.getState();
              if (state === 'SUCCESS') {
                presenceStatuses = JSON.parse(resp.getReturnValue());
                defaultNotReady = Object.entries(presenceStatuses).find(kvp => kvp[0] !== 'Ready' && kvp[0] !== 'Working')[0];
                res();
              } else {
                console.log('CIMplicity Omni Integration get service presence statuses failed');
                rej();
              }
            });
            $A.enqueueAction(action);
          });
        }
        var statusesReady = cimp.statusesReady = buildServicePresenceStatusMap();
        var currStatusId = null;

        function setServicePresenceStatus(statusName) {
          return new Promise((res, rej) => {
            statusesReady.then(() => {
              var status = presenceStatuses[statusName];
              if (!status) {
                res()
                return;
              }
              
              if(status.length > 15)
                status = status.substr(0, 15);
              if(status === currStatusId)
                res();
              else {
                omniAPI.setServicePresenceStatus({ statusId: status }).then((resp) => {
                  if (resp.statusId) {
                    currStatusId = resp.statusId;
                    res();
                  } else if (resp) {
                    currStatusId = status;
                    res();
                  } 
                });
              }
            });
          });
        }
        cimp.setServicePresenceStatus = setServicePresenceStatus;
        cimp.onOmniWork = false;

        function interactionEventHandler(e) {
          var onInteraction = e.CurrentEvent.Status !== "Complete" && e.CurrentEvent.Status !== "Idle";
          if (onInteraction) 
            statusesReady.then(function() {
              setServicePresenceStatus("Working");
            });
          else if (!cimp.onOmniWork && e.CurrentEvent.Status === "Complete")
            statusesReady.then(function() {
              setServicePresenceStatus("Ready");
            });
        }

        cimp.cimStatusReady = true;
        function placeStateHandler(e) { // Currently we are just going to use voice as the trigger, can be easily changed -- if there is no not ready reason code included this will just default to the first that isn't ready or working
          var voice = e.Place.VoiceMedia;
          if (voice.Status === "NotReady") {
            cimp.cimStatusReady = false;
            setServicePresenceStatus(voice.NotReadyReason ? voice.NotReadyReason : defaultNotReady)
          } else if (voice.Status === "Ready" && !cimp.onOmniWork) {
            cimp.cimStatusReady = true;
            setServicePresenceStatus("Ready")
          } 
        }
      
        aria.api.events.interactionStateChanged.bind(interactionEventHandler);
        aria.api.events.placeStateChanged.bind(placeStateHandler);
      })();
    }) 
  },
  handleOmniEvent: function (cmp, ev) {
    aria.apiReady.then(function () {
      switch(ev.getType()) {
        case "lightning:omniChannelStatusChanged":
          var status = ev.getParam('statusApiName');
          if (status === 'Ready' && !cimp.cimStatusReady){
            aria.api.place.goReady();
          }
          else if (status !== 'Working') { // if not working it will be a not ready reason code
            aria.api.place.goNotReady({ reasonCode: status }); // the not ready reason codes need to exactly match what is in the config editor
          }
          break;
        case "lightning:omniChannelWorkAssigned":
        case "lightning:omniChannelWorkAccepted":
          cimp.onOmniWork = true;
          aria.api.place.goNotReady({ reasonCode: "" }); // a not ready reason code can be configured in the config editor to match the working state in omni and then filled here
          cimp.statusesReady.then(function() {
            cimp.setServicePresenceStatus("Working");
          });
          break;
        case "lightning:omniChannelWorkClosed":
        case "lightning:omniChannelWorkDeclined":
          cimp.onOmniWork = false;
          aria.api.place.goReady();
          cimp.statusesReady.then(function() {
            cimp.setServicePresenceStatus("Ready");
          });
          break;
        default:
          console.log("No matching SFORCE_PRESENCE:{KEY}")
          break;
      }
    });
  }  
}) (pane)