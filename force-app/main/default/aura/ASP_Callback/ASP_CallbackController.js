/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 12-Apr-19.
 */

({
  initCrossWindowLib: function (component, event, helper) {
    aria.log.debug('WrapUp:crossWindow library loaded');
    window.__registeredWindows = [];

    /************************************************************
     *********************** REGISTRATION ***********************
     ************************************************************/
    var registerWindow = function (args, postbackFunction, senderWindow) {
      aria.log.info("WrapUp:registerWindow invoked");
      var i, numRegisteredWindows = window.__registeredWindows.length;
      for (i = 0; i < numRegisteredWindows; i++) {
        // Since the API does not unregister a window, we need to ensure that the current window does not get
        // registered multiple times. This could happen if a screen pop overrides the current API client windows
        // in CIMplicity's iFrames.
        if (window.__registeredWindows[i] === senderWindow) {
          postbackFunction();
          aria.log.info("WrapUp:registerWindow windows already registered");
          return;
        }
      }

      window.__registeredWindows.push(senderWindow);
      postbackFunction();
      aria.log.info("WrapUp:registerWindow done");
    };

    aria.crossWindowMessage.registerHandler("Aria.WrapUp.InitializeApi", registerWindow, "*");
    aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.SetPresenceStatus", function (args, postbackFunction) { helper.setOnmiPresenceStatus(component, args, postbackFunction); }, "*");
    aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.Logout", function (args, postbackFunction) { helper.logoutOmni(component, args, postbackFunction); }, "*");
    aria.crossWindowMessage.registerHandler("Aria.WrapUp.Command.ShowToast", function (args) { helper.showToast(component, args); }, "*");
  },

  onOmniStatusChanged : function(component, event, helper) {
    aria.log.debug("WrapUp:onOmniStatusChanged invoked.");
    var statusId = event.getParam('statusId');
    var channels = event.getParam('channels');
    var statusName = event.getParam('statusName');
    var statusApiName = event.getParam('statusApiName');

    helper.triggerEvent(window.__registeredWindows, 'PresenceStatusChanged', {
      statusId: statusId,
      channels: channels,
      statusName: statusName,
      statusApiName: statusApiName
    });
  },

  onOmniLogout : function(component, event, helper) {
    aria.log.debug("WrapUp:onOmniLogout invoked.");
    helper.triggerEvent(window.__registeredWindows, 'Logout', {});
  },

  onWorkAccepted : function(component, event, helper) {
    aria.log.debug("WrapUp:onWorkAccepted invoked.");
    var workItemId = event.getParam('workItemId');
    var workId = event.getParam('workId');
    component.set("v.workItemId", workItemId);
    component.set("v.workId", workId);
    if(workItemId && workItemId.startsWith("a0"))  {
      helper.getCallBack(component, helper);
    }
    helper.triggerEvent(window.__registeredWindows, 'WorkAccepted', {
      workId: workId,
      workItemId: workItemId
    });
  }
})