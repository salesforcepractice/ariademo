/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 30-Oct-18.
 */

({
  triggerEvent: function(registeredWindows, command, parameters) {
      aria.log.info("WrapUpHelper:triggerEvent '" + command + "' invoked, number of registered windows=" + registeredWindows.length);

      var apiCommand = "Aria.WrapUp.Event." + command;

      for (var i = 0, length = registeredWindows.length; i < length; i++) {
        aria.crossWindowMessage.post({
          targetWindow: registeredWindows[i],
          command: apiCommand,
          parameters: parameters,
          targetDomain: "*",
          timeout: 3000
        });
      }

      aria.log.info("WrapUpHelper:triggerEvent done");
  },

  setOnmiPresenceStatus: function (component, args, postbackFunction) {
    aria.log.debug("WrapUpHelper:setOnmiPresenceStatus(" + args.statusId + ")");
    var omniAPI = component.find("omniToolkit");

    omniAPI.getServicePresenceStatusId()
      .then(function(getStatusResult) {
          var currentStatusId = getStatusResult.statusId;
          if (currentStatusId !== args.statusId) {
            omniAPI.setServicePresenceStatus({
              statusId: args.statusId,
              callback: function(setStatusResult) {
                aria.log.debug("WrapUpHelper:onAgentHandler:AgentOnStateChangeHandler " +
                    (setStatusResult.success ? "SUCCEEDED" : "FAILED") +
                    ". Omni status set to " + setStatusResult.statusApiName);

                postbackFunction(setStatusResult);
              }
            });
          } else {
            aria.log.debug('WrapUpHelper:Omni state already set');
            postbackFunction({
              success: true,
              statusId: currentStatusId
            });
          }
        })
      .catch(function(error) {
        postbackFunction({ exception: { message: 'Failed to retrieve current presence status ID' }, error: error });
      });
  },

  logoutOmni: function (component, args, postbackFunction) {
    aria.log.debug("WrapUpHelper:logoutOmni");
    var omniAPI = component.find("omniToolkit");

    omniAPI.getServicePresenceStatusId()
      .then(function(getStatusResult) {
        omniAPI.logout({
          callback: function (logoutResult) {
            postbackFunction(logoutResult);
          }
        });
      })
      .catch(function(error) {
        postbackFunction({ exception: { message: 'Failed to retrieve current presence status ID' }, error: error });
      });
  },

  getFocusedPrimaryTabId: function (component, args, postbackFunction) {
    aria.log.debug("WrapUpHelper:getFocusedPrimaryTabId");
    var workspaceAPI = component.find("workspace");

    workspaceAPI.getFocusedTabInfo()
      .then(function(response) {
        var focusedTabId = response.parentTabId || response.tabId;
        postbackFunction({
          id: focusedTabId
        });
      })
      .catch(function(error) {
        postbackFunction({ exception: { message: 'Failed to retrieve currently focused tab', error: error }, result: {success: false} });
      });
  },

  openSubtab: function (component, args, postbackFunction) {
    aria.log.debug("WrapUpHelper:openSubtab");
    var workspaceAPI = component.find("workspace");
    workspaceAPI.openSubtab(args)
      .then(function(response) {
        postbackFunction({
          success: true,
          id: response
        });
      })
      .catch(function(error) {
        postbackFunction({ exception: { message: 'Failed to open subtab', error: error}, result: {success: false} });
      });
  },

  openPrimaryTab: function (component, args, postbackFunction) {
    aria.log.debug("WrapUpHelper:openPrimaryTab");
    var workspaceAPI = component.find("workspace");
    workspaceAPI.openTab(args)
      .then(function(response) {
        postbackFunction({
          success: true,
          id: response
        });
      })
      .catch(function(error) {
        postbackFunction({ exception: {message: 'Failed to open primary tab', error: error}, result: {success: false} });
      });
  },

  openTab : function(component, workItemId, workId, tabId) {
    aria.log.debug("WrapUpHelper:openTab");
    var workspaceAPI = component.find("workspace");

    if (workItemId.startsWith("570") || workItemId.startsWith("500"))  {
      workItemId = this.convertId(workItemId);
      workId = this.convertId(workId);

      var userId = $A.get("$SObjectType.CurrentUser.Id");
      var helper = this;
      var objectIds = [workItemId];
      
      var segmentId = workItemId.startsWith("570") ? workItemId + userId : workId;
      workspaceAPI.openSubtab({
        parentTabId: tabId,
        url: '/apex/ASP_PostWorkUpdateInteraction?interaction_segment_Id=' + segmentId + '&objectIds=' + objectIds,
        focus: false
      }).then(function(openSubTabResponse) {
        workspaceAPI.setTabLabel({
          tabId: openSubTabResponse,
          label: "Work Wrap Up"
        });
        workspaceAPI.getTabInfo({
          tabId: openSubTabResponse
        }).then(function(getTabInfoResponse) {                
          helper.disableCloseTab(component, getTabInfoResponse.tabId);   
        });
      }).catch(function(error){
        //console.log(error);
      });               
    }
  },

  disableCloseTab : function(component, tabId) {
    aria.log.debug("WrapUpHelper:disableCloseFocusedTab");
    var workspaceAPI = component.find("workspace");

    workspaceAPI.getFocusedTabInfo().then(function(response) {
      workspaceAPI.disableTabClose({
          tabId: tabId,
          disabled: true
      }).then(function(tabInfo) {
          //console.log(tabInfo);
      }).catch(function(error) {
          aria.log.debug(error);
      });
    })
  },

  showToast : function (component, args) {
    aria.log.debug("WrapUpHelper:showToast");
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams(args);
    toastEvent.fire();
  },

  // convert 15-digit to 18-digit Id
  convertId : function(workItemId)  {
    var idParts = workItemId.match(/(.{5})(.{5})(.{5})/);
    var base36 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'.split('');
    var output = [];
    var outer, inner, subparts, buffer;

    for(outer = 1; outer <= 3; outer++) {
        subparts = idParts[outer].split('');
        buffer = 0;
        for(inner = 4; inner >= 0; inner--) {
            buffer = (buffer << 1) | (subparts[inner].match(/[A-Z]/) ? 1 : 0);
        }
        output.push(base36[buffer]);
    }
    return workItemId + output.join('');
  }
})