/**
 * Created by jfischer on 7/20/2016.
 */
({
  doInit: function (component) {
    var action = component.get("c.getNumberOfOpenCasesForCaseAccount");

    action.setParam('caseId', component.get('v.referenceCaseId'));

    action.setCallback(this, function(response) {
        var state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
            component.set("v.items", response.getReturnValue());
        }
    });

    // Send action off to be executed
    $A.enqueueAction(action);
  }
})