/**
 * Created by jliu on 7/18/2018.
 */
({
  getUserContact : function(component, successCallback, errorCallback) {
    var action = component.get("c.getUserContact");
    action.setCallback(this, aria_ltngUtil.createActionCallback(null, successCallback, errorCallback, errorCallback));
    $A.enqueueAction(action);
  },

  createCallCampaign : function(component, formInput, successCallback, errorCallback) {
    var action = component.get("c.submitForm");
    action.setParams({
      formInput: formInput
    });
    action.setCallback(this, aria_ltngUtil.createActionCallback(null, successCallback, errorCallback, errorCallback));
    $A.enqueueAction(action);
  },

  createOutboundCall : function(component, formInput, acInfo, successCallback, errorCallback) {
    var action = component.get("c.createOutboundCall");
    action.setParams({
      formInput: formInput,
      acInfo: acInfo
    });
    action.setCallback(this, aria_ltngUtil.createActionCallback(null, successCallback, errorCallback, errorCallback));
    $A.enqueueAction(action);
  }
})