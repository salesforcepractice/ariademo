/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 21-Jun-19.
 */

({
  triggerEvent: function(registeredWindows, command, parameters) {
    aria.log.info("WrapUpHelper:triggerEvent '" + command + "' invoked, number of registered windows=" + registeredWindows.length);

    var apiCommand = "Aria.WrapUp.Event." + command;

    for (var i = 0, length = registeredWindows.length; i < length; i++) {
      aria.crossWindowMessage.post({
        targetWindow: registeredWindows[i],
        command: apiCommand,
        parameters: parameters,
        targetDomain: "*",
        timeout: 3000
      });
    }

    aria.log.info("WrapUpHelper:triggerEvent done");
  },

  openTab : function(component, workItemId, workId, tabId) {
    aria.log.debug("WrapUpHelper:openTab");
    var workspaceAPI = component.find("workspace");
  },

  getOpenedTabDetails : function(component, tabId)  {
    aria.log.debug("WrapUpHelper:getOpenedTabDetails");
    var workspaceAPI = component.find("workspace");

    workspaceAPI.getTabInfo({
        tabId: tabId
    }).then(function(response) {
      console.log('getOpenedTabDetails:getTabInfo: ' + response.title + '; ' + response.customTitle);
      if(response.title == "AAC_FlowLauncher")  {
        var evt = $A.get("e.force:navigateToComponent");
          evt.setParams({
            componentDef: "c:AAC_LightningModal",
            componentAttributes: {
                // Attributes here.
            }
          });
        evt.fire();
      }
      else{
        console.log('Title is undefined');
      }
    });
  },

  showToast : function (component, args) {
    aria.log.debug("WrapUpHelper:showToast");
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams(args);
    toastEvent.fire();
  },
})