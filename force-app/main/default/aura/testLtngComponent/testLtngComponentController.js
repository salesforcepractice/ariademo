/** Copyright 2017, Aria Solutions Inc.
*
* All Rights Reserved
* Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */


({
  doInit : function(component, event, helper){
    var action = component.get("c.getContact");
    action.setParams({caseId: component.get("v.CaseId")});
    action.setCallback(this, function(response){
      var state = response.getState();
      if(state === "SUCCESS"){
        component.set("v.ContactInfo", response.getReturnValue());
      }
      else{
        var errors = response.getError();
        if(errors) {
          if(errors[0] && errors[0].message){
            console.log("Error message: "+
                        errors[0].message);
          }
        }
      }
    });
    $A.enqueueAction(action);
  },

  openTab : function(component, event, helper){
    console.log("entered click");
    var workspaceAPI = component.find("workspace");
    var utilityBar = component.find("utilitybar");
    var contact = component.get("v.ContactInfo");
    workspaceAPI.openTab({
        url: "#/sObject/"+contact.Id+"/view",
        focus: true
    }).then(function(response) {
        workspaceAPI.getTabInfo({
              tabId: response
        }).then(function(tabInfo) {
          console.log("The url for this tab is: " + tabInfo.url);
        });
    })
    .catch(function(error) {
           console.log(error);
    });
  }
})