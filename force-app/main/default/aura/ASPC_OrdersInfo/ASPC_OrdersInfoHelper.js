/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 13-Jul-18.
 */

({
  getOrders : function(component, helper)  {
      var action = component.get("c.getOrders");
      if (component.get("v.accountId") != null) {
        action.setParams({accountId : component.get("v.accountId")});
      }
      var promise = helper.executeAction(component, action);

      promise.then(
        $A.getCallback(function(allOpportunities) {
          component.set("v.allOrders", allOpportunities);
          var numOrdersToShow = (allOpportunities.length < 5) ? allOpportunities.length : 5
          var opportunities = [];
          for(var i = 0; i < numOrdersToShow; i++)  {
            opportunities.push(allOpportunities[i]);
          }
          component.set("v.isButtonDisabled", (opportunities.length === allOpportunities.length));
          component.set("v.orders", opportunities);
          helper.isObjectAccessible(component, helper);
        })
      ).catch(
        $A.getCallback(function(error)  {
          component.set("v.errorMessage", error.message);
        })
      );
  },

  executeAction: function(component, action, callback)  {
    var logger = component.find('logUtilCmp');

    return new Promise(function(resolve, reject)  {
      action.setCallback(this, aria_ltngUtil.createActionCallback(logger,
        function(response) {
          var allOpportunities = response.getReturnValue();
          resolve(allOpportunities);
        },
        function(response) {
          var errors = response.getError();
          var errorMessage = '';
          if (errors) {
            for(var i = 0; i < error.length; i++)  {
              if (errors[i] && errors[i].message) {
                errorMessage += errors[i].message + ";";
              }
            }
            reject(Error("Error message: Cases cannot be displayed. " + errorMessage + ". Please contact your Salesforce administrator"));
          } else {
           reject(Error("Cases cannot be displayed. Please contact your Salesforce administrator"));
          }
        })
      );
      $A.enqueueAction(action);
    });
  },

  isObjectAccessible : function(component, helper)  {
    var logger = component.find('logUtilCmp');

    var action = component.get("c.isObjectAccessible");
    action.setCallback(this, aria_ltngUtil.createActionCallback(logger,
      function(response) {
        var isAccessible = response.getReturnValue();
        component.set("v.isAccessible", isAccessible);
      },
      function(response) {
        var errors = response.getError();
        if (errors) {
          for(var i = 0; i < error.length; i++)  {
            if (errors[i] && errors[i].message) {
              errorMessage += errors[i].message + ";";
            }
          }
          logger.error(errorMessage);
          reject(Error("Error message: Cases cannot be displayed. " + errorMessage + ". Please contact your Salesforce administrator"));
        } else {
          logger.error("Unknown Error");
          reject(Error("Cases cannot be displayed. Please contact your Salesforce administrator"));
        }
      }
    ));
    $A.enqueueAction(action);
  }
})