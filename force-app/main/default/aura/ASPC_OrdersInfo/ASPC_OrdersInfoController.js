/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 13-Jul-18.
 */

({
  init : function(component, event, helper) {
    helper.getOrders(component, helper);
  },

  openMoreRecords : function(component, event, helper) {
    var currentOrders = component.get("v.orders");
    var allOrders = component.get("v.allOrders");
    var numRecords = ((allOrders.length - currentOrders.length) < 5) ? allOrders.length : currentOrders.length + 5
    var opportunities = [];
    for(var i = 0; i < numRecords; i++)  {
      opportunities.push(allOrders[i]);
    }
    component.set("v.isButtonDisabled", (opportunities.length === allOrders.length));
    component.set("v.orders", opportunities);
  },

  openAllRecords : function(component, event, helper) {
    aria_ltngUtil.navigateToUrl("/my-orders");
  }
})