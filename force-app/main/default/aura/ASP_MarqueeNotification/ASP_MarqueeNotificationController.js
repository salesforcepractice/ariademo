({
  init : function(component, event, helper) {
    helper.getMessages(component, helper);
    component.set('v.subscription', null);
    component.set('v.notifications', []);
    const empApi = component.find('empApi');
    const errorHandler = function (message) {
      console.error('Received error ', JSON.stringify(message));
    };
    empApi.onError($A.getCallback(errorHandler));
    helper.subscribe(component, event, helper);
  },

  setIndex: function(component, event, helper) {
    var index = event.currentTarget.dataset.rowIndex;
    console.log('ASP_MarqueeNotificationController: index: ' + index);
    component.set("v.selected", parseInt(index));
    var marquees = component.get('v.marquees');
    marquees[index].isNew = false;
    marquees[index].isUpdated = false;
    component.set('v.marquees', marquees);
    helper.resetNotification(component, event, helper);
  }
})