({
  getMessages : function(component, helper) {
    var publicGroupAction = component.get("c.getPublicGroups");
    var marqueeAction = component.get("c.getRecords");
    var publicGroupPromise = helper.executeAction(component, publicGroupAction);
    var marqueePromise = helper.executeAction(component, marqueeAction);
    publicGroupPromise.then(
      $A.getCallback(function(groups){
        window.console.log("ASP_MarqueeSliderController: groups: ",groups);
        var userGroups = '';
        groups.forEach(function (item, index) {
          console.log(item, index);
          userGroups += item;
          userGroups += ';';
        });
        component.set('v.userGroups', userGroups);
        return marqueePromise;
      })
    ).then(
      $A.getCallback(function(records){
        window.console.log("ASP_MarqueeSliderController: records: ",records);
        helper.createSlides(component, records);
        helper.startAutoSliding(component, helper);
      })
    ).catch(
      $A.getCallback(function(error)  {
        component.set("v.errorMessage", error.message);
      })
    );
  },

  executeAction: function(component, action, callback)  {
    return new Promise(function(resolve, reject)  {
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS")  {
          resolve(response.getReturnValue());
        }
        else if (state === "ERROR") {
          var errors = response.getError();
          var errorMessage = '';
          if (errors) {
            for(var i = 0; i < error.length; i++)  {
              if (errors[i] && errors[i].message) {
                errorMessage += errors[i].message + ";";
              }
            }
            reject(Error("Error message: Cases cannot be displayed. " + errorMessage + ". Please contact your Salesforce administrator"));
          }
          else {
           reject(Error("Cases cannot be displayed. Please contact your Salesforce administrator"));
          }
        }
      });
      $A.enqueueAction(action);
    });
  },

  createSlides : function(component, messages) {
    var slides = [];
    var slidesToShow = Math.min(10, messages.length);  

    for (var i = 0; i < slidesToShow; i++) {
      slides.push({
        id: messages[i].Id,
        name : messages[i].Name,
        title : messages[i].Title__c ,
        content : messages[i].Message_Body__c,
        date : messages[i].Start_Date__c,
        showPostedData : messages[i].Show_Posted_Date__c,
        isFocused : i === 0
      });
    }
    component.set("v.marquees", slides);
    console.log('ASP_MarqueeNotificationController: Content: ' + slides[0].content);
  },

  subscribe: function (component, event, helper) {
    const empApi = component.find('empApi');
    const channel = component.get('v.channel');
    const replayId = -1;
    const callback = function (message) {
      console.log('ASP_MarqueeNotificationController: Event Received : ' + JSON.stringify(message));
      helper.onReceiveNotification(component, message);
    };
    empApi.subscribe(channel, replayId, $A.getCallback(callback)).then($A.getCallback(function (newSubscription) {
      console.log('Subscribed to channel ' + channel);
      component.set('v.subscription', newSubscription);
    }));
  },

  unsubscribe: function (component, event, helper) {
    const empApi = component.find('empApi');
    const channel = component.get('v.subscription').channel;
    const callback = function (message) {
      console.log('ASP_MarqueeNotificationController: Unsubscribed from channel ' + message.channel);
    };      
    empApi.unsubscribe(component.get('v.subscription'), $A.getCallback(callback));
  },

  onReceiveNotification : function(component, platformEvent) {
    var helper = this;

    var newNotification = {
      recordId : platformEvent.data.payload.Object_Id__c,
      recordName : platformEvent.data.payload.Object_Name__c,
      title : platformEvent.data.payload.Title__c === null ? '' : platformEvent.data.payload.Title__c,
      messageBody : platformEvent.data.payload.Message_Body__c === null ? '' : platformEvent.data.payload.Message_Body__c,
      startDate : platformEvent.data.payload.Start_Date__c,
      userGroup : platformEvent.data.payload.User_Group__c,
      oldUserGroup : platformEvent.data.payload.Old_User_Group__c,
      isNew : platformEvent.data.payload.isNew__c,
      isUpdated : platformEvent.data.payload.isUpdated__c,
      isDeleted : platformEvent.data.payload.isDeleted__c
    };

    const notifications = [];
    notifications.push(newNotification);
    component.set('v.notifications', notifications);
    component.set('v.recordId', newNotification.recordId);

    var marquees = component.get('v.marquees');
    var userGroupsForCurrentUser = component.get('v.userGroups').split(";");
    var isMessageVisible = false;
    var newAssignedGroups = newNotification.userGroup.split(";");
    for (var i = 0; i < newAssignedGroups.length; i++) {
      if (userGroupsForCurrentUser.includes(newAssignedGroups[i])) {
        isMessageVisible = true;
        break;
      }
    }

    if (newNotification.isDeleted)  {
      helper.modifyMarquee(component, helper, newNotification, marquees);
    } else if (newNotification.isUpdated) {
      var isMessageUpdated = false;
      var wasMessageVisible = false;
      for (var j = 0; j < marquees.length; j++) {
        if (marquees[j].id === newNotification.recordId)  {
          wasMessageVisible = true;
          if (!(typeof marquees[j].title === 'undefined') || !(newNotification.title === ''))  {
           if (marquees[j].title !== newNotification.title)  {
             isMessageUpdated = true;
           }
          }
          if (!(typeof marquees[j].content === 'undefined') || !(newNotification.messageBody === '')) {
            if (marquees[j].content !== newNotification.messageBody)  {
              isMessageUpdated = true;
            }
          }
        }
      }
      var isUserGroupUpdated = !(newNotification.userGroup === newNotification.oldUserGroup);
      if (isUserGroupUpdated) {
        if (isMessageVisible) {
          if (!wasMessageVisible) {
            newNotification.isNew = true;
            newNotification.isUpdated = false;
            newNotification.isDeleted = false;
            helper.modifyMarquee(component, helper, newNotification, marquees);
          } else{
            if (isMessageUpdated) {
              helper.modifyMarquee(component, helper, newNotification, marquees);
            }
          }
        } else{
          if (wasMessageVisible) {
            newNotification.isDeleted = true;
            newNotification.isNew = false;
            newNotification.isUpdated = false;
            helper.modifyMarquee(component, helper, newNotification, marquees);
          }
        }
      } else{
        if (isMessageUpdated) {
          helper.modifyMarquee(component, helper, newNotification, marquees);
        }
      }
    } else{
      if (isMessageVisible) {
        helper.modifyMarquee(component, helper, newNotification, marquees);
      }
    }
  },

  modifyMarquee : function(component, helper, newNotification, marquees) {
    if (newNotification.isNew)  {
      marquees.push({
        id: newNotification.recordId,
        name : newNotification.recordName,
        title : newNotification.title,
        content : newNotification.messageBody,
        date : newNotification.startDate,
        showPostedData : true,
        isFocused : false,
        isNew : newNotification.isNew,
        isUpdated : newNotification.isUpdated
      });
      helper.displayToast(component, 'info', 'Marquee ' + newNotification.recordName + ' is added');
      helper.sendNotifications(component, helper, newNotification.recordName);
    } else if (newNotification.isUpdated)  {
      marquees.find(function(element) {
        if (element.id === newNotification.recordId)  {
          element.isUpdated = true;
          element.name = newNotification.recordName;
          element.title = newNotification.title === null ? '' : newNotification.title;
          element.content = newNotification.messageBody === null ? '' : newNotification.messageBody;
          element.date = newNotification.startDate;
          element.isNew = false;
          helper.displayToast(component, 'info', 'Marquee ' + newNotification.recordName + ' is updated');
          helper.sendNotifications(component, helper, newNotification.recordName);
        }
      });
    } else if (newNotification.isDeleted) {
      marquees.find(function(element) {
        if(typeof element !== "undefined") {
          if (element.id === newNotification.recordId)  {
            element.isFocused = false;
            var index = marquees.map(function(e) { return e.id; }).indexOf(element.id);
            if (index > -1) {
              marquees.splice(index, 1);
            }
          }
          else{
            element.isFocused = true;
          }
        }
      });
    }
    marquees.sort(function(a,b){return new Date(b.date) - new Date(a.date)});
    // Setting empty list first solves a bug
    component.set('v.marquees', []);
    component.set('v.marquees', marquees);
  },

  sendNotifications: function (component, helper, recordName)  {
    helper.setUtilityIcon(component, 'alert');
    helper.setUtilityLabel(component, 'New Message');
    helper.setUtilityHighlighted(component, true);
    helper.setPanelHeaderLabel(component, recordName);
    helper.setPanelHeaderIcon(component, 'notification');
  },

  resetNotification : function(component, event, helper) {
    component.set('v.notifications', []);
    helper.setUtilityIcon(component, 'announcement');
    helper.setUtilityLabel(component, 'Aria Marquee Notification');
    helper.setUtilityHighlighted(component, false);
    helper.setPanelHeaderLabel(component, 'Aria Marquee Notification');
    helper.setPanelHeaderIcon(component, 'announcement');
  },

  displayToast : function(component, type, message) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      type: type,
      message: message
    });
    toastEvent.fire();
  },

  setUtilityIcon : function(component, utilityIcon) {
    var utilityAPI = component.find("utilitybar");
    utilityAPI.setUtilityIcon({icon: utilityIcon});
  },

  setUtilityLabel: function(component, utilityLabel) {
    var utilityAPI = component.find("utilitybar");
    utilityAPI.setUtilityLabel({label: utilityLabel});
  },

  setUtilityHighlighted :  function(component, isHighlighted) {
    var utilityAPI = component.find("utilitybar");
    utilityAPI.setUtilityHighlighted({highlighted:isHighlighted});
  },

  setPanelWidth:function(component, width) {
    var utilityAPI = component.find("utilitybar");
    utilityAPI.setPanelWidth({widthPX:width});
  },

  setPanelHeight:function(component, height) {
    var utilityAPI = component.find("utilitybar");
    utilityAPI.setPanelHeight({heightPX :height});
  },

  setPanelHeaderLabel :function(component, title) {
    var utilityAPI = component.find("utilitybar");
    utilityAPI.setPanelHeaderLabel({label  :'New Message: ' + title});
  },

  setPanelHeaderIcon : function(component, headerIcon) {
    var utilityAPI = component.find("utilitybar");
    utilityAPI.setPanelHeaderIcon({icon: headerIcon});
  }
})