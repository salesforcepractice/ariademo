/**
 * Created by jliu on 7/17/2018.
 */
({
  init : function(component, event, helper) {
    helper.getUserContact(component,
      function(response) {
        var contact = response.getReturnValue();
        component.set("v.contact", contact);
        component.set("v.isGuest", contact == null);
      },
      function() {
        component.set("v.contact", null);
        component.set("v.isGuest", true);
      }
    );

    var loginURL = "/s/login/?startURL=" + encodeURIComponent(window.location.pathname);
    component.set("v.loginURL", loginURL);
  },

  submit : function(component, event, helper) {
    var isValid = component.find("callCampaignForm").reduce(function(validSoFar, inputCmp) {
      inputCmp.reportValidity();
      return validSoFar && inputCmp.checkValidity();
    }, true);

    if (!isValid)
      return;

    var isGuest = component.get("v.isGuest");
    var inputPhone = component.get("v.phone");
    var comments = component.get("v.comments");

    if (isGuest) {
      var firstName = component.get("v.firstName");
      var lastName = component.get("v.lastName");

      helper.createCallCampaign(component, firstName, lastName, inputPhone, comments,
        function() {
          component.set("v.isSubmitted", true);
        },
        function() {
          component.set("v.isError", true);
          component.set("v.isSubmitted", true);
        }
      );
    }
    else if (isGuest === false) { // do not do anything if isGuest is undefined
      var contactId = component.get("v.contact.Id");
      var contactPhone = component.get("v.contact.Phone");

      var phone = inputPhone && inputPhone !== "" ? inputPhone : contactPhone;
      helper.createCallCampaignWithContact(component, contactId, phone, comments,
        function() {
          component.set("v.isSubmitted", true);
        },
        function() {
          component.set("v.isError", true);
          component.set("v.isSubmitted", true);
        }
      );
    }
  }
})