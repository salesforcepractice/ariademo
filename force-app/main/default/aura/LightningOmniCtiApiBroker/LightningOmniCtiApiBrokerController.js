/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are premitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belowing to 
 * the customer require a written permission from Aria Solutions. 
 */
({
  initCrossWindowLib: function (component, event, helper) {
    aria.log.debug('LightningOmniCtiApiBroker:initCrossWindowLib');
    window.__registeredWindows = [];

    /************************************************************
     *********************** REGISTRATION ***********************
     ************************************************************/
    var registerWindow = function (args, postbackFunction, senderWindow) {
      aria.log.info("LightningOmniCtiApiBroker:registerWindow invoked");
      var i, numRegisteredWindows = window.__registeredWindows.length;
      for (i = 0; i < numRegisteredWindows; i++) {
        // Since the API does not unregister a window, we need to ensure that the current window does not get
        // registered multiple times. This could happen if a screen pop overrides the current API client windows
        // in CIMplicity's iFrames.
        if (window.__registeredWindows[i] === senderWindow) {
          postbackFunction();
          aria.log.info("LightningOmniCtiApiBroker:registerWindow windows already registered");
          return;
        }
      }

      window.__registeredWindows.push(senderWindow);
      postbackFunction();
      aria.log.info("LightningOmniCtiApiBroker:registerWindow done");
    };

    aria.crossWindowMessage.registerHandler("Aria.Sfdc.InitializeApi", registerWindow, "*");

    aria.crossWindowMessage.registerHandler("Ltng.Omni.Command.OpenPrimaryTab", function (args, postbackFunction) { helper.openPrimaryTab(component, args, postbackFunction); }, "*");
    aria.crossWindowMessage.registerHandler("Ltng.Omni.Command.OpenSubtab", function (args, postbackFunction) { helper.openSubtab(component, args, postbackFunction); }, "*");
    aria.crossWindowMessage.registerHandler("Ltng.Omni.Command.GetFocusedPrimaryTabId", function (args, postbackFunction) { helper.getFocusedPrimaryTabId(component, args, postbackFunction); }, "*");
    aria.crossWindowMessage.registerHandler("Ltng.Omni.Command.SetPresenceStatus", function (args, postbackFunction) { helper.setOnmiPresenceStatus(component, args, postbackFunction); }, "*");
    aria.crossWindowMessage.registerHandler("Ltng.Omni.Command.Logout", function (args, postbackFunction) { helper.logoutOmni(component, args, postbackFunction); }, "*");
  },

  onTabClosed : function(component, event, helper) {
    aria.log.debug("LightningOmniCtiApiBroker:onTabClosed invoked.");
    var tabId = event.getParam('tabId');

    helper.triggerEvent(window.__registeredWindows, 'TabClosed', {
      id: tabId
    });
  },

  onOmniStatusChanged : function(component, event, helper) {
    aria.log.debug("LightningOmniCtiApiBroker:onOmniStatusChanged invoked.");
    var statusId = event.getParam('statusId');
    var channels = event.getParam('channels');
    var statusName = event.getParam('statusName');
    var statusApiName = event.getParam('statusApiName');

    helper.triggerEvent(window.__registeredWindows, 'PresenceStatusChanged', {
      statusId: statusId,
      channels: channels,
      statusName: statusName,
      statusApiName: statusApiName
    });
  },

  onOmniLogout : function(component, event, helper) {
    aria.log.debug("LightningOmniCtiApiBroker:onOmniLogout invoked.");
    helper.triggerEvent(window.__registeredWindows, 'Logout', {});
  },

  onWorkAccepted : function(component, event, helper) {
    aria.log.debug("LightningOmniCtiApiBroker:onWorkAccepted invoked.");
    var workItemId = event.getParam('workItemId');
    var workId = event.getParam('workId');
    helper.triggerEvent(window.__registeredWindows, 'WorkAccepted', {
      workId: workId,
      workItemId: workItemId
    });
  },
})