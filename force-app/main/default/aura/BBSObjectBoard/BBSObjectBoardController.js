({
  doInit : function(component, event, helper) {
    component.set("v.OrderBy", "LastModifiedDate ASC");
    component.set("v.Filter", "OwnerId = '" + $A.get("$SObjectType.CurrentUser.Id") + "'");

    helper.doInit(component, event);
    helper.onStatusChange(component, helper);

    var win = window.setInterval(
      $A.getCallback(function() {
        helper.doInit(component, event);
      }), 300000
    );
    component.set("v.win", win);
	},

  handleRecordStageChanged: function (component, event, helper) {
    helper.doInit(component, event);
  },

  // TODO: remove function as it provides incorrect behaviour
  openTab : function(component, event, helper) {
    if (helper.checkLightningExperience()) {
      var workspaceAPI = component.find("workspace");
      var utilityBar = component.find("utilitybar");
      var id = event.target.id;

      workspaceAPI.openTab({
        url: '#/sObject/'+id+'/view',
        focus: true
      }).catch(function(error) {
        window.console.log(error);
      });
      utilityBar.minimizeUtility();
    } 
    else {
      var myEvent = $A.get("e.c:BBSObjectBoardRecordSelectedEvent");
      myEvent.setParam("recordId", event.target.id);
      myEvent.fire();
    }
  },

  onRecordSelected : function(component, event, helper) {
    if (helper.checkLightningExperience()) {
      var utilityBar = component.find("utilitybar");
      var id = event.target.id;

      helper.assignWorkItem(component, id);

      utilityBar.minimizeUtility();
    } 
    else {
      var myEvent = $A.get("e.c:BBSObjectBoardRecordSelectedEvent");
      myEvent.setParam("recordId", event.target.id);
      myEvent.fire();
    }
  },

  refreshData: function(component, event, helper) {
    helper.doInit(component, event);
  },

  handleConsoleStatusChanged: function(component, event, helper) {
    var isEnabled = event.getParam("status");
    var links = component.find("clickableTitle");
    window.console.log('entered handleConsoleStatusChanged');
    if (isEnabled) {
      for(var cmp in links) {
        $A.util.removeClass(links[cmp], "avoid-clicks");
      }
    }
    else {
      for(var comp in links) {
         $A.util.addClass(links[comp], "avoid-clicks");
      }
    }
  },

  onStatusChange: function(component, event, helper) {
    helper.onStatusChange(component, helper);
  },

  disableWidget: function(component, event, helper) {
    helper.disableWidget(component);
  },

  handleShowPopover: function(component, event, helper) {
    var contactJSON = event.currentTarget.dataset.contact;
    if (!contactJSON) {
      return;
    }

    var contact = JSON.parse(contactJSON);
    var selectItemId = event.target.id;
    var selector = "a[id='" +  selectItemId + "']";
    
    $A.createComponent(
      "c:BBSObjectPopover",
      { "ContactInfo": contact },
      function(content, status) {
        if (status === 'SUCCESS') {
          var overlay = component.get('v.overlayPanel');
          if (overlay) {
            overlay.close();
          }

          component.find('overlayLib').showCustomPopover({
            body: content,
            referenceSelector: selector,
            cssClass: "popover"
          }).then($A.getCallback(function(overlay) {
            component.set('v.overlayPanel', overlay);
          }));
        }
      }
    );
  },

  handleClosePopover: function(component, event, helper) {
    var overlay = component.get('v.overlayPanel');
    if (overlay) {
      var timeout = setTimeout(function() {
        overlay.close();
      }, 500);
      component.set("v.closeOverlayTimer", timeout);
    }
  },

  stopClosePopover: function(component, event, helper) {
    var timeout = component.get("v.closeOverlayTimer");
    clearTimeout(timeout);
  }
})