/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 *
 * TODO: Need further development
 */
({
  invoke : function(component, event, helper) {
    return window.legato.api.phone.calls().then(
      function (calls) {
        if (calls.length > 0) {
          var attributeName = component.get("v.attributeName");
          var attributeMap = new Map(calls[0].data);

          component.set("v.attributeValue", attributeMap.get(attributeName));
        }
      },
      function(reason) {
       // log reason
      }
    );
  }
})