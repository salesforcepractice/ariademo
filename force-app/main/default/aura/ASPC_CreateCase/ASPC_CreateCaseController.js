/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 * Created on 16-Jul-18.
 */

({
  init: function(component, event, helper) {
    component.find("caseRecordCreator").getNewRecord(
             "Case",
             null,      // recordTypeId
             false,
       $A.getCallback(function() {
         var rec = component.get("v.newCase");
         var error = component.get("v.newCaseError");
         if(error || (rec === null)) {
           helper.logError(component, "Error initializing record template: " + error);
           return;
         }
       })
    );
  },

  openModal: function(component, event, helper) {
    component.set("v.openModal", true);
    helper.fetchPickListVal(component, 'Status', 'caseStatus');
  },

  closeModal: function(component, event, helper) {
    component.set("v.openModal", false);
    component.set("v.caseFields.Subject", null);
    component.set("v.caseFields.Description", null);
    component.set("v.caseFields.fileAttachment", null);
    component.set("v.fileName", 'No file selected...');
  },

  saveNewCase : function(component, event, helper)  {
    if(helper.validateForm(component)) {
      component.set("v.caseFields.ContactId", component.get("v.contactId"));
      component.set("v.caseFields.Origin", "Web");
      helper.saveRecord(component, event, helper);
    }
  },

  handleFilesChange : function(component, event, helper)  {
    var fileName = 'No File Selected...';
    if (event.getSource().get("v.files").length > 0) {
        fileName = event.getSource().get("v.files")[0]['name'];
    }
    component.set("v.fileName", fileName);
  },

  onPicklistChange : function(component, event, handler)  {
    component.set("v.caseFields.Status", event.getSource().get("v.value"));
  },

  waiting: function(component, event, helper) {
    if(component.get("v.openModal"))  {
      $A.util.addClass(component.find("uploading").getElement(), "uploading");
      $A.util.removeClass(component.find("uploading").getElement(), "notUploading");
    }
  },

  doneWaiting: function(component, event, helper) {
    if(component.get("v.openModal"))  {
      $A.util.removeClass(component.find("uploading").getElement(), "uploading");
      $A.util.addClass(component.find("uploading").getElement(), "notUploading");
    }
  }
})