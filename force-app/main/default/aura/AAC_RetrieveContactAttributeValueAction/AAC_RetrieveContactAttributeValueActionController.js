/** Copyright 2017, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this 
 * code in all of their Salesforce Orgs (Production, Sandboxes), but 
 * any form of distribution to other Salesforce Orgs not belonging to 
 * the customer require a written permission from Aria Solutions. 
 */
({
  invoke : function(component, event, helper) {
   return aria.ac.getAttributes()
     .then(function (result) {
       var attributeName = component.get("v.attributeName");
       var callAttributes = result.attributes;

       component.set("v.attributeValue", callAttributes[attributeName].value);
     });
  }
})