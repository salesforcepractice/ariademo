({
    doInit : function(component, event, helper) {
        window.addEventListener("message", $A.getCallback(function(event) {
            if (event.source && event.data && event.data.type && event.data.type === 'Interaction') {
                var interactionId = event.data.data.id;
                if (event.data.category === 'connect') {
                    var message = JSON.stringify(event.data);
                    component.set("v.debugMsg", message);
                    component.set("v.interactionId", interactionId);
                    
                    var action = component.get("c.getCaseData");
                    action.setParams({ interactionId : interactionId });
                    action.setCallback(this, function(response) {
                        if (response.getState() === "SUCCESS") {
                            var data = response.getReturnValue();
                            component.set("v.debugMsg", JSON.stringify(data));
                            
                            var createCaseEvent = $A.get("e.force:createRecord");
                            createCaseEvent.setParams({
                                "entityApiName": "Case",
								"recordTypeId": "01215000001cqv5AAA",
                                "defaultFieldValues": {
                                    "Subject": data.newCaseSubject,
                                    "ContactId": data.contactId,
                                    "PureCloud_Interaction_Id__c": interactionId
                                }
                            });
                            createCaseEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                }
            }
        }), false);
    },
    
    handleClick : function(component, event, helper) {
        var createCaseEvent = $A.get("e.force:createRecord");
        createCaseEvent.setParams({
            "entityApiName": "Case",
            "recordTypeId": "01215000001cqv5AAA",
            "defaultFieldValues": {
                "Subject": "New Subject",
                "ContactId": "0031k00000T6RzSAAV"
            }
        });
        createCaseEvent.fire();
    }
})