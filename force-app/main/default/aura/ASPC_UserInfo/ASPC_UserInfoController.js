/** Copyright 2018, Aria Solutions Inc.
 *
 * All Rights Reserved
 * Customers of Aria Solutions are permitted to use and modify this
 * code in all of their Salesforce Orgs (Production, Sandboxes), but
 * any form of distribution to other Salesforce Orgs not belonging to
 * the customer require a written permission from Aria Solutions.
 * Created on 12-Jul-18.
 */

({
  init : function(component, event, helper) {
    helper.getContactIdAndAccountId(component, helper);
  }
})